package logging

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"strings"
	"sync"

	"gitlab.com/decapos/termlink/config"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var zapLogger *logger
var zapLoggerOnce sync.Once

// ILogger interface for logger method
type ILogger interface {
	Debug(args ...interface{})
	Debugf(format string, args ...interface{})
	Error(args ...interface{})
	Errorf(format string, args ...interface{})
	Fatal(args ...interface{})
	Fatalf(format string, args ...interface{})
	Info(args ...interface{})
	Infof(format string, args ...interface{})
	Panic(args ...interface{})
	Panicf(format string, args ...interface{})
	Warn(args ...interface{})
	Warnf(format string, args ...interface{})
}

type logger struct {
	entry *zap.SugaredLogger
}

// GetLogger to get interface of logger
func GetLogger() ILogger {
	zapLoggerOnce.Do(func() {
		zapLogger = new(logger)
		zapLogger.entry = getLogger()
	})
	return zapLogger
}

// getLogger get zap log
func getLogger() *zap.SugaredLogger {
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "unknown"
	}

	zapcfg := zap.Config{
		Encoding:         "json",
		Level:            zap.NewAtomicLevelAt(zapcore.DebugLevel),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey: "message",

			LevelKey:    "level",
			EncodeLevel: zapcore.CapitalLevelEncoder,

			TimeKey:    "time",
			EncodeTime: zapcore.ISO8601TimeEncoder,
		},
		InitialFields: map[string]interface{}{"hostname": hostname},
	}

	// Set log output to file if switch is on
	if config.GetConfig().LogFileSwitch {
		zapcfg.OutputPaths = []string{"./logging/termlink.log"}
		zapcfg.ErrorOutputPaths = []string{"./logging/termlink.error.log"}
	}

	zaplog, err := zapcfg.Build()
	defer func() {
		_ = zaplog.Sync()
	}()

	if err != nil {
		log.Fatalf("[GetLogger] can't initialize zap logger: %v", err)
	}

	return zaplog.Sugar()
}

func (l *logger) Debug(args ...interface{}) {
	fields(l).Debug(args...)
}

func (l *logger) Debugf(format string, args ...interface{}) {
	fields(l).Debugf(format, args...)
}

func (l *logger) Error(args ...interface{}) {
	fields(l).Error(args...)
}

func (l *logger) Errorf(format string, args ...interface{}) {
	fields(l).Errorf(format, args...)
}

func (l *logger) Fatal(args ...interface{}) {
	fields(l).Fatal(args...)
}

func (l *logger) Fatalf(format string, args ...interface{}) {
	fields(l).Fatalf(format, args...)
}

func (l *logger) Info(args ...interface{}) {
	fields(l).Info(args...)
}

func (l *logger) Infof(format string, args ...interface{}) {
	fields(l).Infof(format, args...)
}

func (l *logger) Panic(args ...interface{}) {
	fields(l).Panic(args...)
}

func (l *logger) Panicf(format string, args ...interface{}) {
	fields(l).Panicf(format, args...)
}

func (l *logger) Warn(args ...interface{}) {
	fields(l).Warn(args...)
}

func (l *logger) Warnf(format string, args ...interface{}) {
	fields(l).Warnf(format, args...)
}

func fields(l *logger) *zap.SugaredLogger {
	file, line := getCaller()
	return l.entry.With(zap.String("source", fmt.Sprintf("%s:%d", file, line)))
}

func getCaller() (string, int) {
	_, file, line, ok := runtime.Caller(3)
	if !ok {
		file = "<???>"
		line = 1
	} else {
		slash := strings.LastIndex(file, "/")
		file = file[slash+1:]
	}
	return file, line
}
