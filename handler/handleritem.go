package handler

import (
	"net/http"
	"github.com/gin-gonic/gin"

	"gitlab.com/decapos/termlink/logging"
	"gitlab.com/decapos/termlink/service"
)

type (
	// IItemHandler represent interface of this file
	IItemHandler interface {
		AddItem(c *gin.Context)
	}

	itemhandler struct{}
)

// ItemAddReq is request payload for add item
type ItemAddReq struct {
	Name            string  `json:"name" binding:"required"`
	Price           float32 `json:"price" binding:"required"`
	ImagePath       string  `json:"image_path"`
	HasVariant      bool    `json:"has_variant"`
	IsVariant       bool    `json:"is_variant"`
	IsListed        bool    `json:"is_listed"`
	MeasurementUnit int     `json:"measurement_unit"`
	CompanyID       int16   `json:"company_id" binding:"required"`
}

// NewItemHandler will create new instance for this package
func NewItemHandler() IItemHandler {
	if pkgItem == nil {
		pkgItem = service.NewPkgItem()
	}

	return &itemhandler{}
}

// AddItem will handling add item request
func (h *itemhandler) AddItem(c *gin.Context) {
	var req ItemAddReq

	if err := c.BindJSON(&req); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	input := service.ItemInput{
		Name: req.Name,
		Price: req.Price,
		ImagePath: req.ImagePath,
		HasVariant: req.HasVariant,
		IsVariant: req.IsVariant,
		IsListed: req.IsListed,
		MeasurementUnit: req.MeasurementUnit,
		CompanyID: req.CompanyID,
	}

	res, err := pkgItem.AddItem(input)
	if err != nil {
		logging.GetLogger().Errorf("[handler.AddItem] err add item: %s", err.Error())
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	genresp := ResponseSuccess()
	(*genresp).Data = res

	WriteJSON(c, http.StatusOK, genresp)
}