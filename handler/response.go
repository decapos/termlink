package handler

import (
	"strconv"
	"github.com/gin-gonic/gin"

	"gitlab.com/decapos/termlink/util"
)

type (
	// BaseResponse will be use as based for response
	BaseResponse struct {
		Success bool        `json:"success"`
		Data    interface{} `json:"data"`
	}

	// ErrorData data for error response
	ErrorData struct {
		EN string `json:"en"`
		ID string `json:"id"`
	}
)

// ResponseSuccess success response base structure
func ResponseSuccess() *BaseResponse {
	return &BaseResponse{
		Success: true,
	}
}

// ResponseError error response base structure
func ResponseError(err error) *BaseResponse {
	return &BaseResponse{
		Success: false,
		Data:    ErrorData{
			EN: util.GetStringFromMap("en", err.Error()),
			ID: util.GetStringFromMap("id", err.Error()), 
		},
	}
}

// WriteJSON is general function to send response in json
func WriteJSON(c *gin.Context, status int, obj interface{}) {
	c.Writer.Header().Add("Content-Type", "application/json; charset=utf-8")
	c.Writer.Header().Add("Http-Code", strconv.Itoa(status))

	c.SecureJSON(status, obj)
}
