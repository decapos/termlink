package handler

import (
	"gitlab.com/decapos/termlink/repository"
	"gitlab.com/decapos/termlink/service"
)

var redis repository.IRedis
var pkgUser service.PkgUser
var pkgItem service.PkgItem