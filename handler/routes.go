package handler

import (
	gqlHandler "github.com/99designs/gqlgen/handler"
	"github.com/gin-contrib/static"

	"github.com/gin-gonic/gin"
	"gitlab.com/decapos/termlink"
	"gitlab.com/decapos/termlink/middleware"
)

// InitRouter will init router
func InitRouter() *gin.Engine {
	// init handler
	userhandler := NewUserHandler()
	itemhandler := NewItemHandler()

	router := gin.Default()
	router.Use(middleware.GinContextToContextMiddleware())

	// Silverhand needs to be served with a catch all (/*filepath) , but this causes it to conflict
	// with other route (eg. /api will match both /api route and /*filepath route). Gin doesn't handle this well.
	// So, silverhand will need to be served with a middleware instead.
	// https://github.com/gin-gonic/gin/issues/1044
	router.Use(static.Serve("/", static.LocalFile("./silverhand/public", false)))
	router.GET("/api/playground", gin.WrapF(gqlHandler.Playground("GraphQL playground", "/api")))
	router.POST("/api", gin.WrapF(gqlHandler.GraphQL(termlink.NewExecutableSchema(termlink.Config{Resolvers: &termlink.Resolver{}}))))

	auth := router.Group("/auth")
	auth.POST("/register", userhandler.Register)
	auth.POST("/login", userhandler.Login)

	legacy := router.Group("/legacy")
	legacy.Use(middleware.Authorize())
	{
		legacy.POST("/item/add", itemhandler.AddItem)
	}

	return router
}
