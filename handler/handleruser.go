package handler

import (
	"net/http"
	"strings"
	"github.com/gin-gonic/gin"

	"gitlab.com/decapos/termlink/repository"
	"gitlab.com/decapos/termlink/service"
)

type (
	//IUserHandler represent interface of this file
	IUserHandler interface {
		Register(c *gin.Context)
		Login(c *gin.Context)
	}

	userhandler struct{}
)

// LoginRegisterReq struct of login and register request
type LoginRegisterReq struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// NewUserHandler will create new instance for this package
func NewUserHandler() IUserHandler {
	if redis == nil {
		redis = repository.GetRedis()
	}

	if pkgUser == nil {
		pkgUser = service.NewPkgUser()
	}

	return &userhandler{}
}

// Register handle register request
func (h *userhandler) Register(c *gin.Context) {
	var req LoginRegisterReq

	if err := c.BindJSON(&req); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	res, err := pkgUser.RegisterUser(req.Email, req.Password)
	if err != nil {
		WriteJSON(c, http.StatusOK, ResponseError(err))
		return
	}

	genresp := ResponseSuccess()
	(*genresp).Data = res
	domain := strings.Split(c.Request.Host, ":")

	c.SetCookie("token", res.Token, 0, "/", domain[0], false, true)

	WriteJSON(c, http.StatusOK, genresp)
}

// Login handle login request
func (h *userhandler) Login(c *gin.Context) {
	var req LoginRegisterReq

	if err := c.BindJSON(&req); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	res, err := pkgUser.LoginUser(req.Email, req.Password)
	if err != nil {
		WriteJSON(c, http.StatusOK, ResponseError(err))
		return
	}

	genresp := ResponseSuccess()
	(*genresp).Data = res

	domain := strings.Split(c.Request.Host, ":")

	c.SetCookie("token", res.Token, 0, "/", domain[0], false, true)

	WriteJSON(c, http.StatusOK, genresp)
}