package mock_service

import (
	"testing"
	"github.com/golang/mock/gomock"
)

func Test_ServiceUser(t *testing.T) {
	c := gomock.NewController(t)
	m := NewMockPkgUser(c)

	m.EXPECT().RegisterUser("email", "password") //nolint
	m.RegisterUser("email", "password") //nolint
	m.EXPECT().LoginUser("email", "password") //nolint
	m.LoginUser("email", "password") //nolint
}