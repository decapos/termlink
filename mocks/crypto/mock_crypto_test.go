package mock_crypto

import (
	"testing"
	"github.com/golang/mock/gomock"
)

func Test_Crypto(t *testing.T) {
	c := gomock.NewController(t)
	m := NewMockICrypto(c)

	m.EXPECT().HashPassword("password") //nolint
	m.HashPassword("password") //nolint
	m.EXPECT().CheckPassword("hash", "password") //nolint
	m.CheckPassword("hash", "password") //nolint
	m.EXPECT().GenerateJWTToken(int16(1)) //nolint
	m.GenerateJWTToken(int16(1)) //nolint
	m.EXPECT().VerifyToken("token", []byte("key")) //nolint
	m.VerifyToken("token", []byte("key")) //nolint
}