package mock_repository

import (
	"testing"
	"github.com/golang/mock/gomock"
)

func Test_Redis(t *testing.T) {
	c := gomock.NewController(t)
	m := NewMockIRedis(c)

	m.EXPECT().Set("key", "value") //nolint
	m.Set("key", "value") //nolint
	m.EXPECT().Get("key") //nolint
	m.Get("key") //nolint
}