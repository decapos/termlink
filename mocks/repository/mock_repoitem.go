// Code generated by MockGen. DO NOT EDIT.
// Source: repository/repoitem.go

// Package mock_repository is a generated GoMock package.
package mock_repository

import (
	gomock "github.com/golang/mock/gomock"
	repository "gitlab.com/decapos/termlink/repository"
	reflect "reflect"
)

// MockIItemRepo is a mock of IItemRepo interface
type MockIItemRepo struct {
	ctrl     *gomock.Controller
	recorder *MockIItemRepoMockRecorder
}

// MockIItemRepoMockRecorder is the mock recorder for MockIItemRepo
type MockIItemRepoMockRecorder struct {
	mock *MockIItemRepo
}

// NewMockIItemRepo creates a new mock instance
func NewMockIItemRepo(ctrl *gomock.Controller) *MockIItemRepo {
	mock := &MockIItemRepo{ctrl: ctrl}
	mock.recorder = &MockIItemRepoMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockIItemRepo) EXPECT() *MockIItemRepoMockRecorder {
	return m.recorder
}

// CreateItem mocks base method
func (m *MockIItemRepo) CreateItem(input repository.ItemRequest) (int16, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateItem", input)
	ret0, _ := ret[0].(int16)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateItem indicates an expected call of CreateItem
func (mr *MockIItemRepoMockRecorder) CreateItem(input interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateItem", reflect.TypeOf((*MockIItemRepo)(nil).CreateItem), input)
}
