package mock_repository

import (
	"testing"
	"github.com/golang/mock/gomock"

	repo "gitlab.com/decapos/termlink/repository"
)

func Test_RepoUser(t *testing.T) {
	c := gomock.NewController(t)
	m := NewMockIUserRepo(c)

	m.EXPECT().CreateUser(repo.CreateUserInput{}) //nolint
	m.CreateUser(repo.CreateUserInput{}) //nolint
	m.EXPECT().GetUserByEmail("email") //nolint
	m.GetUserByEmail("email") //nolint
}