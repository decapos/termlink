package main

import (
	"fmt"
	"os"

	"gitlab.com/decapos/termlink/config"
	"gitlab.com/decapos/termlink/db/schema"
	"gitlab.com/decapos/termlink/handler"
	"gitlab.com/decapos/termlink/logging"
	repo "gitlab.com/decapos/termlink/repository"
	"gitlab.com/decapos/termlink/util"
)

const defaultPort = "8080"

func main() {
	schema.InitSchema()

	err := config.InitConfig()
	if err != nil {
		logging.GetLogger().Error("[InitConfig] error: ", err.Error())
	}
	cfg := config.GetConfig()

	db := repo.GetConnection()
	defer db.Close()

	repo.InitRedis(fmt.Sprintf("%s:%s", cfg.Redis.Host, cfg.Redis.Port))

	// create table if not exists
	db.MustExec(schema.GetSchema())
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	// init enum
	util.InitEnum()

	// init map lang
	util.InitMapLang(cfg.MapPath)

	router := handler.InitRouter()

	logging.GetLogger().Infof("connect to http://localhost:%s/ for GraphQL playground", port)
	logging.GetLogger().Fatal(router.Run(":"+port))
}
