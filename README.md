# termlink

Backend system that empowers deca.

## Quick start 
1. `docker-compose up -d` to run the api server and databases.
2  `cd silverhand`
3. `yarn` to install frontend dependencies
4. `yarn develop` to start up the final app.
5. open `localhost:8000` to see the app.
6. api is available on `localhost:8000/api`.

## More Details
### Graphql server
You can run the Graphql server using docker-compose, the dashboard will still need to be run manually using yarn.

#### Running using Docker
Follow the step below to run the code and postgres database using docker-compose. Nothing else is needed.
1. install [docker](https://docs.docker.com/install/)
2. `docker-compose up` to start the Graphql server and postgres image
3. the endpoint will be accessible in `localhost:8080/api` and graphql playground is available in `localhost:8080/api/playground`
4. `docker-compose down` to stop the server

#### Running using go directly
Follow the step below to run the code and postgres database using docker-compose.
1. install [go](https://golang.org/doc/install)
2. install [postgres](https://www.postgresql.org/download/)
3. [start](https://www.postgresql.org/docs/9.1/server-start.html) postgres
4. create a db named deca in your postgres instance
5. create a `env.local` file and overrides the db host, username and password according to your needs.
6. run the server using `go run server/server.go`

### Dashboard
The dashboard frontend code is located in the `silverhand` folder. Built using [Gatsby](https://www.gatsbyjs.org/) and utilizes the graphql api.

#### Running storybook using yarn
1. `cd silverhand`
2. `yarn install` to install dependencies
3. `yarn storybook` to run storybook for development
4. `yarn develop` to run the complete app in development mode

### Local configuration
We use [godotenv](https://github.com/joho/godotenv) for the server configuration. You can customize the server configuration for your local use by creating a `.env.local` file and then overriding the variable needed. Configuration that can be customized can be found on `.env` file included in this repo. Example of things that you can customize are the db username, password, host location and port.

You can also override the included config by adding the required environment variables in your system (this won't work if you are running inside).