package util

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"gitlab.com/decapos/termlink/logging"
)

var langID map[string]string
var langEN map[string]string

// InitMapLang initialize map
func InitMapLang(path string) {
	fileID := fmt.Sprintf("%sid.json", path)
	fileEN := fmt.Sprintf("%sen.json", path)

	bytesID, err := ioutil.ReadFile(fileID)
	if err != nil {
		logging.GetLogger().Errorf("error read lang id file: %s", err.Error())
		return
	}

	bytesEN, err := ioutil.ReadFile(fileEN)
	if err != nil {
		logging.GetLogger().Errorf("error read lang en file: %s", err.Error())
		return
	}

	err = json.Unmarshal(bytesID, &langID)
	if err != nil {
		logging.GetLogger().Errorf("error unmarshal lang id: %s", err.Error())
		return
	}

	err = json.Unmarshal(bytesEN, &langEN)
	if err != nil {
		logging.GetLogger().Errorf("error umarshal lang en: %s", err.Error())
		return
	}
}

// GetStringFromMap get sentence from lang map
func GetStringFromMap(lang, text string) string {
	switch lang {
	case "en":
		if _, ok := langEN[text]; !ok {
			return text
		}
		return langEN[text]
	default:
		if _, ok := langID[text]; !ok {
			return text
		}
		return langID[text]
	}
}