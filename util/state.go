package util

const (
	StateTrue  = 1
	StateFalse = 0
)

type (
	EnumWrapper struct {
		Permission map[string]int
		MeasurementUnit map[string]int
	}
)

var Enum EnumWrapper

func InitEnum() {
	Enum.Permission = enumPermission()
	Enum.MeasurementUnit = enumMeasurementUnit()
}

func enumPermission() map[string]int {
	enum := make(map[string]int)

	enum["owner"] = 1
	enum["admin"] = 2
	enum["cashier"] = 3

	return enum
}

func enumMeasurementUnit() map[string]int {
	enum := make(map[string]int)

	enum["pcs"] = 0
	enum["kg"] = 1
	enum["g"] = 2
	enum["l"] = 3
	enum["ml"] = 4

	return enum
}