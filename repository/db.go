package repository

import (
	"fmt"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/decapos/termlink/config"
	"gitlab.com/decapos/termlink/logging"
)

var source *sqlx.DB
var connOnce sync.Once

// GetConnection will return sqlx db
func GetConnection() *sqlx.DB {
	connOnce.Do(func() {
		cfg := config.GetConfig()
		dbconn := fmt.Sprintf("host=%s port=%s user=%s dbname=deca password=%s sslmode=disable", cfg.Database.Host, cfg.Database.Port, cfg.Database.Username, cfg.Database.Password)

		c, err := sqlx.Connect("postgres", dbconn)
		if err != nil {
			logging.GetLogger().Infof("[GetConnection] db master error: %s", err.Error())
			return
		}
		c.SetMaxIdleConns(cfg.Database.MaxIdle)
		c.SetMaxOpenConns(cfg.Database.MaxConn)
		c.SetConnMaxLifetime(time.Minute * 30)
		// c.LogMode(true)

		source = c
	})

	return source
}
