package repository
//go:generate mockgen --source=repository/redis.go --destination=mocks/repository/mock_redis.go

import (
	"time"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/decapos/termlink/logging"
)

var redisObj RedisConn
var rc redis.Conn

// IRedis is interface for redis functions
type IRedis interface {
	Set(key, value string) error
	Get(key string) ([]byte, error)
	Setex(key, value string, ttl time.Duration) error
}

type RedisConn struct {
	Conn *redis.Pool
}

// InitRedis will init redis connection
func InitRedis(address string) {
	rc := newPool(address)
	redisObj = RedisConn{
		Conn: rc,
	}
}

// GetRedis get redis object
func GetRedis() IRedis {
	return &redisObj
}

func GetRedisObj() RedisConn {
	return redisObj
}

func newPool(address string) *redis.Pool {
	return &redis.Pool{
		// Maximum number of idle connections in the pool.
		MaxIdle: 80,
		// max number of connections
		MaxActive: 12000,
		// Dial is an application supplied function for creating and
		// configuring a connection.
		Dial: func() (redis.Conn, error) {
			rc, err := redis.Dial("tcp", address)
			if err != nil {
				panic(err.Error())
			}
			return rc, err
		},
	}
}

func (r *RedisConn) Set(key, value string) error {
	rc = r.Conn.Get()
	defer rc.Close()

	_, err := rc.Do("SET", key, value)
	if err != nil {
		logging.GetLogger().Errorf("[redis.SET] err: %s", err.Error())
	}
	return err
}

func (r *RedisConn) Get(key string) ([]byte, error) {
	rc = r.Conn.Get()
	defer rc.Close()

	data, err := redis.Bytes(rc.Do("GET", key))
	if err != nil && err != redis.ErrNil {
		logging.GetLogger().Errorf("[redis.GET] err: %s", err.Error())
		return data, err
	}

	return data, nil
}

func (r *RedisConn) Setex(key, value string, ttl time.Duration) error {
	rc = r.Conn.Get()
	defer rc.Close()

	_, err := rc.Do("SETEX", key, int64(ttl/time.Second), value)
	if err != nil {
		logging.GetLogger().Errorf("[redis.SETEX] err: %s", err.Error())
	}
	return nil
}