package repository

import (
	"errors"
	"reflect"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/rafaeljusto/redigomock"
)

var mockItem sqlmock.Sqlmock
var mockItemRepo *itemrepo

func init() {
	db, mock, _ := sqlmock.New()
	source = sqlx.NewDb(db, "postgres")

	mockItem = mock

	redisMock := redigomock.NewConn()
	rc = redisMock
	mockItemRepo = &itemrepo{source, redisObj}
}

func Test_GetItemRepo(t *testing.T) {
	db, _, err := sqlmock.New()
	source = sqlx.NewDb(db, "postgres")
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	testCases := []struct{
		name    string
		wantRes IItemRepo
	}{
		{
			name: "valid",
			wantRes: &itemrepo{source, redisObj},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotRes := GetItemRepo()
			if !reflect.DeepEqual(gotRes, tc.wantRes) {
				t.Errorf("[userrepo.GetItemRepo()] got result %v, want result %v", gotRes, tc.wantRes)
			}
		})
	}
}

func Test_CreateItem(t *testing.T) {
	table := sqlmock.NewRows([]string{"id"})
	testCases := []struct{
		name     string
		input    ItemRequest
		wantErr  bool
		mockFunc func(ItemRequest)
	}{
		{
			name: "valid",
			input: ItemRequest{
				Name: "product 1",
				Price: 1000,
				CompanyID: 1,
			},
			wantErr: false,
			mockFunc: func(input ItemRequest) {
				mockItem.ExpectBegin()
				row := table.AddRow(1)
				mockItem.ExpectQuery(`INSERT INTO item (.+) VALUES (.+) RETURNING id`).
				WithArgs(input.Name, input.Price, input.ImagePath, input.HasVariant, input.IsVariant, input.MeasurementUnit, input.IsListed, input.CompanyID).
				WillReturnRows(row)
				mockItem.ExpectCommit()
			},
		},
		{
			name: "invalid - error commit",
			input: ItemRequest{
				Name: "product 1",
				Price: 1000,
				CompanyID: 1,
			},
			wantErr: true,
			mockFunc: func(input ItemRequest) {
				mockItem.ExpectBegin()
				row := table.AddRow(1)
				mockItem.ExpectQuery(`INSERT INTO item (.+) VALUES (.+) RETURNING id`).
				WithArgs(input.Name, input.Price, input.ImagePath, input.HasVariant, input.IsVariant, input.MeasurementUnit, input.IsListed, input.CompanyID).
				WillReturnRows(row)
				mockItem.ExpectCommit().WillReturnError(errors.New("error commit"))
			},
		},
		{
			name: "invalid - error insert",
			input: ItemRequest{
				Name: "product 1",
				Price: 1000,
				CompanyID: 1,
			},
			wantErr: true,
			mockFunc: func(input ItemRequest) {
				mockItem.ExpectBegin()
				mockItem.ExpectQuery(`INSERT INTO item (.+) VALUES (.+) RETURNING id`).
				WithArgs(input.Name, input.Price, input.ImagePath, input.HasVariant, input.IsVariant, input.MeasurementUnit, input.IsListed, input.CompanyID).
				WillReturnError(errors.New("error"))
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.mockFunc(tc.input)
			_, err := mockItemRepo.CreateItem(tc.input)
			if (err != nil) != tc.wantErr {
				t.Errorf("[userrepo.CreateItem()] got error %v, want error %v", err, tc.wantErr)
			}
		})
	}
}