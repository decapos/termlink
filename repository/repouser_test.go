package repository

import (
	"errors"
	"database/sql"
	"reflect"
	"testing"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/rafaeljusto/redigomock"
)

var mockUser sqlmock.Sqlmock
var mockUserRepo *userrepo

func init() {
	db, mock, _ := sqlmock.New()
	source = sqlx.NewDb(db, "postgres")

	mockUser = mock

	redisMock := redigomock.NewConn()
	rc = redisMock
	mockUserRepo = &userrepo{source, redisObj}
}

func Test_GetUserRepo(t *testing.T) {
	db, _, err := sqlmock.New()
	source = sqlx.NewDb(db, "postgres")
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	testCases := []struct{
		name    string
		wantRes IUserRepo
	}{
		{
			name: "valid",
			wantRes: &userrepo{source, redisObj},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotRes := GetUserRepo()
			if !reflect.DeepEqual(gotRes, tc.wantRes) {
				t.Errorf("[userrepo.GetUserRepo()] got result %v, want result %v", gotRes, tc.wantRes)
			}
		})
	}
}

func Test_CreateUser(t *testing.T) {
	table := sqlmock.NewRows([]string{"id"})
	testCases := []struct{
		name     string
		input    CreateUserInput
		wantErr  bool
		mockFunc func(CreateUserInput)
	}{
		{
			name: "valid",
			input: CreateUserInput{
				"test email",
				"password",
			},
			wantErr: false,
			mockFunc: func(input CreateUserInput) {
				mockUser.ExpectBegin()
				row := table.AddRow(1)
				mockUser.ExpectQuery(`INSERT INTO "user" (.+) VALUES (.+) RETURNING id`).WithArgs("test email", "password").WillReturnRows(row)
				mockUser.ExpectCommit()
			},
		},
		{
			name: "invalid - error commit",
			input: CreateUserInput{
				"test email",
				"password",
			},
			wantErr: true,
			mockFunc: func(input CreateUserInput) {
				mockUser.ExpectBegin()
				row := table.AddRow(1)
				mockUser.ExpectQuery(`INSERT INTO "user" (.+) VALUES (.+) RETURNING id`).WithArgs(input.Email, input.Password).WillReturnRows(row)
				mockUser.ExpectCommit().WillReturnError(errors.New("error commit"))
			},
		},
		{
			name: "invalid - error insert",
			input: CreateUserInput{
				"test email",
				"password",
			},
			wantErr: true,
			mockFunc: func(input CreateUserInput) {
				mockUser.ExpectBegin()
				mockUser.ExpectQuery(`INSERT INTO "user" (.+) VALUES (.+) RETURNING id`).WithArgs(input.Email, input.Password).WillReturnError(errors.New("error insert"))
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.mockFunc(tc.input)
			_, err := mockUserRepo.CreateUser(tc.input)
			if (err != nil) != tc.wantErr {
				t.Errorf("[userrepo.CreateUser()] got error %v, want error %v", err, tc.wantErr)
			}
		})
	}
}

func Test_GetUserByEmail(t *testing.T) {
	table := sqlmock.NewRows([]string{"id", "email", "password"})
	row := table.AddRow(1, "email", "hashpassword")

	testCases := []struct{
		name     string
		email    string
		wantErr  bool
		wantRes  *UserData
		mockFunc func()
	}{
		{
			name: "valid",
			email: "email",
			wantErr: false,
			wantRes: &UserData{
				ID: 1,
				Email: "email",
				Password: "hashpassword",
			},
			mockFunc: func() {
				mockUser.ExpectQuery(`SELECT (.+) FROM "user" WHERE (.+)`).WithArgs("email").WillReturnRows(row)
			},
		},
		{
			name: "valid - user not exist",
			email: "email",
			wantErr: false,
			wantRes: nil,
			mockFunc: func() {
				mockUser.ExpectQuery(`SELECT (.+) FROM "user" WHERE (.+)`).WithArgs("email").WillReturnError(sql.ErrNoRows)
			},
		},
		{
			name: "invalid - error get data",
			email: "email",
			wantErr: true,
			mockFunc: func() {
				mockUser.ExpectQuery(`SELECT (.+) FROM "user" WHERE (.+)`).WithArgs("email").WillReturnError(errors.New("error"))
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.mockFunc()
			gotRes, err := mockUserRepo.GetUserByEmail(tc.email)
			if !reflect.DeepEqual(gotRes, tc.wantRes) {
				t.Errorf("[userrepo.GetUserByEmail()] got result %v, want result %v", gotRes, tc.wantRes)
			}
			if (err != nil) != tc.wantErr {
				t.Errorf("[userrepo.GetUserByEmail()] got error %v, want error %v", err, tc.wantErr)
			}
		})
	}
}

func Test_GetUserByID(t *testing.T)  {
	table := sqlmock.NewRows([]string{"id", "email", "password"})
	row := table.AddRow(1, "email", "hashpassword")

	testCases := []struct{
		name     string
		id       int16
		wantErr  bool
		wantRes  *UserData
		mockFunc func()
	}{
		{
			name: "valid",
			id: 1,
			wantErr: false,
			wantRes: &UserData{
				ID: 1,
				Email: "email",
				Password: "hashpassword",
			},
			mockFunc: func() {
				mockUser.ExpectQuery(`SELECT (.+) FROM "user" WHERE (.+)`).WithArgs(1).WillReturnRows(row)
			},
		},
		{
			name: "valid - user not exist",
			id: 1,
			wantErr: false,
			wantRes: nil,
			mockFunc: func() {
				mockUser.ExpectQuery(`SELECT (.+) FROM "user" WHERE (.+)`).WithArgs(1).WillReturnError(sql.ErrNoRows)
			},
		},
		{
			name: "invalid - error get data",
			id: 1,
			wantErr: true,
			mockFunc: func() {
				mockUser.ExpectQuery(`SELECT (.+) FROM "user" WHERE (.+)`).WithArgs(1).WillReturnError(errors.New("error"))
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.mockFunc()
			gotRes, err := mockUserRepo.GetUserByID(tc.id)
			if !reflect.DeepEqual(gotRes, tc.wantRes) {
				t.Errorf("[userrepo.GetUserByID()] got result %v, want result %v", gotRes, tc.wantRes)
			}
			if (err != nil) != tc.wantErr {
				t.Errorf("[userrepo.GetUserByID()] got error %v, want error %v", err, tc.wantErr)
			}
		})
	}
}