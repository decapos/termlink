package repository
//go:generate mockgen --source=repository/repouser.go --destination=mocks/repository/mock_repouser.go

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
	"gitlab.com/decapos/termlink/logging"
)

type (
	// IUserRepo used as interface related to user
	IUserRepo interface {
		CreateUser(input CreateUserInput) (int16, error)
		GetUserByEmail(email string) (*UserData, error)
		GetUserByID(id int16) (*UserData, error)
	}

	userrepo struct {
		db    *sqlx.DB
		redis RedisConn
	}
)

// CreateUserInput is data needed when creat new user
type CreateUserInput struct {
	Email    string `db:"email" json:"email"`
	Password string `db:"password" json:"password"`
}

// UserData is model of user entity
type UserData struct {
	ID       int16          `db:"id" json:"id"`
	Name     sql.NullString `db:"name" json:"name"`
	Email    string         `db:"email" json:"email"`
	Password string         `db:"password" json:"password"`
	Phone    sql.NullString `db:"phone_number" json:"phone_number"`
	Gender   sql.NullString `db:"gender" json:"gender"`
}

// GetUserRepo to get repo user interface
func GetUserRepo() IUserRepo {
	return &userrepo{db: source, redis: redisObj}
}

// CreateUser will insert new user to db
func (r *userrepo) CreateUser(input CreateUserInput) (int16, error) {
	var id int16

	tx, err := r.db.Begin() //nolint
	defer tx.Rollback()     //nolint

	qry := `INSERT INTO "user" (email, password, created_at) VALUES ($1, $2, NOW()) RETURNING id`
	err = tx.QueryRow(qry, input.Email, input.Password).Scan(&id)
	if err != nil {
		logging.GetLogger().Errorf("[repo.CreateUser] error insert: %s", err.Error())
		return 0, err
	}

	err = tx.Commit()
	if err != nil {
		logging.GetLogger().Errorf("[repo.CreateUser] error commit: %s", err.Error())
		return 0, err
	}

	return id, nil
}

func (r *userrepo) GetUserByEmail(email string) (*UserData, error) {
	user := UserData{}

	qry := `SELECT id, name, email, password, phone_number, gender FROM "user" WHERE email=$1`
	err := r.db.Get(&user, qry, email)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		logging.GetLogger().Errorf("[repo.GetUserByEmail] error select db: %s", err.Error())
		return nil, err
	}

	return &user, nil
}

func (r *userrepo) GetUserByID(id int16) (*UserData, error) {
	user := UserData{}

	qry := `SELECT id, name, email, password, phone_number, gender FROM "user" WHERE id=$1`
	err := r.db.Get(&user, qry, id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		logging.GetLogger().Errorf("[repo.GetUserByID] error select db: %s", err.Error())
		return nil, err
	}

	return &user, nil
}