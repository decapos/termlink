package repository
//go:generate mockgen --source=repository/repoitem.go --destination=mocks/repository/mock_repoitem.go

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/decapos/termlink/logging"
)

type (
	// IItemRepo used as interface related to item
	IItemRepo interface {
		CreateItem(input ItemRequest) (int16, error)
	}

	itemrepo struct {
		db    *sqlx.DB
		redis RedisConn
	}
)

type (
	// ItemRequest represents model of item entity
	ItemRequest struct {
		Name            string  `json:"name"`
		Price           float32 `json:"price"`
		ImagePath       string  `json:"image_path"`
		HasVariant      int     `json:"has_variant"`
		IsVariant       int     `json:"is_variant"`
		MeasurementUnit int     `json:"measurement_unit"`
		IsListed        int     `json:"is_listed"`
		CompanyID       int16   `json:"company_id"`
	}

	// ItemDetail detail of item
	ItemDetail struct {
		ID              int16   `db:"id" json:"id"`
		Name            string  `db:"name" json:"name"`
		Price           float32 `db:"price" json:"price"`
		ImagePath       string  `db:"image_path" json:"image_path"`
		HasVariant      int     `db:"has_variant" json:"has_variant"`
		IsVariant       int     `db:"is_variant" json:"is_variant"`
		IsListed        int     `db:"is_listed" json:"is_listed"`
		CompanyID       int16   `db:"company_id" json:"company_id"`
		MeasurementUnit string  `db:"measurement_unit" json:"measurement_unit"`
	}

)

// GetItemRepo to get repo item interface
func GetItemRepo() IItemRepo {
	return &itemrepo{db: source, redis: redisObj}
}

// CreateItem will insert new item to db
func (r *itemrepo) CreateItem(input ItemRequest) (int16, error) {
	var id int16

	tx, err := r.db.Begin() //nolint
	defer tx.Rollback()     //nolint

	qry := `INSERT INTO item (created_at, name, price, image_path, has_variant, is_variant, measurement_unit, is_listed, company_id)
					VALUES (NOW(), $1, $2, $3, $4, $5, $6, $7, $8) RETURNING id`
	err = tx.QueryRow(qry, input.Name, input.Price, input.ImagePath, input.HasVariant, input.IsVariant, input.MeasurementUnit, input.IsListed, input.CompanyID).Scan(&id)
	if err != nil {
		logging.GetLogger().Errorf("[repo.CreateItem] error insert: %s", err.Error())
		return 0, err
	}

	err = tx.Commit()
	if err != nil {
		logging.GetLogger().Errorf("[repo.CreateItem] error commit: %s", err.Error())
		return 0, err
	}

	return id, nil
}
