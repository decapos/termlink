package termlink

//go:generate go run github.com/99designs/gqlgen

import (
	"context"
	"time"
	"database/sql"
	"errors"
	"gitlab.com/decapos/termlink/logging"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"github.com/vektah/gqlparser/gqlerror"

	repo "gitlab.com/decapos/termlink/repository"
) // THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

var source *sqlx.DB

type Resolver struct {
	db    *sqlx.DB
	redis repo.RedisConn
}

func (r *Resolver) Query() QueryResolver {
	// init db
	if source == nil {
		source = repo.GetConnection()
	}
	r.db = source
	r.redis = repo.GetRedisObj()

	return &queryResolver{r}
}

func (r *Resolver) Mutation() MutationResolver {
	// init db
	if source == nil {
		source = repo.GetConnection()
	}
	r.db = source
	r.redis = repo.GetRedisObj()

	return &mutationResolver{r}
}

type queryResolver struct{ *Resolver }

type mutationResolver struct{ *Resolver }

// GinContextFromContext convert context to gin context from request
func GinContextFromContext(ctx context.Context) (*gin.Context, error) {
	ginContext := ctx.Value("gin_context")
	if ginContext == nil {
		err := errors.New("could not retrieve gin.Context")
		return nil, err
	}

	gc, ok := ginContext.(*gin.Context)
	if !ok {
		err := errors.New("gin.Context has wrong type")
		return nil, err
	}
	return gc, nil
}

func (r *queryResolver) Store(ctx context.Context) (*Store, error) {
	// FIXME: This are just mock function for resolver to make sure
	//	that everything works fine together
	return &Store{
		ID:              "",
		Name:            "Teraskota Stand",
		Address:         "Somewhere",
		Expenses:        nil,
		ConnectedDevice: nil,
		FixExpenses:     nil,
		Inventory:       nil,
		Orders:          nil,
	}, nil
}

func (r *queryResolver) Company(ctx context.Context) (*Company, error) {
	centralPark := StoreEdge{
		Cursor: "",
		Node: &Store{
			ID:              "random",
			Name:            "Central Park",
			Address:         "Central Park",
			Expenses:        nil,
			ConnectedDevice: nil,
			FixExpenses:     nil,
			Inventory:       nil,
			Orders:          nil,
		},
	}
	// FIXME: This are just mock function for resolver to make sure
	//	that everything works fine together
	activity := Activity{
		Timestamp: time.Now(),
		Action:    "",
		Actor:     "",
		Store:     nil,
	}
	return &Company{
		ID:   "adsf",
		Name: "Diary Queen",
		Stores: &StoreConnection{
			PageInfo: nil,
			Edges: []*StoreEdge{
				&centralPark,
			},
		},
		PaymentMethods: nil,
		Items:          nil,
		Activities: []*Activity{
			&activity,
		},
	}, nil
}

func (r *queryResolver) User(ctx context.Context) (*User, error) {
	type UserModel struct {
		Name  sql.NullString `db:"name" json:"name"`
		Email string         `db:"email" json:"email"`
	}
	var user UserModel

	gc, err := GinContextFromContext(ctx)
	if err != nil {
		return nil, err
	}

	token := gc.Value("token").(string)
	if token == "" {
		return nil, gqlerror.Errorf("User not logged in")
	}

	userID, _ := r.redis.Get(token)
	if string(userID) == "" {
		return nil, gqlerror.Errorf("Not authorized")
	}
	id, _ := strconv.Atoi(string(userID))

	qry := `SELECT name, email FROM "user" WHERE id=$1`
	err = r.db.Get(&user, qry, id)
	if err != nil && err != sql.ErrNoRows {
		return nil, gqlerror.Errorf("Error get user from db: %s", err.Error())
	}

	return &User{
		ID:        string(userID),
		Name:      user.Name.String,
		Email:     user.Email,
		Companies: nil,
		Avatar:    "https://images.unsplash.com/photo-1542080681-b52d382432af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
	}, nil
}

func (r *queryResolver) Orders(ctx context.Context) ([]*Order, error) {
	// FIXME: This are just mock function for resolver to make sure
	//	that everything works fine together
	return []*Order{}, nil
}
func (r *queryResolver) Items(ctx context.Context, filter *string, first *int, after *string, last *int, before *string) (*ItemConnection, error) {
	// TODO: get company id from context
	var startCursor, endCursor, qry string
	var hasNextPage, hasPreviousPage bool
	var err error

	edges := []*ItemEdge{}
	var items []repo.ItemDetail

	if after != nil {
		if first == nil {
			return nil, gqlerror.Errorf("first can't empty")
		}
		afterID, _ := strconv.Atoi(*after)

		qry = `SELECT id, name, price, is_listed, measurement_unit, image_path
					FROM item	WHERE id>$1 AND company_id=$2 ORDER BY id LIMIT $3`
		err = r.db.Select(&items, qry, afterID, 1, *first+1)
		if err != nil {
			logging.GetLogger().Errorf("Error: %s", err.Error())
			return nil, gqlerror.Errorf("Error get item from db: %s", err.Error())
		}

		if len(items) == *first+1 {
			hasNextPage = true
		}

		startCursor = strconv.Itoa(int(items[0].ID))          //nolint
		endCursor = strconv.Itoa(int(items[len(items)-1].ID)) //nolint

		for _, v := range items {
			isListed := true
			if v.IsListed == 0 {
				isListed = false
			}
			// TODO: fix static measurement unit
			edges = append(edges, &ItemEdge{
				Cursor: strconv.Itoa(int(v.ID)),
				Node: &Item{
					ID:           strconv.Itoa(int(v.ID)),
					Name:         v.Name,
					Price:        int(v.Price),
					IsListed:     isListed,
					VariantItems: []*Item{},
					Tags:         []*Tag{},
					ImagePath:    &v.ImagePath,
					MeasurementUnit: &MeasurementUnit{
						ID: "1",
					},
				},
			})
		}
	} else {
		if first == nil {
			return nil, gqlerror.Errorf("first can't empty")
		}

		qry = `SELECT id, name, price, is_listed, measurement_unit, image_path
					FROM item	WHERE company_id=$1 ORDER BY id LIMIT $2`
		err = r.db.Select(&items, qry, 1, *first+1)
		if err != nil {
			logging.GetLogger().Errorf("Error: %s", err.Error())
			return nil, gqlerror.Errorf("Error get item from db: %s", err.Error())
		}

		if len(items) == *first+1 {
			hasNextPage = true
		}

		startCursor = strconv.Itoa(int(items[0].ID))          //nolint
		endCursor = strconv.Itoa(int(items[len(items)-1].ID)) //nolint

		for _, v := range items {
			isListed := true
			if v.IsListed == 0 {
				isListed = false
			}
			// TODO: fix static measurement unit
			edges = append(edges, &ItemEdge{
				Cursor: strconv.Itoa(int(v.ID)),
				Node: &Item{
					ID:           strconv.Itoa(int(v.ID)),
					Name:         v.Name,
					Price:        int(v.Price),
					IsListed:     isListed,
					VariantItems: []*Item{},
					Tags:         []*Tag{},
					ImagePath:    &v.ImagePath,
					MeasurementUnit: &MeasurementUnit{
						ID: "1",
					},
				},
			})
		}
	}

	if before != nil {
		if last == nil {
			return nil, gqlerror.Errorf("last can't empty")
		}

		beforeID, _ := strconv.Atoi(*before)
		var items []repo.ItemDetail
		qry = `SELECT id, name, price, is_listed, measurement_unit, image_path
					FROM item	WHERE id<$1 AND company_id=$2 ORDER BY id LIMIT $3`
		err = r.db.Select(&items, qry, beforeID, 1, *last+1)
		if err != nil {
			logging.GetLogger().Errorf("Error: %s", err.Error())
			return nil, gqlerror.Errorf("Error get item from db: %s", err.Error())
		}

		if len(items) == *last+1 {
			hasPreviousPage = true
		}

		startCursor = strconv.Itoa(int(items[0].ID))
		endCursor = strconv.Itoa(int(items[len(items)-1].ID))

		for _, v := range items {
			isListed := true
			if v.IsListed == 0 {
				isListed = false
			}
			// TODO: fix static measurement unit
			edges = append(edges, &ItemEdge{
				Cursor: strconv.Itoa(int(v.ID)),
				Node: &Item{
					ID:           strconv.Itoa(int(v.ID)),
					Name:         v.Name,
					Price:        int(v.Price),
					IsListed:     isListed,
					VariantItems: []*Item{},
					Tags:         []*Tag{},
					ImagePath:    &v.ImagePath,
					MeasurementUnit: &MeasurementUnit{
						ID: "1",
					},
				},
			})
		}
	} else {
		if last == nil {
			return nil, gqlerror.Errorf("last can't empty")
		}

		var items []repo.ItemDetail
		qry = `SELECT * FROM
					(SELECT id, name, price, is_listed, measurement_unit, image_path
					FROM item	WHERE company_id=$1 ORDER BY id DESC LIMIT $2)
					ORDER BY id ASC`
		err = r.db.Select(&items, qry, 1, *last+1)
		if err != nil {
			logging.GetLogger().Errorf("Error: %s", err.Error())
			return nil, gqlerror.Errorf("Error get item from db: %s", err.Error())
		}

		if len(items) == *last+1 {
			hasPreviousPage = true
		}

		startCursor = strconv.Itoa(int(items[0].ID))
		endCursor = strconv.Itoa(int(items[len(items)-1].ID))

		for _, v := range items {
			isListed := true
			if v.IsListed == 0 {
				isListed = false
			}
			// TODO: fix static measurement unit
			edges = append(edges, &ItemEdge{
				Cursor: strconv.Itoa(int(v.ID)),
				Node: &Item{
					ID:           strconv.Itoa(int(v.ID)),
					Name:         v.Name,
					Price:        int(v.Price),
					IsListed:     isListed,
					VariantItems: []*Item{},
					Tags:         []*Tag{},
					ImagePath:    &v.ImagePath,
					MeasurementUnit: &MeasurementUnit{
						ID: "1",
					},
				},
			})
		}
	}

	// set to response
	return &ItemConnection{
		PageInfo: &PageInfo{
			HasNextPage:     hasNextPage,
			HasPreviousPage: hasPreviousPage,
			StartCursor:     startCursor,
			EndCursor:       endCursor,
		},
		Edges: edges,
	}, nil
}

func (r *queryResolver) Report(ctx context.Context) (*Report, error) {
	// FIXME: This are just mock function for resolver to make sure
	//	that everything works fine together
	chocolatePerformance := ItemPerformance{
		SalesCount: 1000,
		ItemName:   "Choco",
		Item:       nil,
	}
	vanillaPerformance := ItemPerformance{
		SalesCount: 1000,
		ItemName:   "Vanilla",
		Item:       nil,
	}

	salesJanuary := SalesSummary{
		Time:       "",
		TotalValue: 1000,
		Orders:     nil,
	}
	salesFebruari := SalesSummary{
		Time:       "",
		TotalValue: 2000,
		Orders:     nil,
	}
	sales := ReportSales{
		TimeRange:  "",
		TotalSales: 120000,
		Summaries: []*SalesSummary{
			&salesJanuary,
			&salesFebruari,
		},
	}
	item := ReportItem{
		TimeRange: "",
		ItemPerformances: []*ItemPerformance{
			&vanillaPerformance,
			&chocolatePerformance,
		},
	}
	return &Report{
		Sales: &sales,
		Item:  &item,
	}, nil
}

func (r *mutationResolver) AddItem(ctx context.Context, input ItemInput) (*Item, error) {
	var qry string
	var err error
	var parentID int
	hasVariant := 0
	isListed := 1

	tx, _ := r.db.Begin() //nolint
	defer tx.Rollback()   //nolint

	if !input.IsListed {
		isListed = 0
	}
	// input new item
	if input.ID == nil {
		if len(input.VariantItems) > 0 {
			hasVariant = 1
		}
		qry = `INSERT INTO item (created_at, name, price, image_path, has_variant, is_variant, is_listed, measurement_unit, company_id)
					VALUES (NOW(),$1,$2,$3,$4,$5,$6,$7,$8) RETURNING id`
		err = tx.QueryRow(qry, input.Name, input.Price, input.ImagePath, hasVariant, 0, isListed, input.MeasurementUnit, input.CompanyID).Scan(&parentID)
		if err != nil {
			logging.GetLogger().Debugf("error query row: %s", err.Error())
			return nil, gqlerror.Errorf("Oops, something went wrong")
		}

		// input variant item as new item
		for _, v := range input.VariantItems {
			var id int
			vIsListed := 1
			if !v.IsListed {
				vIsListed = 0
			}
			qry = `INSERT INTO item (created_at, name, price, image_path, has_variant, is_variant, is_listed, measurement_unit, company_id)
						VALUES (NOW(),$1,$2,$3,$4,$5,$6,$7,$8) RETURNING id`
			err = tx.QueryRow(qry, v.Name, v.Price, v.ImagePath, 0, 1, vIsListed, v.MeasurementUnit, v.CompanyID).Scan(&id)
			if err != nil {
				logging.GetLogger().Debugf("error query row: %s", err.Error())
				return nil, gqlerror.Errorf("Oops, something went wrong")
			}

			// input relation parent-child variant
			qry = `INSERT INTO item_variant_relation (created_at, item_id, parent_id) VALUES (NOW(),$1,$2)`
			_, err = tx.Exec(qry, id, parentID)
			if err != nil {
				logging.GetLogger().Debugf("error query row: %s", err.Error())
				return nil, gqlerror.Errorf("Oops, something went wrong")
			}
		}

	} else {
		id := *input.ID
		parentID, _ = strconv.Atoi(id)
		if len(input.VariantItems) > 0 {
			hasVariant = 1
		}
		qry = `UPDATE item SET
						updated_at=NOW(),
						name=$1,
						price=$2,
						image_path=$3,
						has_variant=$4,
						is_variant=$5,
						is_listed=$6,
						measurement_unit=$7,
						company_id=$8
					WHERE id=$9`
		_, err = tx.Exec(qry, input.Name, input.Price, input.ImagePath, hasVariant, 0, isListed, input.MeasurementUnit, input.CompanyID, parentID)
		if err != nil {
			logging.GetLogger().Debugf("error query row: %s", err.Error())
			return nil, gqlerror.Errorf("Oops, something went wrong")
		}

		for _, v := range input.VariantItems {
			vIsListed := 1
			if !v.IsListed {
				vIsListed = 0
			}
			// insert new
			if v.ID == nil {
				var id int
				qry = `INSERT INTO item (created_at, name, price, image_path, has_variant, is_variant, is_listed, measurement_unit, company_id)
				VALUES (NOW(),$1,$2,$3,$4,$5,$6,$7,$8) RETURNING id`

				err = tx.QueryRow(qry, v.Name, v.Price, v.ImagePath, 0, 1, vIsListed, v.MeasurementUnit, v.CompanyID).Scan(&id)
				if err != nil {
					logging.GetLogger().Debugf("error query row: %s", err.Error())
					return nil, gqlerror.Errorf("Oops, something went wrong")
				}

				// input relation parent-child variant
				qry = `INSERT INTO item_variant_relation (created_at, item_id, parent_id) VALUES (NOW(),$1,$2)`
				_, err = tx.Exec(qry, id, parentID)
				if err != nil {
					logging.GetLogger().Debugf("error query row: %s", err.Error())
					return nil, gqlerror.Errorf("Oops, something went wrong")
				}
			} else {
				// update
				qry = `UPDATE item SET
								updated_at=NOW(),
								name=$1,
								price=$2,
								image_path=$3,
								has_variant=$4,
								is_variant=$5,
								is_listed=$6,
								measurement_unit=$7,
								company_id=$8
							WHERE id=$9`
				_, err = tx.Exec(qry, v.Name, v.Price, v.ImagePath, 0, 1, vIsListed, v.MeasurementUnit, v.CompanyID, v.ID)
				if err != nil {
					logging.GetLogger().Debugf("error query row: %s", err.Error())
					return nil, gqlerror.Errorf("Oops, something went wrong")
				}
			}
		}
	}

	err = tx.Commit()
	if err != nil {
		logging.GetLogger().Debugf("error commit: %s", err.Error())
		return nil, gqlerror.Errorf("Oops, something went wrong")
	}

	return &Item{
		ID:       strconv.Itoa(parentID),
		Name:     input.Name,
		Price:    input.Price,
		IsListed: input.IsListed,
	}, nil
}
