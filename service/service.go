package service

import (
	"gitlab.com/decapos/termlink/crypto"
	"gitlab.com/decapos/termlink/repository"
)

var redis repository.IRedis
var userRepo repository.IUserRepo
var itemRepo repository.IItemRepo
var pkgCyrpto crypto.ICrypto