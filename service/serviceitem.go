package service
//go:generate mockgen --source=service/serviceitem.go --destination=mocks/service/mock_serviceitem.go

import (
	"gitlab.com/decapos/termlink/logging"
	repo "gitlab.com/decapos/termlink/repository"
	"gitlab.com/decapos/termlink/util"
)

type (
	// PkgItem is an interface represent this package
	PkgItem interface {
		AddItem(input ItemInput) (ItemResponse, error)
	}

	pkgItem struct{}
)

// ItemInput is structure for add item
type ItemInput struct {
	Name            string  `json:"name"`
	Price           float32 `json:"price"`
	ImagePath       string  `json:"image_path"`
	HasVariant      bool    `json:"has_variant"`
	IsVariant       bool    `json:"is_variant"`
	IsListed        bool    `json:"is_listed"`
	MeasurementUnit int     `json:"measurement_unit"`
	CompanyID       int16   `json:"company_id"`
}

// ItemResponse is response structure after add item
type ItemResponse struct {
	ID              int16   `json:"id"`
	Name            string  `json:"name"`
	Price           float32 `json:"price"`
	ImagePath       string  `json:"image_path"`
	HasVariant      bool    `json:"has_variant"`
	IsVariant       bool    `json:"is_variant"`
	MeasurementUnit int     `json:"measurement_unit"`
	IsListed        bool    `json:"is_listed"`
	CompanyID       int16   `json:"company_id"`
}

// NewPkgItem for create an interface for this package
func NewPkgItem() PkgItem {
	if itemRepo == nil {
		itemRepo = repo.GetItemRepo()
	}

	return &pkgItem{}
}

// AddItem function for add item to db
func (p *pkgItem) AddItem(input ItemInput) (ItemResponse, error) {
	res := ItemResponse{}
	data := repo.ItemRequest{
		Name: input.Name,
		Price: input.Price,
		ImagePath: input.ImagePath,
		MeasurementUnit: input.MeasurementUnit,
		CompanyID: input.CompanyID,
	}

	if input.HasVariant {
		data.HasVariant = util.StateTrue
	}
	if input.IsVariant {
		data.IsVariant = util.StateTrue
	}
	if input.IsListed {
		data.IsListed = util.StateTrue
	}

	id, err := itemRepo.CreateItem(data)
	if err != nil {
		logging.GetLogger().Errorf("[service.AddItem] error create item: %s", err.Error())
		return res, err
	}
	res = ItemResponse{
		ID: id,
		Name: input.Name,
		Price: input.Price,
		ImagePath: input.ImagePath,
		HasVariant: input.HasVariant,
		IsVariant: input.IsVariant,
		MeasurementUnit: input.MeasurementUnit,
		IsListed: input.IsListed,
		CompanyID: input.CompanyID,
	}

	return res, nil
}