package service

import (
	"errors"
	"reflect"
	"strconv"
	"testing"
	"github.com/golang/mock/gomock"

	mCrypto "gitlab.com/decapos/termlink/mocks/crypto"
	mock "gitlab.com/decapos/termlink/mocks/repository"
	"gitlab.com/decapos/termlink/repository"
)

var mockUser *mock.MockIUserRepo
var mockRedis *mock.MockIRedis
var mockCrypto *mCrypto.MockICrypto

var pkg *pkgUser

func initMock(t *testing.T) {
	c := gomock.NewController(t)
	defer c.Finish()

	mockUser = mock.NewMockIUserRepo(c)
	userRepo = mockUser

	mockRedis = mock.NewMockIRedis(c)
	redis = mockRedis

	mockCrypto = mCrypto.NewMockICrypto(c)
	pkgCyrpto = mockCrypto

	pkg = &pkgUser{}
}

func Test_NewPkgUser(t *testing.T) {
	testCases := []struct {
		name    string
		wantRes PkgUser
	}{
		{
			name:    "valid",
			wantRes: &pkgUser{},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			gotRes := NewPkgUser()
			if !reflect.DeepEqual(gotRes, tc.wantRes) {
				t.Errorf("[userservice.NewPkgUser()] got result %v, want result %v", gotRes, tc.wantRes)
			}
		})
	}
}

func Test_RegisterUser(t *testing.T) {
	type args struct {
		email    string
		password string
	}

	testCases := []struct{
		name     string
		args     args
		wantErr  bool
		wantRes  UserResponse
		mockFunc func()
	}{
		{
			name: "valid",
			args: args{
				"newemail@email.com",
				"password",
			},
			wantErr: false,
			wantRes: UserResponse{
				Token: "token",
				UserData: &UserDetail{
					ID: 1,
					Email: "newemail@email.com",
				},
			},
			mockFunc: func() {
				input := repository.CreateUserInput{
					Email: "newemail@email.com",
					Password: "password",
				}
				mockCrypto.EXPECT().HashPassword("password").Return("password", nil)
				mockUser.EXPECT().GetUserByEmail("newemail@email.com").Return(nil, nil)
				mockUser.EXPECT().CreateUser(input).Return(int16(1), nil)
				mockCrypto.EXPECT().GenerateJWTToken(int16(1)).Return("token", nil)
				mockRedis.EXPECT().Setex("token", strconv.Itoa(int(1)), gomock.Any()).Return(nil)
			},
		},
		{
			name: "invalid - error hash password",
			args: args{
				"newemail@email.com",
				"password",
			},
			wantErr: true,
			wantRes: UserResponse{},
			mockFunc: func() {
				mockCrypto.EXPECT().HashPassword("password").Return("password", errors.New("error"))
			},
		},
		{
			name: "invalid - error create user",
			args: args{
				"newemail@email.com",
				"password",
			},
			wantErr: true,
			wantRes: UserResponse{},
			mockFunc: func() {
				input := repository.CreateUserInput{
					Email: "newemail@email.com",
					Password: "password",
				}
				mockCrypto.EXPECT().HashPassword("password").Return("password", nil)
				mockUser.EXPECT().GetUserByEmail("newemail@email.com").Return(nil, nil)
				mockUser.EXPECT().CreateUser(input).Return(int16(1), errors.New("error"))
			},
		},
		{
			name: "invalid - error generate token",
			args: args{
				"newemail@email.com",
				"password",
			},
			wantErr: true,
			wantRes: UserResponse{},
			mockFunc: func() {
				input := repository.CreateUserInput{
					Email: "newemail@email.com",
					Password: "password",
				}
				mockCrypto.EXPECT().HashPassword("password").Return("password", nil)
				mockUser.EXPECT().GetUserByEmail("newemail@email.com").Return(nil, nil)
				mockUser.EXPECT().CreateUser(input).Return(int16(1), nil)
				mockCrypto.EXPECT().GenerateJWTToken(int16(1)).Return("", errors.New("error"))
			},
		},
		{
			name: "invalid - error get user by email",
			args: args{
				"newemail@email.com",
				"password",
			},
			wantErr: true,
			wantRes: UserResponse{},
			mockFunc: func() {
				mockCrypto.EXPECT().HashPassword("password").Return("password", nil)
				mockUser.EXPECT().GetUserByEmail("newemail@email.com").Return(nil, errors.New("error"))
			},
		},
		{
			name: "invalid - email already registered",
			args: args{
				"newemail@email.com",
				"password",
			},
			wantErr: true,
			wantRes: UserResponse{},
			mockFunc: func() {
				mockCrypto.EXPECT().HashPassword("password").Return("password", nil)
				mockUser.EXPECT().GetUserByEmail("newemail@email.com").Return(&repository.UserData{}, nil)
			},
		},
		{
			name: "invalid - wrong email format",
			args: args{
				"abcd@gmail_yahoo.com",
				"password",
			},
			wantErr: true,
			wantRes: UserResponse{},
			mockFunc: func() {},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			initMock(t)
			tc.mockFunc()
			gotRes, err := pkg.RegisterUser(tc.args.email, tc.args.password)
			if !reflect.DeepEqual(gotRes, tc.wantRes) {
				t.Errorf("[userservice.RegisterUser()] got result %v, want result %v", gotRes, tc.wantRes)
			}
			if (err != nil) != tc.wantErr {
				t.Errorf("[userservice.RegisterUser()] got error %v, want error %v", err, tc.wantErr)
			}
		})
	}
}

func Test_LoginUser(t *testing.T) {
	type args struct {
		email    string
		password string
	}
	testCases := []struct{
		name     string
		args     args
		wantErr  bool
		wantRes  UserResponse
		mockFunc func()
	}{
		{
			name: "valid",
			args: args{
				email:    "newemail",
				password: "password",
			},
			wantErr: false,
			wantRes: UserResponse{
				Token: "token",
				UserData: &UserDetail{
					ID:    1,
					Email: "newemail",
				},
			},
			mockFunc: func(){
				mockUser.EXPECT().GetUserByEmail("newemail").Return(&repository.UserData{
					ID:       1,
					Email:    "newemail",
					Password: "hashpassword",
				}, nil)
				mockCrypto.EXPECT().CheckPassword("hashpassword", "password").Return(true)
				mockCrypto.EXPECT().GenerateJWTToken(int16(1)).Return("token", nil)
				mockRedis.EXPECT().Setex("token", strconv.Itoa(int(1)), gomock.Any()).Return(nil)
			},
		},
		{
			name: "invalid - error get user by email",
			args: args{
				email:    "newemail",
				password: "password",
			},
			wantErr: true,
			wantRes: UserResponse{},
			mockFunc: func() {
				mockUser.EXPECT().GetUserByEmail("newemail").Return(nil, errors.New("error"))
			},
		},
		{
			name: "invalid - user doesn't exist",
			args: args{
				email:    "newemail",
				password: "password",
			},
			wantErr: true,
			wantRes: UserResponse{},
			mockFunc: func() {
				mockUser.EXPECT().GetUserByEmail("newemail").Return(nil, nil)
			},
		},
		{
			name: "invalid - password not valid",
			args: args{
				email:    "newemail",
				password: "password",
			},
			wantErr: true,
			wantRes: UserResponse{},
			mockFunc: func() {
				mockUser.EXPECT().GetUserByEmail("newemail").Return(&repository.UserData{
					ID:       1,
					Email:    "newemail",
					Password: "hashpassword",
				}, nil)
				mockCrypto.EXPECT().CheckPassword("hashpassword", "password").Return(false)
			},
		},
		{
			name: "invalid - generate token failed",
			args: args{
				email:    "newemail",
				password: "password",
			},
			wantErr: true,
			wantRes: UserResponse{},
			mockFunc: func() {
				mockUser.EXPECT().GetUserByEmail("newemail").Return(&repository.UserData{
					ID:       1,
					Email:    "newemail",
					Password: "hashpassword",
				}, nil)
				mockCrypto.EXPECT().CheckPassword("hashpassword", "password").Return(true)
				mockCrypto.EXPECT().GenerateJWTToken(int16(1)).Return("", errors.New("error"))
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			initMock(t)
			tc.mockFunc()
			gotRes, err := pkg.LoginUser(tc.args.email, tc.args.password)
			if !reflect.DeepEqual(gotRes, tc.wantRes) {
				t.Errorf("[userservice.LoginUser()] got result %v, want result %v", gotRes, tc.wantRes)
			}
			if (err != nil) != tc.wantErr {
				t.Errorf("[userservice.LoginUser()] got error %v, want error %v", err, tc.wantErr)
			}
		})
	}
}

func Test_GetUserByID(t *testing.T) {
	testCases := []struct{
		name     string
		id       int16
		wantErr  bool
		wantRes  UserDetail
		mockFunc func()
	}{
		{
			name: "valid",
			id: 1,
			wantErr: false,
			wantRes: UserDetail{
				ID:    1,
				Email: "newemail",
			},
			mockFunc: func() {
				mockUser.EXPECT().GetUserByID(int16(1)).Return(&repository.UserData{
					ID:       1,
					Email:    "newemail",
					Password: "hashpassword",
				}, nil)
			},
		},
		{
			name: "invalid - error get user by id",
			id: 1,
			wantErr: true,
			wantRes: UserDetail{},
			mockFunc: func() {
				mockUser.EXPECT().GetUserByID(int16(1)).Return(&repository.UserData{}, errors.New("error"))
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			initMock(t)
			tc.mockFunc()
			gotRes, err := pkg.GetUserByID(tc.id)
			if !reflect.DeepEqual(gotRes, tc.wantRes) {
				t.Errorf("[userservice.GetUserByID()] got result %v, want result %v", gotRes, tc.wantRes)
			}
			if (err != nil) != tc.wantErr {
				t.Errorf("[userservice.GetUserByID()] got error %v, want error %v", err, tc.wantErr)
			}
		})
	}
}