package service
//go:generate mockgen --source=repository/serviceuser.go --destination=mocks/service/mock_serviceuser.go

import (
	"errors"
	"regexp"
	"strconv"
	"time"

	"gitlab.com/decapos/termlink/crypto"
	"gitlab.com/decapos/termlink/logging"
	repo "gitlab.com/decapos/termlink/repository"
)

type (
	// PkgUser is an interface represent this package
	PkgUser interface {
		RegisterUser(email, password string) (UserResponse, error)
		LoginUser(email, password string) (UserResponse, error)
		GetUserByID(id int16) (UserDetail, error)
	}

	pkgUser struct{}
)

// UserResponse return response after register or login user
type UserResponse struct {
	Token    string      `json:"token"`
	UserData *UserDetail `json:"user_data"`
}

// UserDetail is the detail of user info
type UserDetail struct {
	ID     int16  `json:"id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Phone  string `json:"phone_number"`
	Gender string `json:"gender"`
}

// NewPkgUser for create an interface for this package
func NewPkgUser() PkgUser {
	if redis == nil {
		redis = repo.GetRedis()
	}

	if userRepo == nil {
		userRepo = repo.GetUserRepo()
	}

	if pkgCyrpto == nil {
		pkgCyrpto = crypto.New()
	}

	return &pkgUser{}
}

// RegisterUser register new user
func (p *pkgUser) RegisterUser(email, password string) (UserResponse, error) {
	res := UserResponse{}

	// check email regex
	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if !re.MatchString(email) {
		return res, errors.New("wrong email format")
	}

	hashPassword, err := pkgCyrpto.HashPassword(password)
	if err != nil {
		logging.GetLogger().Errorf("[service.RegisterUser] error hash password: %+v", err)
		return res, errors.New("error hashing password")
	}

	// check if user already registered before
	user, err := userRepo.GetUserByEmail(email)
	if err != nil {
		logging.GetLogger().Errorf("[service.RegisterUser] error get user by email: %+v", err)
		return res, errors.New("failed get user by email")
	}

	if user != nil {
		return res, errors.New("email already registered")
	}

	input := repo.CreateUserInput{
		Email: email,
		Password: hashPassword,
	}

	id, err := userRepo.CreateUser(input)
	if err != nil {
		logging.GetLogger().Errorf("[service.RegisterUser] error create user: %+v", err)
		return res, errors.New("failed create user")
	}

	token, err := pkgCyrpto.GenerateJWTToken(id)
	if err != nil {
		logging.GetLogger().Errorf("[service.RegisterUser] error generate jwt token id: %d, err: %+v", id, err)
		return res, errors.New("failed generate token")
	}

	// set to redis (key: token, value: id)
	_ = redis.Setex(token, strconv.Itoa(int(id)), 30*24*time.Hour)

	res.Token = token
	res.UserData = &UserDetail{
		ID:    id,
		Email: email,
	}

	return res, nil
}

// LoginUser check user credential when login
func (p *pkgUser) LoginUser(email, password string) (UserResponse, error) {
	res := UserResponse{}
	user, err := userRepo.GetUserByEmail(email)
	if err != nil {
		logging.GetLogger().Errorf("[service.LoginUser] error get user by email: %s", err.Error())
		return res, errors.New("error get user by email")
	}

	if user == nil {
		return res, errors.New("user_not_exist")
	}

	// check password
	valid := pkgCyrpto.CheckPassword(user.Password, password)
	if !valid {
		return res, errors.New("wrong_password")
	}

	token, err := pkgCyrpto.GenerateJWTToken(user.ID)
	if err != nil {
		logging.GetLogger().Errorf("[service.LoginUser] error generate jwt token id: %d, err: %+v", user.ID, err)
		return res, errors.New("failed generate token")
	}

	// set to redis (key: token, value: id)
	_ = redis.Setex(token, strconv.Itoa(int(user.ID)), 30*24*time.Hour)
	res = UserResponse{
		Token: token,
		UserData: &UserDetail{
			ID:     user.ID,
			Name:   user.Name.String,
			Email:  user.Email,
			Phone:  user.Phone.String,
			Gender: user.Gender.String,
		},
	}

	return res, nil
}

func (p *pkgUser) GetUserByID(id int16) (UserDetail, error) {
	res := UserDetail{}

	user, err := userRepo.GetUserByID(id)
	if err != nil {
		logging.GetLogger().Errorf("[service.GetUserByID] error get user by id: %s", err.Error())
		return res, errors.New("error get user by id")
	}

	if user != nil {
		res = UserDetail{
			ID:     user.ID,
			Name:   user.Name.String,
			Email:  user.Email,
			Phone:  user.Phone.String,
			Gender: user.Gender.String,
		}
	}

	return res, nil
}