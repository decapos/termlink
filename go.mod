module gitlab.com/decapos/termlink

go 1.12

require (
	github.com/99designs/gqlgen v0.9.2
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-contrib/static v0.0.0-20190913125243-df30d4057ba1
	github.com/gin-gonic/gin v1.4.0
	github.com/golang/mock v1.3.1
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/json-iterator/go v1.1.8 // indirect
	github.com/lib/pq v1.2.0
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/rafaeljusto/redigomock v0.0.0-20191016070255-2f957ca2c86c
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/ugorji/go v1.1.7 // indirect
	github.com/vektah/gqlparser v1.1.2
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/crypto v0.0.0-20191029031824-8986dd9e96cf
	golang.org/x/net v0.0.0-20191028085509-fe3aa8a45271 // indirect
	golang.org/x/sys v0.0.0-20191029155521-f43be2a4598c // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.2.4 // indirect
)
