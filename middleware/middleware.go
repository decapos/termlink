package middleware

import (
	"context"
	"net/http"
	"github.com/gin-gonic/gin"
	"log"

	"gitlab.com/decapos/termlink/repository"
)

const ginContextKey = "gin_context"
var redis repository.IRedis

// Middleware type will be executed before handler function
type Middleware func(gin.HandlerFunc) gin.HandlerFunc

// Chain applies middlewares to a http.HandlerFunc
func Chain(f gin.HandlerFunc, middlewares ...Middleware) gin.HandlerFunc {
	for _, m := range middlewares {
		f = m(f)
	}
	return f
}

// MiddlewareFunc example of middleware function
var MiddlewareFunc = func(next gin.HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		//FIXME code goes here
		log.Println("empty middleware")
		c.Next()
	}
}

func TestMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		//FIXME code goes here
		log.Println("empty middleware")
		c.Next()
	}
}

// Authorize is to check if user is authorized
func Authorize() gin.HandlerFunc {
	redis = repository.GetRedis()

  return func(c *gin.Context) {
		token := c.Request.Header.Get("Authorization")
		if token == "" {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		// get user id from cookie
		cookieToken, _ := c.Cookie("token")
		if token == "" {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		if cookieToken != token {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		id, _ := redis.Get(token)
		if string(id) == "" {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		c.Set("user_id", id)
    c.Next()
  }
}

// GinContextToContextMiddleware inject gin context to request
func GinContextToContextMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// get token from cookie
		token, _ := c.Cookie("token")
		c.Set("token", token)

		// set gin context in to context in request
		ctx := context.WithValue(c.Request.Context(), ginContextKey, c)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}