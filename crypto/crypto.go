package crypto

// ICrypto interface of this package
type ICrypto interface {
	HashPassword(password string) (string, error)
	CheckPassword(hashPassword, password string) bool
	GenerateJWTToken(userID int16) (string, error)
	VerifyToken(tokenString string, key []byte) (int16, bool)
}

type crypto struct{}

// New create new crypto package
func New() ICrypto {
	return &crypto{}
}
