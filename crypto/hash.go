package crypto

import (
	"gitlab.com/decapos/termlink/logging"
	"golang.org/x/crypto/bcrypt"
)

var COST_FACTOR = 10

// HashPassword will hashing plain password
func (c *crypto) HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), COST_FACTOR)
	if err != nil {
		logging.GetLogger().Errorf("[HashPassword] error generate from password: %s", err.Error())
		return "", err
	}

	return string(bytes), nil
}

// CheckPassword checking credibility by comparing hash password with plaing password
func (c *crypto) CheckPassword(hashPassword, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashPassword), []byte(password))

	return err == nil
}
