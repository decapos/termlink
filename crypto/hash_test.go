package crypto

import (
	"testing"
)

func Test_HashPassword(t *testing.T) {
	testCases := []struct{
		name     string
		password string
		wantErr  bool
		mockFunc func()
	}{
		{
			name: "valid",
			password: "password",
			wantErr: false,
			mockFunc: func() {},
		},
		{
			name: "invalid",
			password: "password",
			wantErr: true,
			mockFunc: func() {
				COST_FACTOR = 33
			},
		},
	}

	for _, tc := range testCases {
		pkg := New()
		t.Run(tc.name, func(t *testing.T) {
			tc.mockFunc()
			_, err := pkg.HashPassword(tc.password)
			if (err != nil) != tc.wantErr {
				t.Errorf("[crypto.HashPassword()] got error %v, want error %v", err, tc.wantErr)
			}
		})
	}
}

func Test_CheckPassword(t *testing.T) {
	testCases := []struct{
		name     string
		password string
		hashPassword string
		wantRes  bool
	}{
		{
			name: "valid - correct password",
			password: "newpassword",
			hashPassword: "$2a$10$AjPQkqUyzWdCUtSjiJOkfuipnNvMhC8b3FRsoKI/hFTDIUS3FW8Te",
			wantRes: true,
		},
		{
			name: "valid - incorrect password",
			password: "password",
			hashPassword: "hash",
			wantRes: false,
		},
	}

	for _, tc := range testCases {
		pkg := New()
		t.Run(tc.name, func(t *testing.T) {
			res := pkg.CheckPassword(tc.hashPassword, tc.password)
			if res != tc.wantRes {
				t.Errorf("[crypto.CheckPassword()] got result %v, want result %v", res, tc.wantRes)
			}
		})
	}
}