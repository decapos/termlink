package crypto

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/decapos/termlink/logging"
)

const (
	SUBJECT  = "authorization token"
	ISSUER   = "deca security"
	AUDIENCE = "termlink"
)

// JWTClaim custom claim struct
type JWTClaim struct {
	UserID int16 `json:"user_id"`
	jwt.StandardClaims
}

// GenerateJWTToken will generate token with custom claims
func (c *crypto) GenerateJWTToken(userID int16) (string, error) {
	//TODO: move key to some place safer
	key := []byte("supersecretkey")
	claims := &JWTClaim{
		UserID: userID,
		StandardClaims: jwt.StandardClaims{
			Subject:   SUBJECT,
			Issuer:    ISSUER,
			Audience:  AUDIENCE,
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(time.Minute * 30).Unix(), //TODO: put timeout in config
		},
	}

	token, err := claimsToToken(key, claims)
	if err != nil {
		logging.GetLogger().Errorf("[claimsToToken] failed: %s", err.Error())
		return "", err
	}

	return token, err
}

func claimsToToken(key []byte, claims jwt.Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(key)
	if err != nil {
		logging.GetLogger().Errorf("[SignedString] claims: %+v, failed: %s", claims, err.Error())
		return "", err
	}
	return tokenString, nil
}

// VerifyToken to verifying token from request
func (c *crypto) VerifyToken(tokenString string, key []byte) (int16, bool) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return key, nil
	})

	if err != nil {
		return 0, false
	}

	claims, ok := token.Claims.(*JWTClaim)
	if !ok || !token.Valid {
		return 0, false
	}

	return claims.UserID, true
}
