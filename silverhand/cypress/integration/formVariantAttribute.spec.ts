describe("FormVariantAttribute component test", () => {
  beforeEach(() => {
    cy.visit(
      "http://localhost:6006/iframe.html?id=complex-forms-formvariantattribute--basic"
    )
    cy.injectAxe()
    cy.get(".sb-show-main")
  })

  it("should have working name input field is working", () => {
    cy.get("[data-cy=typeOfVariant]")
      .find("input")
      .should("have.value", "")

    cy.get("[data-cy=typeOfVariant]")
      .type("Color")
      .find("input")
      .should("have.value", "Color")
  })

  it("should have working option field", () => {
    cy.get("[data-cy=variantOptionField]")
      .find("input")
      .should("have.value", "")

    cy.get("[data-cy=variantOptionField]")
      .type("White")
      .find("input")
      .should("have.value", "White")
  })

  it("should save variant option when enter pressed", () => {
    cy.contains("white").should("have.length", 0)

    cy.get("[data-cy=variantOptionField]")
      .type("white")
      .type("{enter}")
      .find("input")
      .should("have.value", "")

    cy.contains("white").should("have.length", 1)
  })

  it("should save variant option when add option button is pressed", () => {
    cy.contains("white").should("have.length", 0)

    cy.get("[data-cy=variantOptionField]")
      .type("white")
      .get("[data-cy=addOption]")
      .click()

    cy.contains("white").should("have.length", 1)
  })

  it("should be able to save multple variant option", () => {
    cy.contains("white").should("have.length", 0)
    cy.contains("Black").should("have.length", 0)
    cy.contains("Purple").should("have.length", 0)

    cy.get("[data-cy=variantOptionField]")
      .type("white")
      .get("[data-cy=addOption]")
      .click()

    cy.get("[data-cy=variantOptionField]")
      .type("Black")
      .get("[data-cy=addOption]")
      .click()

    cy.get("[data-cy=variantOptionField]")
      .type("Purple")
      .get("[data-cy=addOption]")
      .click()

    cy.contains("white").should("have.length", 1)
    cy.contains("Black").should("have.length", 1)
    cy.contains("Purple").should("have.length", 1)
  })
})
