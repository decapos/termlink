describe("PageHome component test", () => {
  beforeEach(() => {
    cy.visit("http://localhost:6006/iframe.html?id=pages-pagehome--basic")
    cy.injectAxe()
  })

  it("tabs should be functional", () => {
    cy.viewport("iphone-6")
    cy.contains("Activities").click()
    cy.get("[data-cy=overview]").should("not.be.visible")
    // TODO: enable this again after moving from frappe to d3
    //  currently fails cause frappe is buggy.
    // cy.checkA11y()

    cy.contains("Overview").click()
    cy.get("[data-cy=overview]").should("visible")
    // TODO: enable this again after moving from frappe to d3
    //  currently fails cause frappe is buggy.
    // cy.checkA11y()
  })
})
