describe("PageNewItem component test", () => {
  beforeEach(() => {
    cy.visit("http://localhost:6006/iframe.html?id=pages-pagenewitem--basic")
    cy.injectAxe()
    cy.get(".sb-show-main")
  })

  it("light mode should be accessible", () => {
    cy.checkA11y()
  })

  it("dark mode should be accessible", () => {
    cy.get("[data-cy=toggleTheme]").click()
    cy.checkA11y()
  })

  it("should disable save when form is incomplete", () => {
    cy.get("[data-cy=name")
      .find("input")
      .should("be.empty")
    cy.get("[data-cy=save]").should("be.disabled")
  })
})
