describe("Layout component test", () => {
  beforeEach(() => {
    cy.visit("http://localhost:6006/iframe.html?id=basic-layout--basic")
  })

  it("should have a functional sidebar on mobile", () => {
    cy.viewport("iphone-6")
    cy.get("[data-cy=sidebar").should("not.be.visible")
    cy.get("[data-cy=menu]").click()
    cy.get("[data-cy=sidebar").should("be.visible")
  })

  it("should show sidebar on tablet and desktop", () => {
    cy.viewport("macbook-13")
    cy.get("[data-cy=sidebar").should("be.visible")
    cy.get("[data-cy=menu]").should("not.be.visible")
  })
})
