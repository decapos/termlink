---
to: src/__mocks__/apollo/<%= name%>.ts
---
import { ApolloMock } from "./index"
import { <%= type%> } from "../../apollo/graphqlTypes"

const <%= name%>: ApolloMock<<%= type%>> = {
  request: {
    query: <%= query%>
  },
  result: {
    data: {

    }
  }
}

export default <%= name%>
