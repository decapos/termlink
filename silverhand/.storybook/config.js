import { addDecorator, addParameters, configure } from "@storybook/react"
import React from "react"
import { withKnobs } from "@storybook/addon-knobs"
import { action } from "@storybook/addon-actions"
import { create } from "@storybook/theming"
import "@storybook/addon-console"
import { setIntlConfig, withIntl } from "storybook-addon-intl"
import { INITIAL_VIEWPORTS } from "@storybook/addon-viewport/"
import { jsxDecorator } from "storybook-addon-jsx"
import { withA11y } from "@storybook/addon-a11y"
import { ThemeProvider } from "theme-ui"
import SilverhandTheme from "../src/gatsby-plugin-theme-ui"
import { Box } from "../src/components/Box/Box"
import { MockedProvider } from "@apollo/react-testing"
import apolloMock from "../src/__mocks__/apollo"
import "../src/global.css"
import en from "../src/intl/en"
import id from "../src/intl/id"

// Register withKnobs Addon to be able to change component props on the fly
addDecorator(withKnobs)
addDecorator(jsxDecorator)

// Prepare react-intl
// addLocaleData(enLocaleData);
// addLocaleData(idLocaleData);

const messages = { en, id }

const getMessages = locale => messages[locale]

setIntlConfig({
  locales: ["en", "id"],
  defaultLocale: "en",
  getMessages
})

addDecorator(withIntl)

// find stories.
const reqComponents = require.context(
  "../src",
  true,
  /^((?!styles).)*\.stories\.tsx$/
)
const reqIntroduction = require.context(
  "../src/storybook",
  true,
  /\.stories\.mdx$/
)
const reqStyles = require.context(
  "../src/components",
  true,
  /\.styles\.stories\.tsx$/
)

// Setup Typography js
const ThemeDecorator = story => (
  <ThemeProvider theme={SilverhandTheme}>
    <Box backgroundColor="background" fontSize={[16, 20]}>{story()}</Box>
  </ThemeProvider>
)
addDecorator(ThemeDecorator)

// Setup Apollo mock
const ApolloDecorator = story => (
  <MockedProvider  mocks={apolloMock} addTypename={false}>
    {story()}
  </MockedProvider>
)
addDecorator(ApolloDecorator)

// Setup Custom theme and storybook options
const theme = create({
  base: "dark",
  appBg: "#1d1d1d",
  barBg: "#222222",
  brandUrl: "https://decapos.io",
  brandName: "SILVERHAND",
})

addParameters({
  viewport: {
    viewports: {
      ...INITIAL_VIEWPORTS
    }
  },
  backgrounds: [
    { name: "light", value: "#F7F8FA", default: true },
    { name: "dark", value: "#121212" }
  ],
  options: {
    theme: theme
    // isFullscreen: false,
    // isToolShown: true,
    // panelPosition: "right",
    // showPanel: true
  }
})

// Added after all other decorator so it has full view of the preview.
addDecorator(withA11y)

// Make gatsby components work on storybook.
// Gatsby's Link overrides:
// Gatsby defines a global called ___loader to prevent its method calls from creating console errors you override it here
global.___loader = {
  enqueue: () => {},
  hovering: () => {}
}
// Gatsby internal mocking to prevent unnecessary errors in storybook testing environment
global.__PATH_PREFIX__ = ""
// This is to utilized to override the window.___navigate method Gatsby defines and uses to report what path a Link would be taking us to if it wasn't inside a storybook
window.___navigate = pathname => {
  action("NavigateTo:")(pathname)
}

configure([reqIntroduction, reqStyles, reqComponents], module)
