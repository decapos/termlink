import React, { FC } from "react"
import Layout from "../components/Layout/Layout"
import PageInventory from "../components/PageInventory/PageInventory"
import SEO from "../components/seo"
import { useFormatMessage } from "../components/hooks"

const Inventory: FC = () => (
  <Layout pageTitleIntlId="inventory">
    <SEO title={useFormatMessage("manageInventory")} />
    <PageInventory />
  </Layout>
)

export default Inventory
