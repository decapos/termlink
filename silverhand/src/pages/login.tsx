import React, { FC } from "react"
import PageLogin from "../components/PageLogin/PageLogin"
import SEO from "../components/seo"
import { useFormatMessage } from "../components/hooks"

const Login: FC = () => (
  <>
    <SEO title={useFormatMessage("login")} />
    <PageLogin />
  </>
)

export default Login
