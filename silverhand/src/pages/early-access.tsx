import React, { FC } from "react"
import PageEarlyAccess from "../components/PageEarlyAccess/PageEarlyAccess"
import SEO from "../components/seo"
import { useFormatMessage } from "../components/hooks"

const EarlyAccess: FC = () => (
  <>
    <SEO title={useFormatMessage("requestEarlyAccess")} />
    <PageEarlyAccess />
  </>
)

export default EarlyAccess
