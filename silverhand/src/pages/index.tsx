import React, { FC } from "react"
import PageHome from "../components/PageHome/PageHome"
import Layout from "../components/Layout/Layout"
import SEO from "../components/seo"
import { useFormatMessage } from "../components/hooks"

const IndexPage: FC = () => (
  <Layout pageTitleIntlId="home">
    <SEO title={useFormatMessage("home")} />
    <PageHome />
  </Layout>
)

export default IndexPage
