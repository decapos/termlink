import React, { FC } from "react"
import PageForgotPassword from "../components/PageForgotPassword/PageForgotPassword"
import SEO from "../components/seo"
import { useFormatMessage } from "../components/hooks"

const ForgotPassword: FC = () => {
  return (
    <>
      <SEO title={useFormatMessage("forgotPassword")} />
      <PageForgotPassword />
    </>
  )
}

export default ForgotPassword
