import React, { FC } from "react"
import Layout from "../../components/Layout/Layout"
import SEO from "../../components/seo"
import { useFormatMessage } from "../../components/hooks"
import PageNewItem from "../../components/PageNewItem/PageNewItem"

const NewItem: FC = () => (
  <Layout pageTitleIntlId="newItem">
    <SEO title={useFormatMessage("newItem")} />
    <PageNewItem />
  </Layout>
)

export default NewItem
