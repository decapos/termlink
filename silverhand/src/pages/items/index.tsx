import React, { FC } from "react"
import Layout from "../../components/Layout/Layout"
import PageItems from "../../components/PageItems/PageItems"
import SEO from "../../components/seo"
import { useFormatMessage } from "../../components/hooks"

const Index: FC = () => (
  <Layout pageTitleIntlId="items">
    <SEO title={useFormatMessage("manageItems")} />
    <PageItems />
  </Layout>
)

export default Index
