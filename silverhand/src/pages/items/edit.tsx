import React, { FC } from "react"
import { PageRendererProps } from "gatsby"
import queryString from "query-string"
import Layout from "../../components/Layout/Layout"
import SEO from "../../components/seo"
import { useFormatMessage } from "../../components/hooks"
import PageEditItem from "../../components/PageEditItem/PageEditItem"

const EditItem: FC<PageRendererProps> = ({ location }) => {
  const query = queryString.parse(location.search)

  // TODO: If query parameter is invalid, we should show an error page instead
  let id: string
  if (Array.isArray(query?.id)) {
    id = query.id[0] ?? ""
  } else {
    id = query?.id ?? ""
  }

  return (
    <Layout pageTitleIntlId="editItem">
      <SEO title={useFormatMessage("editItem")} />
      <PageEditItem id={id} />
    </Layout>
  )
}

export default EditItem
