import React, { FC } from "react"
import Layout from "../components/Layout/Layout"
import SEO from "../components/seo"
import { useFormatMessage } from "../components/hooks"
import PagePos from "../components/PagePos/PagePos"

const Inventory: FC = () => (
  <Layout pageTitleIntlId="inventory">
    <SEO title={useFormatMessage("manageInventory")} />
    <PagePos />
  </Layout>
)

export default Inventory
