import React, { FC } from "react"
import SEO from "../components/seo"
import { useFormatMessage } from "../components/hooks"

const NotFoundPage: FC = () => (
  <>
    <SEO title={useFormatMessage("404")} />
    <main>404 !!!!! AAAAAAAAAA</main>
  </>
)

export default NotFoundPage
