import React, { FC } from "react"
import PageSettings from "../components/PageSettings/PageSettings"
import Layout from "../components/Layout/Layout"
import SEO from "../components/seo"
import { useFormatMessage } from "../components/hooks"

const Settings: FC = () => (
  <Layout pageTitleIntlId="settings">
    <SEO title={useFormatMessage("settings")} />
    <PageSettings />
  </Layout>
)

export default Settings
