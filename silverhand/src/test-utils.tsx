/* eslint-disable import/no-extraneous-dependencies */
import React, { FC, ReactElement } from "react"
import { ThemeProvider } from "theme-ui"
import { MockedProvider } from "@apollo/react-testing"
import { render, RenderOptions, RenderResult } from "@testing-library/react"
import { IntlProvider } from "gatsby-plugin-intl3"
import apolloMock from "./__mocks__/apollo"
import SilverhandTheme from "./gatsby-plugin-theme-ui"
import en from "./intl/en.json"
import Box from "./components/Box/Box"

const AllProviders: FC = ({ children }) => (
  <ThemeProvider theme={SilverhandTheme}>
    <IntlProvider locale="en" messages={en}>
      <Box fontSize={[16, 20]}>
        <MockedProvider mocks={apolloMock} addTypename={false}>
          <>
            {/* TODO:remove later, this is a workaround for @types/react bug #18051 */}
            {children}
          </>
        </MockedProvider>
      </Box>
    </IntlProvider>
  </ThemeProvider>
)

const customRender = (
  ui: ReactElement,
  options?: Omit<RenderOptions, "queries">
): RenderResult => render(ui, { wrapper: AllProviders, ...options })

// re-export everything
export * from "@testing-library/react"

// override render method
export { customRender as render }
