import { GET_SALES } from "../../components/CardTotalSales/CardTotalSales"
import { SalesOverviewQuery } from "../../apollo/graphqlTypes"
import { ApolloMock } from "./index"

const getSalesMock: ApolloMock<SalesOverviewQuery> = {
  request: {
    query: GET_SALES
  },
  result: {
    data: {
      report: {
        sales: {
          summaries: [
            {
              time: "Monday",
              totalValue: 20000
            },
            {
              time: "Tuesday",
              totalValue: 30000
            },
            {
              time: "Wednesday",
              totalValue: 50000
            },
            {
              time: "Thursday",
              totalValue: 70000
            },
            {
              time: "Friday",
              totalValue: 30000
            },
            {
              time: "Saturday",
              totalValue: 90000
            },
            {
              time: "Sunday",
              totalValue: 10000
            }
          ]
        }
      }
    }
  }
}

export default getSalesMock
