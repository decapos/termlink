// eslint-disable-next-line import/no-extraneous-dependencies
import faker from "faker"
import { ApolloMock } from "./index"
import { ItemEdge, ItemsQuery } from "../../apollo/graphqlTypes"
import { GET_ITEMS } from "../../components/PageItems/PageItems"
import artImage from "../../images/pawel-czerwinski-yRctKX99woU-unsplash.jpg"

export const generateItems = (seed: number): ItemEdge => {
  faker.seed(seed)
  return {
    cursor: seed.toString(),
    node: {
      id: seed.toString(),
      isListed: faker.random.boolean(),
      name: faker.commerce.product(),
      imagePath: artImage,
      price: Math.ceil(parseInt(faker.commerce.price(100000), 10) / 100) * 100,
      itemRequirements: [],
      measurementUnit: {
        id: "1",
        name: "ml"
      },
      tags: [],
      variantItems: []
    }
  }
}

const getItemsMock: ApolloMock<ItemsQuery> = {
  request: {
    query: GET_ITEMS,
    variables: { searchTerm: "" }
  },
  result: () => ({
    data: {
      items: {
        pageInfo: {
          startCursor: "0",
          endCursor: "10",
          hasNextPage: true,
          hasPreviousPage: false
        },
        edges: [
          generateItems(1),
          generateItems(2),
          generateItems(3),
          generateItems(4),
          generateItems(5),
          generateItems(6),
          generateItems(7),
          generateItems(8),
          generateItems(9)
        ]
      }
    }
  })
}

export default getItemsMock
