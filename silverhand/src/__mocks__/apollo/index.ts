/* eslint-disable import/no-extraneous-dependencies */
import { MockedResponse, ResultFunction } from "@apollo/react-testing"
import { FetchResult } from "apollo-boost"
import getSalesMock from "./getSalesMock"
import getPopularProductMock from "./getPopularProductMock"
import getActivitiesMock from "./getActivitiesMock"
import getCompanyNameMock from "./getCompanyNameMock"
import getUserDataMock from "./getUserDataMock"
import getItemsMock from "./getItemsMock"
import getItemNamesMock from "./getItemNamesMock"
import getStoreNamesMock from "./getStoreNamesMock"

export interface ApolloMock<T> extends MockedResponse {
  result: FetchResult<T> | ResultFunction<FetchResult<T>>
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const apolloMock: ReadonlyArray<MockedResponse> = [
  getSalesMock,
  getPopularProductMock,
  getActivitiesMock,
  getCompanyNameMock,
  getUserDataMock,
  getItemsMock,
  getItemNamesMock,
  getStoreNamesMock
]

export default apolloMock
