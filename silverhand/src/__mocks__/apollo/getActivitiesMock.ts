import { ApolloMock } from "./index"
import { ActivitiesQuery } from "../../apollo/graphqlTypes"
import { GET_ACTIVITIES } from "../../components/Activities/Activities"

const getActivitiesMock: ApolloMock<ActivitiesQuery> = {
  request: {
    query: GET_ACTIVITIES
  },
  result: {
    data: {
      company: {
        activities: [
          {
            action: "added a new product",
            actor: "Samuel",
            timestamp: "2019-11-26T16:53:04Z",
            store: {
              name: "Kebon Jeruk HQ"
            }
          }
        ]
      }
    }
  }
}

export default getActivitiesMock
