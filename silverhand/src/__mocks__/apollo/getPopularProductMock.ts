import { ApolloMock } from "./index"
import { PopularItemOverviewQuery } from "../../apollo/graphqlTypes"
import { GET_POPULAR_PRODUCT } from "../../components/CardPopularProducts/CardPopularProducts"

const getPopularProductMock: ApolloMock<PopularItemOverviewQuery> = {
  request: {
    query: GET_POPULAR_PRODUCT
  },
  result: {
    data: {
      report: {
        item: {
          itemPerformances: [
            { label: "Trophy", salesCount: 200 },
            { label: "Trop", salesCount: 500 },
            { label: "Pie", salesCount: 20 },
            { label: "Fidelius", salesCount: 240 },
            { label: "Trucktor", salesCount: 390 },
            { label: "Truckla", salesCount: 290 },
            { label: "Chicken", salesCount: 100 },
            { label: "Later", salesCount: 70 },
            { label: "Others", salesCount: 10 }
          ]
        }
      }
    }
  }
}

export default getPopularProductMock
