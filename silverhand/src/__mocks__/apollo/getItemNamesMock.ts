import nanoid from "nanoid"
import { GET_ITEM_NAMES } from "../../components/DialogItemRequirementPicker/DialogItemRequirementPicker"
import { ApolloMock } from "./index"
import { ItemNamesQuery } from "../../apollo/graphqlTypes"

const getItemNamesMock: ApolloMock<ItemNamesQuery> = {
  request: {
    query: GET_ITEM_NAMES
  },
  result: {
    data: {
      items: {
        edges: [
          {
            node: {
              id: nanoid(),
              name: "item one",
              price: 1200,
              measurementUnit: {
                id: nanoid(),
                name: "kilogram",
                notation: "Kg"
              }
            }
          },
          {
            node: {
              id: nanoid(),
              name: "item two",
              price: 1200,
              measurementUnit: {
                id: nanoid(),
                name: "kilogram",
                notation: "Kg"
              }
            }
          },
          {
            node: {
              id: nanoid(),
              name: "item three",
              price: 1200,
              measurementUnit: {
                id: nanoid(),
                name: "kilogram",
                notation: "Kg"
              }
            }
          },
          {
            node: {
              id: nanoid(),
              name: "item four",
              price: 1200,
              measurementUnit: {
                id: nanoid(),
                name: "kilogram",
                notation: "Kg"
              }
            }
          },
          {
            node: {
              id: nanoid(),
              name: "item five",
              price: 1200,
              measurementUnit: {
                id: nanoid(),
                name: "kilogram",
                notation: "Kg"
              }
            }
          },
          {
            node: {
              id: nanoid(),
              name: "item six",
              price: 1200,
              measurementUnit: {
                id: nanoid(),
                name: "kilogram",
                notation: "Kg"
              }
            }
          },
          {
            node: {
              id: nanoid(),
              name: "item seven",
              price: 1200,
              measurementUnit: {
                id: nanoid(),
                name: "kilogram",
                notation: "Kg"
              }
            }
          },
          {
            node: {
              id: nanoid(),
              name: "item eight",
              price: 1200,
              measurementUnit: {
                id: nanoid(),
                name: "kilogram",
                notation: "Kg"
              }
            }
          },
          {
            node: {
              id: nanoid(),
              name: "item nine",
              price: 1200,
              measurementUnit: {
                id: nanoid(),
                name: "kilogram",
                notation: "Kg"
              }
            }
          },
          {
            node: {
              id: nanoid(),
              name: "item ten",
              price: 1200,
              measurementUnit: {
                id: nanoid(),
                name: "kilogram",
                notation: "Kg"
              }
            }
          }
        ]
      }
    }
  }
}

export default getItemNamesMock
