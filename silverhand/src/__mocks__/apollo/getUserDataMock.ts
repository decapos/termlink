import { ApolloMock } from "./index"
import { UserDataQuery } from "../../apollo/graphqlTypes"
import { GET_USER_DATA } from "../../components/AppBar/AppBar"

const getUserDataMock: ApolloMock<UserDataQuery> = {
  request: {
    query: GET_USER_DATA
  },
  result: {
    data: {
      user: {
        name: "Diane Kruger"
      }
    }
  }
}

export default getUserDataMock
