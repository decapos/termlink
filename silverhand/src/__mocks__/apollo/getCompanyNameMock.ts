import { ApolloMock } from "./index"
import { CompanyNameQuery } from "../../apollo/graphqlTypes"
import { GET_COMPANY_NAME } from "../../components/AppBar/AppBar"

const getCompanyNameMock: ApolloMock<CompanyNameQuery> = {
  request: {
    query: GET_COMPANY_NAME
  },
  result: {
    data: {
      company: {
        name: "Diary Queen"
      }
    }
  }
}

export default getCompanyNameMock
