import nanoid from "nanoid"
import { ApolloMock } from "./index"
import { StoreNamesQuery } from "../../apollo/graphqlTypes"
import { GET_STORE_NAMES } from "../../components/ItemEditor/useItemEditorState"

const getStoreNamesMock: ApolloMock<StoreNamesQuery> = {
  request: {
    query: GET_STORE_NAMES
  },
  result: {
    data: {
      company: {
        stores: {
          edges: [
            {
              node: {
                id: nanoid(),
                name: "Mall of Indonesia"
              }
            },
            {
              node: {
                id: nanoid(),
                name: "Mall of America"
              }
            },
            {
              node: {
                id: nanoid(),
                name: "Dakota"
              }
            },
            {
              node: {
                id: nanoid(),
                name: "Caribbean"
              }
            }
          ]
        }
      }
    }
  }
}

export default getStoreNamesMock
