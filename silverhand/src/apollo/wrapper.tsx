/* eslint-disable */
import React from "react"
import { ApolloProvider } from "@apollo/react-hooks"
import { createApolloClient } from "./client"

if (!Intl.PluralRules) {
  import("../polyfill/pluralRules")
}

// @ts-ignore
if (!Intl.RelativeTimeFormat) {
  import("../polyfill/relativeTimeFormat")
}

const client = createApolloClient()
export const wrapRootElement: (element: any) => any = ({ element }) => (
  <ApolloProvider client={client}>{element}</ApolloProvider>
)
