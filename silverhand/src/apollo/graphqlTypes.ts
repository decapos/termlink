export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  Cursor: string,
  Time: string,
  Upload: any,
};

export type Activity = {
   __typename?: 'Activity',
  timestamp: Scalars['Time'],
  /** The action being done */
  action: Scalars['String'],
  /** The source of the activity */
  actor: Scalars['String'],
  /** The store affected by the activity */
  store: Store,
};

/** 
 * ===============================================================
 * Copmany
 * ===============================================================
 **/
export type Company = {
   __typename?: 'Company',
  id: Scalars['ID'],
  name: Scalars['String'],
  stores: StoreConnection,
  paymentMethods?: Maybe<Array<PaymentMethod>>,
  items?: Maybe<Array<Item>>,
  activities?: Maybe<Array<Activity>>,
};

export type ConnectedDevice = {
   __typename?: 'ConnectedDevice',
  id: Scalars['ID'],
  store?: Maybe<Store>,
  name?: Maybe<Scalars['String']>,
  token?: Maybe<Scalars['String']>,
};

export type Connection = {
  pageInfo: PageInfo,
  edges: Array<Edge>,
};


export type Edge = {
  cursor: Scalars['Cursor'],
};

export type Expense = {
   __typename?: 'Expense',
  id: Scalars['ID'],
  amount?: Maybe<Scalars['Int']>,
  date: Scalars['Time'],
};

export type FixExpense = {
   __typename?: 'FixExpense',
  id: Scalars['ID'],
  startDate?: Maybe<Scalars['Time']>,
  endDate?: Maybe<Scalars['Time']>,
  amoount?: Maybe<Scalars['Int']>,
  /** FIXME: Monthly? Yearly? that kind of thing. Better name? */
  repetition?: Maybe<Scalars['Int']>,
};

export type Inventory = {
   __typename?: 'Inventory',
  id: Scalars['ID'],
  store?: Maybe<Store>,
  item?: Maybe<Item>,
  resupplyHistories?: Maybe<Array<ResupplyHistory>>,
  stock?: Maybe<Scalars['Int']>,
};

export type Item = {
   __typename?: 'Item',
  id: Scalars['ID'],
  name: Scalars['String'],
  /** 
 * TODO: Might be better to use something more robust than Int
   *   (eg. Shopify uses custom Money schema)
 **/
  price: Scalars['Int'],
  itemRequirements: Array<ItemRequirement>,
  /** Signify wheter the item is available for purchase */
  isListed: Scalars['Boolean'],
  variantItems?: Maybe<Array<Item>>,
  tags: Array<Tag>,
  /** FIXME: is there better name? */
  measurementUnit: MeasurementUnit,
  imagePath: Scalars['String'],
};

/** 
 * ===============================================================
 * Item
 * ===============================================================
 **/
export type ItemConnection = Connection & {
   __typename?: 'ItemConnection',
  pageInfo: PageInfo,
  edges: Array<ItemEdge>,
};

export type ItemEdge = Edge & {
   __typename?: 'ItemEdge',
  cursor: Scalars['Cursor'],
  node: Item,
};

/** Data about the performance of each items */
export type ItemPerformance = {
   __typename?: 'ItemPerformance',
  salesCount: Scalars['Int'],
  itemName: Scalars['String'],
  item: Item,
};

export type ItemRequirement = {
   __typename?: 'ItemRequirement',
  id: Scalars['ID'],
  item?: Maybe<Item>,
  requiredItem?: Maybe<Item>,
  amount?: Maybe<Scalars['Int']>,
};

export type MeasurementUnit = {
   __typename?: 'MeasurementUnit',
  id: Scalars['ID'],
  /** Name of the unit */
  name?: Maybe<Scalars['String']>,
  notation?: Maybe<Scalars['String']>,
};

export type Order = {
   __typename?: 'Order',
  id: Scalars['ID'],
  status?: Maybe<OrderStatus>,
  items?: Maybe<Array<Item>>,
  /** 
 * TODO: Might be better to use something more robust than Int
   *   (eg. Shopify uses custom Money schema)
 **/
  totalPrice?: Maybe<Scalars['Int']>,
  purchaseDate: Scalars['Time'],
  labels?: Maybe<Array<OrderLabels>>,
};

export type OrderLabels = {
   __typename?: 'OrderLabels',
  id: Scalars['ID'],
  name?: Maybe<Scalars['String']>,
  values?: Maybe<Array<Scalars['String']>>,
  selectedValue?: Maybe<Scalars['String']>,
};

/** Each outlet have different OrderStatus */
export type OrderStatus = {
   __typename?: 'OrderStatus',
  id: Scalars['ID'],
  name?: Maybe<Scalars['String']>,
  after?: Maybe<OrderStatus>,
  before?: Maybe<OrderStatus>,
  company?: Maybe<Company>,
};

export type PageInfo = {
   __typename?: 'PageInfo',
  hasNextPage: Scalars['Boolean'],
  hasPreviousPage: Scalars['Boolean'],
  startCursor: Scalars['Cursor'],
  endCursor: Scalars['Cursor'],
};

export type PaymentMethod = {
   __typename?: 'PaymentMethod',
  id: Scalars['ID'],
  store?: Maybe<Store>,
  token?: Maybe<Scalars['String']>,
  /** String? Another object on the schema? */
  type?: Maybe<Scalars['String']>,
};

export type Query = {
   __typename?: 'Query',
  company: Company,
  store: Store,
  user: User,
  orders: Array<Order>,
  report: Report,
  items: ItemConnection,
};


export type QueryItemsArgs = {
  filter?: Maybe<Scalars['String']>,
  first?: Maybe<Scalars['Int']>,
  after?: Maybe<Scalars['Cursor']>,
  last?: Maybe<Scalars['Int']>,
  before?: Maybe<Scalars['Cursor']>
};

export type Report = {
   __typename?: 'Report',
  /** Sales report of the business */
  sales: ReportSales,
  /** Reports regarding the performance of each product */
  item: ReportItem,
};


export type ReportSalesArgs = {
  timeRange?: Maybe<ReportTimeRange>
};


export type ReportItemArgs = {
  timeRange?: Maybe<ReportTimeRange>
};

export type ReportItem = {
   __typename?: 'ReportItem',
  timeRange: ReportTimeRange,
  itemPerformances: Array<ItemPerformance>,
};

export type ReportSales = {
   __typename?: 'ReportSales',
  timeRange: ReportTimeRange,
  /** Total sales in the given time range */
  totalSales: Scalars['Int'],
  /** All sales info in the given time range */
  summaries: Array<SalesSummary>,
};

/** 
 * ==================================================
 * All possible report data
 * =================================================
 **/
export enum ReportTimeRange {
  DAILY = 'DAILY',
  WEEKLY = 'WEEKLY',
  MONTHLY = 'MONTHLY',
  YEARLY = 'YEARLY'
}

export type ResupplyHistory = {
   __typename?: 'ResupplyHistory',
  id: Scalars['ID'],
  date?: Maybe<Scalars['Time']>,
  /** Again, int? custom Money object like shopify? */
  price?: Maybe<Scalars['Int']>,
  amount?: Maybe<Scalars['Int']>,
  supplier?: Maybe<Supplier>,
};

/** Successful transactions */
export type SalesSummary = {
   __typename?: 'SalesSummary',
  /** 
 * The time of the sales (can be day, hours or month depending of the time range, eg.
   * MONTHLY, WEEKLY, or DAILY)
 **/
  time: Scalars['String'],
  /** Total value involves in the sale */
  totalValue: Scalars['Int'],
  /** Order that is involved on the sales */
  orders: Array<Order>,
};

export type Store = {
   __typename?: 'Store',
  id: Scalars['ID'],
  name: Scalars['String'],
  address: Scalars['String'],
  expenses?: Maybe<Array<Expense>>,
  connectedDevice?: Maybe<Array<ConnectedDevice>>,
  fixExpenses?: Maybe<Array<FixExpense>>,
  inventory?: Maybe<Array<Inventory>>,
  orders?: Maybe<Array<Order>>,
};

export type StoreConnection = Connection & {
   __typename?: 'StoreConnection',
  pageInfo: PageInfo,
  edges: Array<StoreEdge>,
};

export type StoreEdge = Edge & {
   __typename?: 'StoreEdge',
  cursor: Scalars['Cursor'],
  node: Store,
};

export type Supplier = {
   __typename?: 'Supplier',
  id: Scalars['ID'],
  name?: Maybe<Scalars['String']>,
  type?: Maybe<Scalars['String']>,
  url?: Maybe<Scalars['String']>,
  address?: Maybe<Scalars['String']>,
  email?: Maybe<Scalars['String']>,
  phone?: Maybe<Scalars['String']>,
};

export type Tag = {
   __typename?: 'Tag',
  id: Scalars['ID'],
  name?: Maybe<Scalars['String']>,
  value?: Maybe<Scalars['String']>,
};



/** 
 * ===============================================================
 * User
 * ===============================================================
 **/
export type User = {
   __typename?: 'User',
  id: Scalars['ID'],
  name: Scalars['String'],
  email: Scalars['String'],
  companies?: Maybe<Array<Company>>,
  avatar: Scalars['String'],
};

export type ActivitiesQueryVariables = {};


export type ActivitiesQuery = (
  { __typename?: 'Query' }
  & { company: (
    { __typename?: 'Company' }
    & { activities: Maybe<Array<(
      { __typename?: 'Activity' }
      & Pick<Activity, 'actor' | 'action' | 'timestamp'>
      & { store: (
        { __typename?: 'Store' }
        & Pick<Store, 'name'>
      ) }
    )>> }
  ) }
);

export type CompanyNameQueryVariables = {};


export type CompanyNameQuery = (
  { __typename?: 'Query' }
  & { company: (
    { __typename?: 'Company' }
    & Pick<Company, 'name'>
  ) }
);

export type UserDataQueryVariables = {};


export type UserDataQuery = (
  { __typename?: 'Query' }
  & { user: (
    { __typename?: 'User' }
    & Pick<User, 'name'>
  ) }
);

export type PopularItemOverviewQueryVariables = {};


export type PopularItemOverviewQuery = (
  { __typename?: 'Query' }
  & { report: (
    { __typename?: 'Report' }
    & { item: (
      { __typename?: 'ReportItem' }
      & { itemPerformances: Array<(
        { __typename?: 'ItemPerformance' }
        & Pick<ItemPerformance, 'salesCount'>
        & { label: ItemPerformance['itemName'] }
      )> }
    ) }
  ) }
);

export type SalesOverviewQueryVariables = {};


export type SalesOverviewQuery = (
  { __typename?: 'Query' }
  & { report: (
    { __typename?: 'Report' }
    & { sales: (
      { __typename?: 'ReportSales' }
      & { summaries: Array<(
        { __typename?: 'SalesSummary' }
        & Pick<SalesSummary, 'time' | 'totalValue'>
      )> }
    ) }
  ) }
);

export type ItemNamesQueryVariables = {};


export type ItemNamesQuery = (
  { __typename?: 'Query' }
  & { items: (
    { __typename?: 'ItemConnection' }
    & { edges: Array<(
      { __typename?: 'ItemEdge' }
      & { node: (
        { __typename?: 'Item' }
        & Pick<Item, 'id' | 'name' | 'price'>
        & { measurementUnit: (
          { __typename?: 'MeasurementUnit' }
          & Pick<MeasurementUnit, 'id' | 'name' | 'notation'>
        ) }
      ) }
    )> }
  ) }
);

export type StoreNamesQueryVariables = {};


export type StoreNamesQuery = (
  { __typename?: 'Query' }
  & { company: (
    { __typename?: 'Company' }
    & { stores: (
      { __typename?: 'StoreConnection' }
      & { edges: Array<(
        { __typename?: 'StoreEdge' }
        & { node: (
          { __typename?: 'Store' }
          & Pick<Store, 'id' | 'name'>
        ) }
      )> }
    ) }
  ) }
);

export type ItemsQueryVariables = {
  searchTerm: Scalars['String'],
  first?: Maybe<Scalars['Int']>,
  after?: Maybe<Scalars['Cursor']>,
  last?: Maybe<Scalars['Int']>,
  before?: Maybe<Scalars['Cursor']>
};


export type ItemsQuery = (
  { __typename?: 'Query' }
  & { items: (
    { __typename?: 'ItemConnection' }
    & { pageInfo: (
      { __typename?: 'PageInfo' }
      & Pick<PageInfo, 'endCursor' | 'hasNextPage' | 'hasPreviousPage' | 'startCursor'>
    ), edges: Array<(
      { __typename?: 'ItemEdge' }
      & Pick<ItemEdge, 'cursor'>
      & { node: (
        { __typename?: 'Item' }
        & Pick<Item, 'id' | 'isListed' | 'name' | 'price' | 'imagePath'>
      ) }
    )> }
  ) }
);
