import { ApolloClient, InMemoryCache } from "apollo-boost"
import { createHttpLink } from "apollo-link-http"
import fetch from "isomorphic-fetch"

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function createApolloClient(): ApolloClient<any> {
  const link = createHttpLink({
    uri: "/api",
    fetch
  })
  const cache = new InMemoryCache()
  return new ApolloClient({ cache, link })
}

export default createApolloClient
