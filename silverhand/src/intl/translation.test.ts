import en from "./en.json"
import id from "./id.json"

describe("Translation data test", () => {
  it("should have indonesian translation", () => {
    const englishKeys = Object.keys(en)
    const indonesianKeys = Object.keys(id)
    englishKeys.forEach(key => {
      expect(indonesianKeys).toContain(key)
    })
  })
})
