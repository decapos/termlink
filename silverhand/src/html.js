/* eslint-disable */
// This is special file used by gatsby, so we leave it as a JS file.
// eslint is disabled because it causes too much hassle for little benefit
// for this special file. This file should rarely ever be modified. Its job is only
// to provide the base template for the final generated html file.
import React from "react"
import PropTypes from "prop-types"
import { Flex, Image, Text } from "rebass"
import { ReactComponent as AlertIcon } from "./icons/alert.svg"

export default function HTML(props) {
  return (
    <html {...props.htmlAttributes}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        {props.headComponents}
      </head>
      <body {...props.bodyAttributes}>
        {props.preBodyComponents}
        <noscript key="noscript" id="gatsby-noscript">
          <Flex
            width="100%"
            justifyContent="center"
            sx={{ position: "fixed", bottom: 0, zIndex: 9999 }}
          >
            <Text
              fontFamily="Open Sans, -apple-system, BlinkMacSystemFont, San Francisco, Roboto, Segoe UI, Helvetica Neue, sans-serif"
              fontSize={1}
              color="white"
              fontWeight="bold"
              p={3}
              backgroundColor="#d50000"
              m={2}
              display="flex"
              sx={{ alignItems: "center", borderRadius: 6 }}
            >
              <Image as={AlertIcon} size={24} mr={2} sx={{ fill: "white" }} />
              This app requires JavaScript to work.
            </Text>
          </Flex>
        </noscript>
        <div
          key="body"
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />
        {props.postBodyComponents}
      </body>
    </html>
  )
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array
}
