import React, { FC } from "react"
import PageNewItem from "./PageNewItem"
import Layout from "../Layout/Layout"

export default {
  title: "Pages|PageNewItem",
  component: PageNewItem,
  parameters: {
    componentSubtitle: "Just a simple PageNewItem"
  }
}

export const Basic: FC = () => (
  <Layout pageTitleIntlId="newItem">
    <PageNewItem />
  </Layout>
)
