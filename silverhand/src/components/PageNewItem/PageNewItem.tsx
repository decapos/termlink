import React, { FC, useState } from "react"
import { navigate } from "gatsby-plugin-intl3"
import BackNavigation from "../BackNavigation/BackNavigation"
import DiscardItemWarningDialog from "./DiscardItemWarningDialog"
import Box from "../Box/Box"
import ItemEditor from "../ItemEditor/ItemEditor"
import { useItemEditorState } from "../ItemEditor/useItemEditorState"
import ManageItemAppBar from "../ManageItemAppBar/ManageItemAppBar"
import { useFormatMessage } from "../hooks"

export const PageNewItem: FC = () => {
  const [warnItemDiscard, setWarnItemDiscard] = useState(false)
  const [isFormValid, setIsFormValid] = useState(false)
  const [state, dispatch] = useItemEditorState()

  return (
    <>
      <ManageItemAppBar
        title={useFormatMessage("newItem")}
        onDiscardClick={() => setWarnItemDiscard(true)}
        onSaveClick={() => navigate("/items")}
        isSavePossible={isFormValid}
      />
      <Box maxWidth="maxWidth.md" margin="auto" pb={100}>
        <BackNavigation to="/items" textIntlId="items" />
        <ItemEditor
          onValidityChange={setIsFormValid}
          state={state}
          dispatch={dispatch}
        />
      </Box>
      {warnItemDiscard && (
        <DiscardItemWarningDialog
          onDismiss={() => setWarnItemDiscard(false)}
          onPositive={() => navigate("/items")}
        />
      )}
    </>
  )
}

export default PageNewItem
