import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./PageNewItem.stories"

describe("PageNewItem", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
