import React, { FC } from "react"
import AlertDialog from "../AlertDialog/AlertDialog"
import { useFormatMessage } from "../hooks"

interface Props {
  onDismiss: () => void
  onPositive: () => void
}
const DiscardItemWarningDialog: FC<Props> = ({ onDismiss, onPositive }) => (
  <AlertDialog
    title={useFormatMessage("discardItemDialogTitle")}
    body={useFormatMessage("discardItemDialogDesc")}
    negativeText={useFormatMessage("continueCreating")}
    positiveText={useFormatMessage("discard")}
    onPositiveClick={onPositive}
    onDismiss={onDismiss}
    onNegativeClick={onDismiss}
  />
)

export default DiscardItemWarningDialog
