import React, { FC } from "react"
import { gql } from "apollo-boost"
import { useQuery } from "@apollo/react-hooks"
import { useThemeUI } from "theme-ui"
import Card from "../Card/Card"
import Typography from "../Typography/Typography"
import LineChart from "../LineChart/LineChart"
import {
  ReportTimeRange,
  SalesOverviewQuery,
  SalesSummary
} from "../../apollo/graphqlTypes"
import LoadingPlaceholder from "../LoadingPlaceholder/LoadingPlaceholder"
import Box from "../Box/Box"

export const GET_SALES = gql`
  query SalesOverview {
    report {
      sales(timeRange: DAILY) {
        summaries {
          time
          totalValue
        }
      }
    }
  }
`

const generateDataForChart = (
  summaries: Partial<SalesSummary>[]
): ChartData => {
  const values = summaries?.map(summary => summary.totalValue ?? 0) ?? []
  const labels = summaries?.map(summary => summary.time ?? "") ?? []
  return {
    datasets: [{ name: "Sales Data", values }],
    labels
  }
}

interface Props {
  timeRange: ReportTimeRange
}
const CardTotalSales: FC<Props> = () => {
  const { loading, data } = useQuery<SalesOverviewQuery>(GET_SALES)
  const { theme } = useThemeUI()
  const chartData = generateDataForChart(data?.report.sales.summaries ?? [])
  const primaryColor = theme.colors?.primary ?? "purple"

  return (
    <Card borderRadius={[0, "default"]}>
      <Typography.Body px={3} pt={3} m={0} fontSize={1}>
        TOTAL SALES
      </Typography.Body>
      <Typography.H4 px={3} mb={1} lineHeight={1.2}>
        12.000.000
      </Typography.H4>
      <Box height={240} p={3}>
        {loading ? (
          <LoadingPlaceholder width="auto" height="100%" />
        ) : (
          <LineChart colors={[primaryColor]} data={chartData} />
        )}
      </Box>
    </Card>
  )
}

export default CardTotalSales
