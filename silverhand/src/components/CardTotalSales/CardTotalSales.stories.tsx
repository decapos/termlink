import React, { FC } from "react"
import CardTotalSales from "./CardTotalSales"
import { ReportTimeRange } from "../../apollo/graphqlTypes"

export default {
  title: "Complex|Cards/CardTotalSales",
  component: CardTotalSales,
  parameters: {
    componentSubtitle: "Just a simple CardTotalSales"
  }
}

export const Basic: FC = () => (
  <CardTotalSales timeRange={ReportTimeRange.WEEKLY} />
)
