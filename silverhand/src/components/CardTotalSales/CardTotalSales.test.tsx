import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./CardTotalSales.stories"

describe("CardTotalSales", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
