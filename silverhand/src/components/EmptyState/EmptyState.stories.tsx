import React, { FC } from "react"
import EmptyState from "./EmptyState"
import { useNoItemImage } from "../../gatsby/useNoItemImage"

export default {
  title: "Basic|EmptyState",
  component: EmptyState,
  parameters: {
    componentSubtitle: "Just a simple EmptyState"
  }
}

export const Basic: FC = () => {
  const noItemImage = useNoItemImage()
  return (
    <EmptyState
      titleIntlId="addYourFirstItem"
      descriptionIntlId="addYourFirstItemDesc"
      image={noItemImage}
      buttonTextIntlId="addItem"
      buttonDestination="/"
    />
  )
}
