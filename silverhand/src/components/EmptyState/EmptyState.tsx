import React, { FC } from "react"
import GatsbyImage, { FluidObject } from "gatsby-image"
import { Link, FormattedMessage } from "gatsby-plugin-intl3"
import { Typography } from "../Typography/Typography"
import { Button } from "../Button/Button"
import Flex from "../Flex/Flex"
import Box, { BoxProps } from "../Box/Box"

interface Props {
  titleIntlId: string
  descriptionIntlId: string
  buttonTextIntlId: string
  image?: FluidObject
  imageProps?: BoxProps
  buttonDestination: string
}
export const EmptyState: FC<Props> = ({
  descriptionIntlId,
  image,
  titleIntlId,
  buttonTextIntlId,
  imageProps,
  buttonDestination
}) => (
  <Flex
    flexDirection="column"
    justifyContent="center"
    height={["", "", "80vh", "100vh"]}
    overflowX={["hidden", "hidden", "hidden", "inherit"]}
    maxWidth={1280}
    minHeight={["", "", "100%"]}
    margin="auto"
    mb={[0, 0, 0]}
    pt={[3, 4]}
  >
    <Box mx={3} my={[4.4]} py={[4, 0]} sx={{ zIndex: 1 }}>
      <Typography.H2 maxWidth={500}>
        <FormattedMessage id={titleIntlId} />
      </Typography.H2>
      <Typography.Body fontSize={3} color="textMediumEmphasis" maxWidth={400}>
        <FormattedMessage id={descriptionIntlId} />
      </Typography.Body>
      <Link to={buttonDestination}>
        <Button variant="primary">
          <FormattedMessage id={buttonTextIntlId} />
        </Button>
      </Link>
    </Box>
    <Box
      minWidth={500}
      mt={[-50, -50, -90, -130]}
      mr={[-50, -50, -80, 0]}
      maxWidth={900}
      alignSelf="flex-end"
      width={["120%", "120%", "50%"]}
      {...imageProps}
    >
      <GatsbyImage fluid={image} />
    </Box>
    <Typography.Body
      mt={-4}
      mb={4}
      fontSize={0}
      width="100%"
      textAlign="right"
      pr={4}
      sx={{ zIndex: 2 }}
    >
      Illustration by{" "}
      <Box
        as="a"
        href="https://icons8.com"
        color="textPrimary"
        sx={{ textDecoration: "underline" }}
      >
        Ouch.pics
      </Box>
    </Typography.Body>
  </Flex>
)

export default EmptyState
