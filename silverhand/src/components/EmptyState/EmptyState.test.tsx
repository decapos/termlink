import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./EmptyState.stories"

describe("EmptyState", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
