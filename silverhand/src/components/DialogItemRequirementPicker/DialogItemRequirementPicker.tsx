import React, { FC, useState } from "react"
import { gql } from "apollo-boost"
import { useQuery } from "@apollo/react-hooks"
import { ItemNamesQuery, MeasurementUnit } from "../../apollo/graphqlTypes"
import { useFormatMessage } from "../hooks"
import ScrollableDialog from "../ScrollableDialog/ScrollableDialog"
import Box from "../Box/Box"
import SearchBar from "../SearchBar/SearchBar"
import Checkbox from "../Checkbox/Checkbox"

export const GET_ITEM_NAMES = gql`
  query ItemNames {
    items {
      edges {
        node {
          id
          name
          price
          measurementUnit {
            id
            name
            notation
          }
        }
      }
    }
  }
`
interface Props {
  excludedItems?: Requirement[]
  onSelectedItemsChange: (items: Requirement[]) => void
  selectedItems: Requirement[]
  onDismiss?: () => void
  onNegativeClick?: () => void
  onPositiveClick?: () => void
}
export interface Requirement {
  id: string
  name: string
  measurementUnit: MeasurementUnit
  amount?: number
}
export const DialogItemRequirementPicker: FC<Props> = ({
  onSelectedItemsChange,
  excludedItems,
  selectedItems,
  ...props
}) => {
  const [filter, setFilter] = useState("")
  const { data, loading } = useQuery<ItemNamesQuery>(GET_ITEM_NAMES)
  const subtext = useFormatMessage("itemsSelected", {
    itemCount: selectedItems.length
  })

  function selectItem(item: Requirement): void {
    const updatedSelectedItems = [...selectedItems, { ...item, amount: 0 }]
    onSelectedItemsChange(updatedSelectedItems)
  }
  function unselectItem(item: Requirement): void {
    const updatedSelectedItems = selectedItems.filter(
      ({ id }) => id !== item.id
    )
    onSelectedItemsChange(updatedSelectedItems)
  }

  return (
    <ScrollableDialog
      negativeText={useFormatMessage("cancel")}
      positiveText={useFormatMessage("select")}
      title={useFormatMessage("chooseAnItem")}
      subtext={subtext}
      {...props}
    >
      <Box px={3}>
        <SearchBar
          onChange={e => setFilter(e.target.value)}
          value={filter}
          mb={0}
        />
        {!loading &&
          data?.items.edges
            .filter(
              edge =>
                edge.node.name.includes(filter) &&
                !excludedItems?.some(({ id }) => id === edge.node.id)
            )
            .map(edge => (
              <Checkbox
                label={edge.node.name}
                p={2}
                my={2}
                key={edge.node.id}
                checked={selectedItems.some(({ id }) => edge.node.id === id)}
                onChange={e =>
                  e.target.checked
                    ? selectItem(edge.node)
                    : unselectItem(edge.node)
                }
                sx={{
                  "&:hover, &:focus": {
                    cursor: "pointer",
                    backgroundColor: "primaryLighter",
                    borderRadius: "default"
                  }
                }}
              />
            ))}
      </Box>
    </ScrollableDialog>
  )
}

export default DialogItemRequirementPicker
