import React, { FC, useState } from "react"
import { action } from "@storybook/addon-actions"
import DialogItemRequirementPicker, {
  Requirement
} from "./DialogItemRequirementPicker"

export default {
  title: "Complex|Dialogs/DialogItemRequirementPicker",
  component: DialogItemRequirementPicker,
  parameters: {
    componentSubtitle: "Just a simple DialogItemRequirementPicker"
  }
}

export const Basic: FC = () => {
  const [selectedItem, setSelectedItem] = useState<Requirement[]>([])

  function handleSelectedItemChange(item: Requirement[]): void {
    action("selected item change")(item)
    setSelectedItem(item)
  }
  return (
    <DialogItemRequirementPicker
      selectedItems={selectedItem}
      onSelectedItemsChange={handleSelectedItemChange}
    />
  )
}
