import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./DialogItemRequirementPicker.stories"

describe("DialogItemRequirementPicker", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
