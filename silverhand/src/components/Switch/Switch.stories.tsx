import React, { FC } from "react"
import Switch from "./Switch"

export default {
  title: "Core|Forms/Switch",
  component: Switch,
  parameters: {
    componentSubtitle: "Just a simple Switch"
  }
}

export const Basic: FC = () => <Switch />
