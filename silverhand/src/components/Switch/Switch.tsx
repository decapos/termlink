import React, { FC } from "react"
import { Switch as BaseSwitch } from "@rebass/forms"
import { BoxProps } from "../Box/Box"

export const Switch: FC<BoxProps> = props => <BaseSwitch {...props} />

export default Switch
