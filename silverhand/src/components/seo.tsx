import React, { FC } from "react"
import { Helmet } from "react-helmet"
import { useIntl } from "gatsby-plugin-intl3"

interface Props {
  title: string
}
const SEO: FC<Props> = ({ title }) => {
  const titleTemplate = `Deca | %s`
  const intl = useIntl()

  return (
    <Helmet title={title} titleTemplate={titleTemplate}>
      <html lang={intl.locale} />
    </Helmet>
  )
}

export default SEO
