import React, { FC } from "react"
import PercentageChart from "./PercentageChart"
import { generateMockChartData } from "../../__mocks__/data"

export default {
  title: "Basic|Charts/PercentageChart",
  component: PercentageChart,
  parameters: {
    componentSubtitle: "Just a simple PercentageChart"
  }
}

export const Basic: FC = () => (
  <PercentageChart data={generateMockChartData()} />
)
