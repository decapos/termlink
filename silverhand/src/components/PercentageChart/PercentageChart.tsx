import React, { FC } from "react"
import Charts from "../Charts/Charts"

interface Props {
  title?: string
  data: ChartData
}
export const PercentageChart: FC<Props> = ({ data }) => (
  <Charts
    data={data}
    type="percentage"
    barOptions={{ spaceRatio: 0.2, height: 20, depth: 1 }}
    axisOptions={{ xAxisMode: "span" }}
  />
)

export default PercentageChart
