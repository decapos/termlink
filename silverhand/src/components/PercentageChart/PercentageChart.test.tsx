import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./PercentageChart.stories"

describe("PercentageChart", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
