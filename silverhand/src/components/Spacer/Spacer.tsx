import React, { FC } from "react"
import Box from "../Box/Box"

export const Spacer: FC = () => <Box flex={1} sx={{ pointerEvents: "none" }} />

export default Spacer
