import React, { FC } from "react"
import BarChart from "./BarChart"

export default {
  title: "Basic|Charts/BarChart",
  component: BarChart,
  parameters: {
    componentSubtitle: "Just a simple BarChart"
  }
}

export const Basic: FC = () => {
  const data = {
    labels: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
    datasets: [{ name: "Draco", values: [18, 40, 30, 35, 8, 52, 17, -4] }]
  }

  return <BarChart data={data} />
}
