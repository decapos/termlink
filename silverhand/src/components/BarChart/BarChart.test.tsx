import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./BarChart.stories"

describe("BarChart", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
