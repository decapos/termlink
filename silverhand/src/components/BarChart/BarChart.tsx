import React, { FC } from "react"
import Charts from "../Charts/Charts"

interface Props {
  title?: string
  options?: ChartOptions
  data: ChartData
}
export const BarChart: FC<Props> = ({ data }) => (
  <Charts
    data={data}
    type="bar"
    barOptions={{ spaceRatio: 0.2 }}
    axisOptions={{ xAxisMode: "span" }}
  />
)

export default BarChart
