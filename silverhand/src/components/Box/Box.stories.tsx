import React, { FC } from "react"
import Box from "./Box"

export default {
  title: "Core|Box",
  component: Box,
  parameters: {
    componentSubtitle: "Just a simple Box"
  }
}

export const Basic: FC = () => <Box />
