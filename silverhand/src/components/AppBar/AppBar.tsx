import React, { FC, MouseEventHandler, useRef, useState } from "react"
import { gql } from "apollo-boost"
import { useQuery } from "@apollo/react-hooks"
import GatsbyImage from "gatsby-image"
import Typography from "../Typography/Typography"
import { ReactComponent as StorefrontIcon } from "../../icons/storefront.svg"
import { ReactComponent as LogoutIcon } from "../../icons/logout.svg"
import Avatar from "../Avatar/Avatar"
import Icon from "../Icon/Icon"
import { CompanyNameQuery, UserDataQuery } from "../../apollo/graphqlTypes"
import { ReactComponent as ArrowDownIcon } from "../../icons/arrow-down-outline.svg"
import { Card } from "../Card/Card"
import { useOutsideClick } from "../hooks"
import Spacer from "../Spacer/Spacer"
import LoadingPlaceholder from "../LoadingPlaceholder/LoadingPlaceholder"
import MenuIcon from "./MenuIcon"
import { useAvatarPlaceholder } from "../../gatsby/useAvatarPlaceholder"
import Flex, { FlexProps } from "../Flex/Flex"
import Box from "../Box/Box"

interface Props {
  onMenuClick?: MouseEventHandler<HTMLImageElement>
  position?: "fixed" | "relative"
  title: string
}

export const AppBar: FC<Props> = ({
  onMenuClick,
  title,
  position = "relative"
}) => {
  return (
    <Card
      as="header"
      backgroundColor="surface"
      height="appbar"
      width="100%"
      display="flex"
      borderRadius={0}
      sx={{
        alignItems: "center",
        borderColor: "border",
        zIndex: 50,
        top: 0,
        position
      }}
    >
      <StoreName
        alignItems="center"
        height={57}
        width={[0, "auto"]}
        overflow="hidden"
      />
      <MenuIcon onMenuClick={onMenuClick} />
      <Typography.H6 fontWeight="body" opacity={[1, 0]} mb={0} ml={[0, 3]}>
        {title}
      </Typography.H6>
      <Spacer />
      <UserAvatar />
    </Card>
  )
}

export const GET_COMPANY_NAME = gql`
  query CompanyName {
    company {
      name
    }
  }
`
export const StoreName: FC<FlexProps> = ({ ...props }) => {
  const { loading, data } = useQuery<CompanyNameQuery>(GET_COMPANY_NAME)
  let companyName = ""
  if (data && data.company) {
    companyName = data.company.name
  }
  return (
    <Flex height="appbar" {...props}>
      <Icon as={StorefrontIcon} size={24} alt="Store Icon" />
      <Typography.Body as="div" ml="-4px" mb={0} sx={{ whiteSpace: "nowrap" }}>
        {!loading ? (
          companyName
        ) : (
          <LoadingPlaceholder variant="loadingPlaceholder.text" width={120} />
        )}
      </Typography.Body>
    </Flex>
  )
}

export const GET_USER_DATA = gql`
  query UserData {
    user {
      name
    }
  }
`
const UserAvatar: FC = () => {
  const [isShowingOption, setIsShowingOption] = useState(false)
  const element = useRef<HTMLElement>(null)
  const avatar = useAvatarPlaceholder()
  useOutsideClick(element, () => setIsShowingOption(false))

  const { loading, data } = useQuery<UserDataQuery>(GET_USER_DATA)
  let name = ""
  if (data && data.user) {
    name = data.user.name
  }

  return (
    <Flex
      ref={element}
      mx={3}
      p={1}
      alignItems="center"
      sx={{
        borderStyle: "solid",
        borderRadius: "circle",
        borderColor: "border",
        borderWidth: 1,
        cursor: "pointer",
        transition: "background-color 250ms cubic-bezier(0.0, 0.0, 0.2, 1)",
        "&:hover": {
          backgroundColor: "muted"
        }
      }}
      onClick={() => setIsShowingOption(!isShowingOption)}
    >
      <Avatar as={Box}>
        <GatsbyImage fixed={avatar} alt="avatar" />
      </Avatar>
      <Typography.Body
        as="div"
        ml={2}
        mr={1}
        my={0}
        display={["none", "block"]}
      >
        {!loading ? (
          name
        ) : (
          <LoadingPlaceholder variant="loadingPlaceholder.text" width={100} />
        )}
      </Typography.Body>
      <Icon
        as={ArrowDownIcon}
        alt="Store Icon"
        m={0}
        ml={[2, 0]}
        mr={[2, 1]}
        opacity={0.4}
      />
      {isShowingOption && <SignOutCard />}
    </Flex>
  )
}

const SignOutCard: FC = () => (
  <Card
    sx={{
      top: 52,
      right: 3,
      position: "fixed",
      boxShadow: "low",
      borderRadius: "default"
    }}
  >
    <a href="/logout">
      <Flex alignItems="center" width={140}>
        <Icon as={LogoutIcon} size={20} />
        <Typography.Body m={0}>Log out</Typography.Body>
      </Flex>
    </a>
  </Card>
)

export default AppBar
