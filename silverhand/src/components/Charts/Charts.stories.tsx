import React, { FC } from "react"
import { object, select } from "@storybook/addon-knobs"
import Charts from "./Charts"

export default {
  title: "Basic|Charts",
  component: Charts,
  parameters: {
    componentSubtitle: "Charting component powered by Frappe Charts"
  }
}

export const Basic: FC = () => {
  const data = {
    labels: [
      "12am-3am",
      "3am-6pm",
      "6am-9am",
      "9am-12am",
      "12pm-3pm",
      "3pm-6pm",
      "6pm-9pm",
      "9am-12am"
    ],
    datasets: [
      {
        name: "Some Data",
        type: "bar",
        values: [25, 40, 30, 35, 8, 52, 17, -4]
      },
      {
        name: "Another Set",
        type: "line",
        values: [25, 50, -10, 15, 18, 32, 27, 14]
      }
    ]
  }
  const type = select(
    "type",
    {
      Mixed: "axis-mixed",
      Line: "line",
      Bar: "bar",
      Percentage: "percentage",
      Heatmap: "heatmap",
      Pie: "pie",
      Donut: "donut"
    },
    "axis-mixed"
  )

  return <Charts type={type} data={object("data", data, "")} />
}
