import React, { FC, MutableRefObject, useEffect, useRef, useState } from "react"
import { Chart } from "frappe-charts/dist/frappe-charts.min.esm"

export const Charts: FC<ChartOptions> = props => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const chartContainer: MutableRefObject<HTMLDivElement | any> = useRef()
  const [chart, setChart] = useState<null | Chart>(null)
  useEffect(() => {
    if (chart === null) {
      setChart(new Chart(chartContainer.current, { ...props }))
    } else {
      chart.update(props.data)
    }
  }, [chart, props])

  return <div ref={chartContainer} />
}

export default Charts
