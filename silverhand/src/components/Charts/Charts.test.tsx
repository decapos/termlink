import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./Charts.stories"

describe("Charts", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
