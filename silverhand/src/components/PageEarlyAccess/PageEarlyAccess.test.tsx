import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./PageEarlyAccess.stories"

describe("PageEarlyAccess", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
