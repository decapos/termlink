import React, { FC } from "react"
import { Link } from "gatsby-plugin-intl3"
import Button from "../Button/Button"
import { Typography } from "../Typography/Typography"
import Input from "../Input/Input"
import Spacer from "../Spacer/Spacer"
import { Flex } from "../Flex/Flex"
import Box from "../Box/Box"
import { useFormatMessage } from "../hooks"

export const PageEarlyAccess: FC = () => (
  <Flex
    flexDirection="column"
    alignItems="center"
    justifyContent="center"
    minHeight={["auto", "100vh"]}
  >
    <Header />
    <Flex as="form" flexDirection="column" p={3} width="100%" maxWidth="500px">
      <WelcomeMessage />
      <Input label={useFormatMessage("email")} width="100%" />
      <Button variant="primaryBig" my={3} aria-label="submit" type="button">
        request early access
      </Button>
    </Flex>
  </Flex>
)

const Header: FC = () => (
  <Flex
    width="100%"
    flexDirection="row"
    p={3}
    sx={{
      position: ["relative", "fixed"],
      top: 0,
      left: 0
    }}
  >
    <Spacer />
    <Box>
      <Link to="/login">
        <Button variant="outlineBig">Login</Button>
      </Link>
    </Box>
  </Flex>
)

const WelcomeMessage: FC = () => (
  <>
    <Typography.H3>Hi there,</Typography.H3>
    <Typography.Body mb={4}>
      We&apos;re still building our software right now. But we would really love
      to have you try it and get your feedback.
    </Typography.Body>
    <Typography.Body>
      Leave us your email and we&apos;ll be in touch :)
    </Typography.Body>
  </>
)

export default PageEarlyAccess
