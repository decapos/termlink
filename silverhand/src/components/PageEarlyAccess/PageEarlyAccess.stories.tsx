import React, { FC } from "react"
import PageEarlyAccess from "./PageEarlyAccess"

export default {
  title: "Pages|PageEarlyAccess",
  component: PageEarlyAccess,
  parameters: {
    componentSubtitle: "Just a simple PageEarlyAccess"
  }
}

export const Basic: FC = () => <PageEarlyAccess />
