import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./PageEditItem.stories"

describe("PageEditItem", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
