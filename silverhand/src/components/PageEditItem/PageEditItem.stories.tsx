import React, { FC } from "react"
import PageEditItem from "./PageEditItem"
import Layout from "../Layout/Layout"

export default {
  title: "Core|PageEditItem",
  component: PageEditItem,
  parameters: {
    componentSubtitle: "Just a simple PageEditItem"
  }
}

export const Basic: FC = () => (
  <Layout pageTitleIntlId="newItem">
    <PageEditItem id="galaxy-s5" />
  </Layout>
)
