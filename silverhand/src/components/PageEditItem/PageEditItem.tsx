import React, { FC, useState } from "react"
import { navigate } from "gatsby-plugin-intl3"
import { useItemEditorState } from "../ItemEditor/useItemEditorState"
import Box from "../Box/Box"
import BackNavigation from "../BackNavigation/BackNavigation"
import ItemEditor from "../ItemEditor/ItemEditor"
import DiscardItemWarningDialog from "../PageNewItem/DiscardItemWarningDialog"
import { useFormatMessage } from "../hooks"
import ManageItemAppBar from "../ManageItemAppBar/ManageItemAppBar"

interface Props {
  id: string
}
export const PageEditItem: FC<Props> = ({ id }) => {
  const [warnItemDiscard, setWarnItemDiscard] = useState(false)
  const [isFormValid, setIsFormValid] = useState(false)
  const [state, dispatch] = useItemEditorState()

  return (
    <>
      <ManageItemAppBar
        title={useFormatMessage("editItem")}
        onDiscardClick={() => setWarnItemDiscard(true)}
        onSaveClick={() => navigate("/items")}
        isSavePossible={isFormValid}
      />
      <Box maxWidth="maxWidth.md" margin="auto" pb={100}>
        <BackNavigation to="/items" textIntlId="items" />
        {id}
        <ItemEditor
          onValidityChange={setIsFormValid}
          state={state}
          dispatch={dispatch}
        />
      </Box>
      {warnItemDiscard && (
        <DiscardItemWarningDialog
          onDismiss={() => setWarnItemDiscard(false)}
          onPositive={() => navigate("/items")}
        />
      )}
    </>
  )
}

export default PageEditItem
