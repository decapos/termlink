import React, { FC } from "react"
import { action } from "@storybook/addon-actions"
import ManageItemAppBar from "./ManageItemAppBar"

export default {
  title: "Core|ManageItemAppBar",
  component: ManageItemAppBar,
  parameters: {
    componentSubtitle: "Just a simple ManageItemAppBar"
  }
}

export const Basic: FC = () => (
  <ManageItemAppBar
    onDiscardClick={action("discard")}
    onSaveClick={action("save")}
    title="New Item"
  />
)
