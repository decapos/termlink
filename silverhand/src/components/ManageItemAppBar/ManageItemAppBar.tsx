import React, { FC } from "react"
import { FormattedMessage } from "gatsby-plugin-intl3"
import { useSilverhandTheme } from "../hooks"
import Box from "../Box/Box"
import Flex from "../Flex/Flex"
import Button from "../Button/Button"
import Spacer from "../Spacer/Spacer"
import Typography from "../Typography/Typography"

interface Props {
  title: string
  onDiscardClick: () => void
  onSaveClick: () => void
  isSavePossible?: boolean
}
export const ManageItemAppBar: FC<Props> = ({
  title,
  onDiscardClick,
  onSaveClick,
  isSavePossible
}) => {
  const theme = useSilverhandTheme()
  return (
    <Box
      sx={{
        zIndex: 75,
        position: "fixed",
        top: 0
      }}
      backgroundColor="surface"
      ml={[theme.sizes.icon + 16, 0]} // 16 is icon's default margin
      width={[
        "calc(100% - 40px)",
        `calc(100% - ${theme.sizes.sidebar.desktop}px)`
      ]}
    >
      <Flex
        flexDirection="row-reverse"
        alignItems="center"
        maxWidth="maxWidth.md"
        margin="auto"
        height="56px"
        px={3}
      >
        <Button
          variant="primary"
          onClick={onSaveClick}
          disabled={!isSavePossible}
          data-cy="save"
        >
          <FormattedMessage id="save" />
        </Button>
        <Button
          variant="outline"
          onClick={onDiscardClick}
          mr={2}
          data-cy="discard"
        >
          <FormattedMessage id="discard" />
        </Button>
        <Spacer />
        <Typography.H6 mb={0} lineHeight={1}>
          {title}
        </Typography.H6>
      </Flex>
    </Box>
  )
}

export default ManageItemAppBar
