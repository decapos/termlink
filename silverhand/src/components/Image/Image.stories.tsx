import React, { FC } from "react"
import Image from "./Image"

export default {
  title: "Core|Image",
  component: Image,
  parameters: {
    componentSubtitle: "Just a simple Image"
  }
}

export const Basic: FC = () => <Image />
