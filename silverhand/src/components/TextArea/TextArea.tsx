import React, { ChangeEventHandler, FC } from "react"
import { Textarea as BaseTextArea } from "@rebass/forms"

import { FormattedMessage } from "gatsby-plugin-intl3"
import Label from "../Label/Label"
import Box, { BoxProps } from "../Box/Box"

interface Props extends BoxProps {
  labelIntlId?: string
  onChange?: ChangeEventHandler<HTMLInputElement>
}
export const TextArea: FC<Props> = ({ labelIntlId, ...props }) => (
  <Label display="flex" sx={{ flexDirection: "column" }}>
    {labelIntlId && (
      <Box pl={1} pb={1}>
        <FormattedMessage id={labelIntlId} />
      </Box>
    )}
    <BaseTextArea {...props} />
  </Label>
)

export default TextArea
