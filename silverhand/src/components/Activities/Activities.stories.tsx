import React, { FC } from "react"
import Activities from "./Activities"

export default {
  title: "Complex|Activities",
  component: Activities,
  parameters: {
    componentSubtitle: "Just a simple Activities"
  }
}

export const Basic: FC = () => <Activities />
