import React, { FC } from "react"
import { gql } from "apollo-boost"
import { useQuery } from "@apollo/react-hooks"
import { FormattedDate } from "gatsby-plugin-intl3"
import { Box } from "../Box/Box"
import { ActivitiesQuery } from "../../apollo/graphqlTypes"
import Typography from "../Typography/Typography"

export const GET_ACTIVITIES = gql`
  query Activities {
    company {
      activities {
        actor
        action
        timestamp
        store {
          name
        }
      }
    }
  }
`
export const Activities: FC = () => {
  const { loading, data } = useQuery<ActivitiesQuery>(GET_ACTIVITIES)

  if (loading) {
    return <Box p={2}>error</Box>
  }
  if (data && data.company && data.company.activities) {
    return (
      <Box p={2}>
        <Typography.Body
          width="100%"
          textAlign="center"
          my={3}
          sx={{ opacity: 0.7 }}
        >
          TODAY ACTIVITIES
        </Typography.Body>
        {data.company.activities.map(({ actor, timestamp, store, action }) => {
          const description = `${actor} ${action} to ${store.name}`
          return (
            <Typography.Body key={timestamp + description}>
              <b>
                <FormattedDate
                  value={timestamp}
                  year="numeric"
                  month="short"
                  day="2-digit"
                />
              </b>{" "}
              {description}
            </Typography.Body>
          )
        })}
      </Box>
    )
  }
  return <Box p={2}>error</Box>
}

export default Activities
