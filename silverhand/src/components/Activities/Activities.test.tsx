import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./Activities.stories"

describe("Activities", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
