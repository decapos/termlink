import { renderHook } from "@testing-library/react-hooks"
import { useFileUrl } from "./hooks"

describe("useFileUrl test", () => {
  it("should return undefined when input is undefined", () => {
    const { result } = renderHook(() => useFileUrl(undefined))
    expect(result.current).toBeUndefined()
  })

  it("should return stringified file when passed a file", async () => {
    const mockFile = new File(["test"], "filename", { type: "text/html" })
    const { result, waitForNextUpdate } = renderHook(() => useFileUrl(mockFile))

    await waitForNextUpdate()

    expect(result.current).toBe("data:text/html;charset=undefined,test")
  })
})
