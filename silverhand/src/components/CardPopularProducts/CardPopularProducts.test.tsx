import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./CardPopularProducts.stories"

describe("CardPopularProducts", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
