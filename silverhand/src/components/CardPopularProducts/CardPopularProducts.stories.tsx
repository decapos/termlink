import React, { FC } from "react"
import CardPopularProducts from "./CardPopularProducts"
import { ReportTimeRange } from "../../apollo/graphqlTypes"

export default {
  title: "Complex|Cards/CardPopularProducts",
  component: CardPopularProducts,
  parameters: {
    componentSubtitle: "Just a simple CardPopularProducts"
  }
}

export const Basic: FC = () => (
  <CardPopularProducts timeRange={ReportTimeRange.WEEKLY} />
)
