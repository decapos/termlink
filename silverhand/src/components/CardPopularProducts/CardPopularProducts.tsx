import React, { FC } from "react"
import { gql } from "apollo-boost"
import { useQuery } from "@apollo/react-hooks"
import Typography from "../Typography/Typography"
import Card from "../Card/Card"
import PercentageChart from "../PercentageChart/PercentageChart"
import {
  PopularItemOverviewQuery,
  ReportTimeRange
} from "../../apollo/graphqlTypes"
import LoadingPlaceholder from "../LoadingPlaceholder/LoadingPlaceholder"

export const GET_POPULAR_PRODUCT = gql`
  query PopularItemOverview {
    report {
      item(timeRange: DAILY) {
        itemPerformances {
          label: itemName
          salesCount
        }
      }
    }
  }
`
interface Props {
  timeRange: ReportTimeRange
}
export const CardPopularProducts: FC<Props> = () => {
  const { loading, data } = useQuery<PopularItemOverviewQuery>(
    GET_POPULAR_PRODUCT
  )

  // Process returned data to be display as a graph
  const chartData: ChartData = { datasets: [], labels: [] }
  if (data && data.report) {
    chartData.labels = data.report.item.itemPerformances.map(
      performance => performance.label
    )
    const values = data.report.item.itemPerformances
      .map(performance => performance.salesCount)
      .sort((a, b) => b - a)
    chartData.datasets = [{ values, name: "products" }]
  }

  return (
    <Card mt={3} borderRadius={[1, "default"]}>
      <Typography.Body px={3} pt={3} m={0} fontSize={1}>
        MOST POPULAR
      </Typography.Body>
      <Typography.H4 px={3} mb={1} lineHeight={1.2}>
        Products
      </Typography.H4>
      {loading ? (
        <LoadingPlaceholder width="auto" height={164} m={3} />
      ) : (
        <PercentageChart data={chartData} />
      )}
    </Card>
  )
}

export default CardPopularProducts
