import React, { Dispatch, FC, useMemo } from "react"
import { FormattedMessage } from "gatsby-plugin-intl3"
import { TextProps } from "rebass"
import Checkbox from "../Checkbox/Checkbox"
import { useFormatMessage } from "../hooks"
import Input from "../Input/Input"
import Textarea from "../TextArea/TextArea"
import ProductImageInput from "./ProductImageInput"
import Button from "../Button/Button"
import { ReactComponent as PlusIcon } from "../../icons/plus.svg"
import Select from "../Select/Select"
import DependentInventory from "./DependentInventory"
import Box from "../Box/Box"
import Typography from "../Typography/Typography"
import { ItemEditorState } from "./useItemEditorState"
import { ItemEditorActions, itemEditorActions } from "./itemEditorActions"
import FormVariantAttribute from "../FormVariantAttribute/FormVariantAttribute"
import FormDiscreteInventory from "../FormDiscreteInventory/FormDiscreteInventory"

interface Props {
  onValidityChange: (value: boolean) => void
  state: ItemEditorState
  dispatch: Dispatch<ItemEditorActions>
}
export const ItemEditor: FC<Props> = ({
  onValidityChange,
  state,
  dispatch
}) => {
  const {
    description,
    attributes,
    inventoryType,
    itemRequirements,
    itemImage,
    isListed,
    name,
    price,
    discreteInventory
  } = state

  const {
    setItemRequirements,
    updateAttribute,
    deleteAttribute,
    newEmptyAttribute,
    setDescription,
    setInventoryType,
    setIsListed,
    setItemImage,
    setName,
    setPrice,
    setDiscreteInventory
  } = itemEditorActions

  useMemo(() => {
    const isFormValid = state.name !== ""
    onValidityChange(isFormValid)
  }, [state.name, onValidityChange])

  const itemInfoSection = (
    <>
      <SectionHeader mt={0} mb={2}>
        <FormattedMessage id="itemInfo" />
      </SectionHeader>
      <Checkbox
        mb={3}
        label={useFormatMessage("isListed")}
        id="isListed"
        checked={isListed}
        onChange={e => dispatch(setIsListed(e.target.checked))}
      />
      <Input
        label={useFormatMessage("name")}
        width="100%"
        mb={3}
        onChange={e => dispatch(setName(e.target.value))}
        value={name}
        placeholder={useFormatMessage("nameHint")}
        data-cy="name"
      />
      <Input
        disabled={!isListed}
        label={useFormatMessage("sellingPrice")}
        width="100%"
        mb={3}
        value={price}
        type="number"
        onChange={e => dispatch(setPrice(parseInt(e.target.value, 10)))}
        data-cy="price"
      />
      <Textarea
        labelIntlId="description"
        width="100%"
        height={220}
        onChange={e => dispatch(setDescription(e.target.value))}
        value={description}
        placeholder={useFormatMessage("descriptionHint")}
        data-cy="description"
      />
      <ProductImageInput
        value={itemImage}
        onChange={file => dispatch(setItemImage(file))}
      />
    </>
  )

  const variantSection = (
    <>
      <SectionHeader>
        <FormattedMessage id="variant" />
      </SectionHeader>
      {attributes.map(attribute => (
        <FormVariantAttribute
          key={attribute.id}
          onChange={updatedAttribute =>
            dispatch(updateAttribute(updatedAttribute))
          }
          onDelete={() => dispatch(deleteAttribute(attribute))}
          value={attribute}
        />
      ))}
      <Button
        mt={3}
        type="button"
        icon={PlusIcon}
        variant="outline"
        width={["100%"]}
        onClick={() => dispatch(newEmptyAttribute())}
        data-cy="addVariant"
      >
        <FormattedMessage id="addVariant" />
      </Button>
    </>
  )

  const inventorySection = (
    <>
      <SectionHeader pb={3}>
        <FormattedMessage id="inventory" />
      </SectionHeader>
      <Select
        label={useFormatMessage("trackingMode")}
        hideLabel
        mb={3}
        onChange={e => dispatch(setInventoryType(parseInt(e.target.value, 10)))}
      >
        <option value={0}>{useFormatMessage("dontTrack")}</option>
        <option value={1}>{useFormatMessage("discreteValue")}</option>
        <option value={2}>{useFormatMessage("basedOnOtherItems")}</option>
      </Select>
      {inventoryType === 1 && discreteInventory && (
        <FormDiscreteInventory
          inventory={discreteInventory}
          onInventoryChange={changedInventory =>
            dispatch(setDiscreteInventory(changedInventory))
          }
        />
      )}
      {inventoryType === 2 && (
        <DependentInventory
          itemRequirements={itemRequirements}
          onChangeItemRequirements={setItemRequirements}
        />
      )}
    </>
  )

  return (
    <Box as="form" p={3} pt={0}>
      {itemInfoSection}
      {variantSection}
      {inventorySection}
    </Box>
  )
}

const SectionHeader: FC<TextProps> = props => (
  <Typography.H3 mt={5} mb={0} {...props} />
)

export default ItemEditor
