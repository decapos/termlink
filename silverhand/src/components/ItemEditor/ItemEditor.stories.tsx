import React, { FC } from "react"
import { action } from "@storybook/addon-actions"
import ItemEditor from "./ItemEditor"
import { useItemEditorState } from "./useItemEditorState"

export default {
  title: "Complex|Forms/ItemEditor",
  component: ItemEditor,
  parameters: {
    componentSubtitle: "Just a simple ItemEditor"
  }
}

const Story: FC = () => {
  const [state, dispatch] = useItemEditorState()

  return (
    <ItemEditor
      onValidityChange={action("isFormValid")}
      dispatch={dispatch}
      state={state}
    />
  )
}

// TODO: We require this workaround because calling useQuery (including
//  custom hooks that calls useQuery) inside stories causes Apollo to throw
//  invariant violation exception. (*remove when storybook fix landed)
export const Basic: FC = () => <Story />
