import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./ItemEditor.stories"

describe("ItemEditor test", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
