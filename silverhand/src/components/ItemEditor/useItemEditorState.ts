import { Dispatch, useEffect, useReducer } from "react"
import nanoid from "nanoid"
import { gql } from "apollo-boost"
import { useQuery } from "@apollo/react-hooks"
import { Requirement } from "../DialogItemRequirementPicker/DialogItemRequirementPicker"
import { generateItemNames } from "./generateItemNames"
import { itemEditorActions, ItemEditorActions } from "./itemEditorActions"
import { VariantAttribute } from "../FormVariantAttribute/FormVariantAttribute"
import { DiscreteInventoryData } from "../FormDiscreteInventory/FormDiscreteInventory"
import { StoreNamesQuery } from "../../apollo/graphqlTypes"

const initialState = {
  name: "",
  price: 0,
  description: "",
  isListed: true,
  itemImage: undefined as File | undefined,
  inventoryType: 0,
  discreteInventory: undefined as DiscreteInventoryData | undefined,
  itemRequirements: [] as Requirement[],
  attributes: [] as VariantAttribute[],
  storeNames: [] as StoreName[]
}
export type ItemEditorState = typeof initialState
export type StoreName = { id: string; name: string }

function removeCorruptAttributes(
  attrs: VariantAttribute[]
): VariantAttribute[] {
  return attrs.filter(
    ({ options, name }) => options.length !== 0 && name !== ""
  )
}

function updateInventoryItemNames(
  originalName: string,
  attrs: VariantAttribute[],
  storeNames: StoreName[],
  inventory?: DiscreteInventoryData
): DiscreteInventoryData {
  const itemNames = generateItemNames(
    originalName,
    removeCorruptAttributes(attrs)
  )
  let newInventory: DiscreteInventoryData
  if (inventory === undefined) {
    newInventory = {
      unit: { id: "0" },
      stores: storeNames.map(storeName => ({
        name: storeName.name,
        id: storeName.id,
        items: itemNames.map(itemName => ({
          id: itemName.id,
          name: itemName.name,
          amount: 0
        }))
      }))
    }
  } else if (itemNames.length === 0) {
    const storeWithUpdatedItem = inventory.stores.map(store => {
      const item = store.items[0]
      const newStore = { ...store }
      newStore.items[0] = {
        name: originalName,
        amount: item?.amount ?? 0,
        id: item?.id ?? originalName
      }
      return newStore
    })
    return { ...inventory, stores: storeWithUpdatedItem }
  } else {
    const storeWithUpdatedItem = inventory.stores.map(store => {
      const updatedItems = itemNames.map(name => {
        const amount = store.items.find(({ id }) => id === name.id)?.amount ?? 0
        return { ...name, amount }
      })
      return { ...store, items: updatedItems }
    })
    newInventory = { ...inventory, stores: storeWithUpdatedItem }
  }
  return newInventory
}

function setAttributes(
  state: ItemEditorState,
  attributes: VariantAttribute[]
): ItemEditorState {
  const inventory = updateInventoryItemNames(
    state.name,
    attributes,
    state.storeNames,
    state.discreteInventory
  )
  return { ...state, attributes, discreteInventory: inventory }
}

function reducer(
  state: ItemEditorState,
  action: ItemEditorActions
): ItemEditorState {
  switch (action.type) {
    case "setPrice":
      return { ...state, price: action.payload }
    case "setDescription":
      return { ...state, description: action.payload }
    case "setIsListed":
      return { ...state, isListed: action.payload }
    case "setItemImage":
      return { ...state, itemImage: action.payload }
    case "setInventoryType":
      return { ...state, inventoryType: action.payload }
    case "setItemRequirements":
      return { ...state, itemRequirements: action.payload }
    case "setStoreNames": {
      const inventory = updateInventoryItemNames(
        state.name,
        state.attributes,
        action.payload,
        state.discreteInventory
      )
      return {
        ...state,
        storeNames: action.payload,
        discreteInventory: inventory
      }
    }
    case "setName": {
      const inventory = updateInventoryItemNames(
        action.payload,
        state.attributes,
        state.storeNames,
        state.discreteInventory
      )
      return { ...state, name: action.payload, discreteInventory: inventory }
    }
    case "newEmptyAttributes": {
      const newAttribute = { id: nanoid(), options: [], name: "" }
      return setAttributes(state, [...state.attributes, newAttribute])
    }
    case "updateAttribute": {
      const idx = state.attributes.findIndex(
        ({ id }) => id === action.payload.id
      )
      const updatedAttributes = [...state.attributes]
      updatedAttributes[idx] = action.payload
      return setAttributes(state, updatedAttributes)
    }
    case "deleteAttribute": {
      const updatedAttributes = state.attributes.filter(
        ({ id }) => id !== action.payload.id
      )
      return setAttributes(state, updatedAttributes)
    }
    default:
      return state
  }
}

export const GET_STORE_NAMES = gql`
  query StoreNames {
    company {
      stores {
        edges {
          node {
            id
            name
          }
        }
      }
    }
  }
`
export const useItemEditorState = (): [
  ItemEditorState,
  Dispatch<ItemEditorActions>,
  boolean
] => {
  const { loading, data, error } = useQuery<StoreNamesQuery>(GET_STORE_NAMES)
  const [state, dispatch] = useReducer(reducer, initialState)

  useEffect(() => {
    // TODO: Error shouldn't be ignored
    if (loading || error) return
    if (data) {
      const names = data.company.stores.edges.map(({ node }) => ({
        id: node.id,
        name: node.name
      }))
      dispatch(itemEditorActions.setStoreNames(names))
    }
  }, [data, error, loading])

  return [state, dispatch, loading]
}
