import { ActionsUnion, createAction } from "../../reducerHelper"
import { Requirement } from "../DialogItemRequirementPicker/DialogItemRequirementPicker"
import { VariantAttribute } from "../FormVariantAttribute/FormVariantAttribute"
import { StoreName } from "./useItemEditorState"
import { DiscreteInventoryData } from "../FormDiscreteInventory/FormDiscreteInventory"

export const itemEditorActions = {
  setName: (name: string) => createAction("setName", name),
  setPrice: (price: number) => createAction("setPrice", price),
  setDescription: (description: string) =>
    createAction("setDescription", description),
  setIsListed: (isListed: boolean) => createAction("setIsListed", isListed),
  setItemImage: (itemImage?: File) => createAction("setItemImage", itemImage),
  setInventoryType: (type: number) => createAction("setInventoryType", type),
  setItemRequirements: (requirements: Requirement[]) =>
    createAction("setItemRequirements", requirements),
  newEmptyAttribute: () => createAction("newEmptyAttributes"),
  updateAttribute: (attribute: VariantAttribute) =>
    createAction("updateAttribute", attribute),
  deleteAttribute: (attribute: VariantAttribute) =>
    createAction("deleteAttribute", attribute),
  setStoreNames: (names: StoreName[]) => createAction("setStoreNames", names),
  setDiscreteInventory: (inventory: DiscreteInventoryData) =>
    createAction("setDiscreteInventory", inventory)
}
export type ItemEditorActions = ActionsUnion<typeof itemEditorActions>
