import React, { FC, useRef } from "react"
import { FormattedMessage } from "gatsby-plugin-intl3"
import Flex from "../Flex/Flex"
import Image from "../Image/Image"
import Icon from "../Icon/Icon"
import { ReactComponent as ImageIcon } from "../../icons/image.svg"
import Typography from "../Typography/Typography"
import Spacer from "../Spacer/Spacer"
import Button from "../Button/Button"
import { ReactComponent as EditIcon } from "../../icons/edit.svg"
import { ReactComponent as PlusIcon } from "../../icons/plus.svg"
import { ReactComponent as TrashIcon } from "../../icons/trash.svg"
import { useFileUrl } from "../hooks"

interface Props {
  value?: File
  onChange: (file?: File) => void
}
const ProductImageInput: FC<Props> = ({ value, onChange }) => {
  const fileInputRef = useRef<HTMLInputElement>(null)
  const fileUrl = useFileUrl(value)

  return (
    <>
      <input
        style={{ display: "none" }}
        accept="image/png, image/jpeg"
        type="file"
        ref={fileInputRef}
        onChange={e => onChange(e.target.files?.[0])}
      />
      <Flex my={3}>
        <Flex
          alignItems="center"
          justifyContent="center"
          height={80}
          width={80}
          backgroundColor="surface"
          sx={{
            borderStyle: "solid",
            borderRadius: "default",
            borderWidth: 1,
            borderColor: "border",
            outline: "none"
          }}
        >
          {fileUrl ? (
            <Image src={fileUrl} />
          ) : (
            <Icon as={ImageIcon} size={32} />
          )}
        </Flex>
        <Flex ml={3} flexDirection="column">
          <Typography.Body color="textMediumEmphasis" mb={2}>
            <FormattedMessage id="itemImage" />
          </Typography.Body>
          <Spacer />
          <Flex>
            <Button
              icon={value ? EditIcon : PlusIcon}
              variant="outline"
              type="button"
              onClick={() => fileInputRef?.current?.click()}
            >
              {value ? (
                <FormattedMessage id="change" />
              ) : (
                <FormattedMessage id="add" />
              )}
            </Button>
            {fileUrl && (
              <Button
                type="button"
                variant="outline"
                color="danger"
                ml={2}
                onClick={() => onChange(undefined)}
              >
                <Icon as={TrashIcon} m={0} fill="danger" />
              </Button>
            )}
          </Flex>
        </Flex>
      </Flex>
    </>
  )
}

export default ProductImageInput
