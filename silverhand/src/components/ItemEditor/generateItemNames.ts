import { VariantAttribute } from "../FormVariantAttribute/FormVariantAttribute"

const nextIndices = (
  indices: readonly number[],
  topMostIndices: readonly number[],
  indexToIncrement: number
): number[] => {
  const newIndices = [...indices]
  if (indices[indexToIncrement] > 0) {
    newIndices[indexToIncrement] -= 1
    return newIndices
  }
  if (indexToIncrement === 0) return newIndices

  newIndices[indexToIncrement] = topMostIndices[indexToIncrement]
  return nextIndices(newIndices, topMostIndices, indexToIncrement - 1)
}

function constructName(
  originalName: string,
  attributes: readonly VariantAttribute[],
  coordinate: readonly number[]
): string {
  const currentVariantValues = coordinate.map(
    (axis, idx) => attributes[idx].options[axis].value
  )
  return `${originalName} ${currentVariantValues.join(" ")}`
}

interface ItemNames {
  id: string
  name: string
}
export function generateItemNames(
  originalName: string,
  attributes: readonly VariantAttribute[]
): ItemNames[] {
  const names: ItemNames[] = []

  if (attributes.length === 0) {
    names.push({ name: originalName, id: originalName })
    return names
  }

  const topMostIndices = attributes.map(
    attribute => attribute.options.length - 1
  )
  let currentIndices = [...topMostIndices]
  let isNotFinalIndices
  do {
    isNotFinalIndices = currentIndices.some(axis => axis > 0)
    names.push({
      name: constructName(originalName, attributes, currentIndices),
      id: currentIndices
        .map(
          (valueIndex, attributeIndex) =>
            attributes[attributeIndex].id + valueIndex
        )
        .join("-")
    })

    currentIndices = nextIndices(
      currentIndices,
      topMostIndices,
      currentIndices.length - 1
    )
  } while (isNotFinalIndices)

  return names
}
