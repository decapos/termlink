import React, { FC, useState } from "react"
import { FormattedMessage } from "gatsby-plugin-intl3"
import { useQuery } from "@apollo/react-hooks"
import Button from "../Button/Button"
import { ReactComponent as PlusIcon } from "../../icons/plus.svg"
import Box from "../Box/Box"
import Typography from "../Typography/Typography"
import Flex from "../Flex/Flex"
import Input from "../Input/Input"
import Icon from "../Icon/Icon"
import { ReactComponent as TrashIcon } from "../../icons/trash.svg"
import { useFormatMessage } from "../hooks"
import { ItemNamesQuery } from "../../apollo/graphqlTypes"
import LoadingPlaceholder from "../LoadingPlaceholder/LoadingPlaceholder"
import DialogItemRequirementPicker, {
  GET_ITEM_NAMES,
  Requirement
} from "../DialogItemRequirementPicker/DialogItemRequirementPicker"
import Spacer from "../Spacer/Spacer"

interface Props {
  itemRequirements: Requirement[]
  onChangeItemRequirements: (requirements: Requirement[]) => void
}
const DependentInventory: FC<Props> = ({
  itemRequirements,
  onChangeItemRequirements
}) => {
  const [showItemPicker, setShowItemPicker] = useState(false)
  const [selectedItems, setSelectedItems] = useState<Requirement[]>([])
  const { data, loading } = useQuery<ItemNamesQuery>(GET_ITEM_NAMES)

  function resetDialogItemPicker(): void {
    setShowItemPicker(false)
    setSelectedItems([])
  }
  function saveSelectedItems(): void {
    onChangeItemRequirements([...itemRequirements, ...selectedItems])
    resetDialogItemPicker()
  }
  function handleRequirementChange(changedRequirement: Requirement): void {
    const updatedRequirements = [...itemRequirements]
    const changedRequirementIdx = updatedRequirements.findIndex(
      ({ id }) => id === changedRequirement.id
    )
    updatedRequirements[changedRequirementIdx] = changedRequirement
    onChangeItemRequirements(updatedRequirements)
  }
  function handleRequirementDelete(deletedRequirement: Requirement): void {
    const updatedRequirements = itemRequirements.filter(
      ({ id }) => id !== deletedRequirement.id
    )
    onChangeItemRequirements(updatedRequirements)
  }

  if (loading) {
    return <LoadingPlaceholder width="100%" />
  }
  const haveNoOtherItem = data?.items.edges.length === 0

  return (
    <>
      {haveNoOtherItem && <EmptyItemWarning />}
      {!haveNoOtherItem && itemRequirements.length === 0 && (
        <EmptyDependencyWarning />
      )}
      {itemRequirements.map(requirement => (
        <RequiredItem
          key={requirement.id}
          requirement={requirement}
          onChange={handleRequirementChange}
          onDelete={handleRequirementDelete}
        />
      ))}
      <Button
        mt={3}
        type="button"
        icon={PlusIcon}
        variant="outline"
        width={["100%"]}
        data-cy="addRequiredItem"
        onClick={() => setShowItemPicker(true)}
        disabled={haveNoOtherItem}
      >
        <FormattedMessage id="addRequiredItem" />
      </Button>
      {showItemPicker && (
        <DialogItemRequirementPicker
          excludedItems={itemRequirements}
          onSelectedItemsChange={setSelectedItems}
          selectedItems={selectedItems}
          onDismiss={resetDialogItemPicker}
          onNegativeClick={resetDialogItemPicker}
          onPositiveClick={saveSelectedItems}
        />
      )}
    </>
  )
}

const EmptyDependencyWarning: FC = () => (
  <Flex
    justifyContent="center"
    p={2}
    backgroundColor="surface"
    sx={{
      borderRadius: "default",
      borderWidth: 2,
      borderColor: "warning",
      borderStyle: "solid"
    }}
  >
    <Typography.Body mb={0}>
      <FormattedMessage id="addAtLeastOneItem" />
    </Typography.Body>
  </Flex>
)

const EmptyItemWarning: FC = () => (
  <Flex
    justifyContent="center"
    p={2}
    backgroundColor="surface"
    sx={{
      borderRadius: "default",
      borderWidth: 2,
      borderColor: "error",
      borderStyle: "solid"
    }}
  >
    <Typography.Body mb={0} textAlign="center">
      <FormattedMessage id="youNeedAtLeastOneItemSaved" />
    </Typography.Body>
  </Flex>
)

const RequiredItem: FC<{
  requirement: Requirement
  onChange: (requirement: Requirement) => void
  onDelete: (requirement: Requirement) => void
}> = ({ requirement, onChange, onDelete }) => {
  function handleAmountChange(amount: number): void {
    onChange({ ...requirement, amount })
  }

  return (
    <Box
      backgroundColor="background"
      p={3}
      mb={3}
      sx={{
        borderRadius: "default",
        borderWidth: 1,
        borderColor: "border",
        borderStyle: "solid"
      }}
    >
      <Typography.Body
        mb={2}
        lineHeight={1}
        fontSize={0}
        color="textMediumEmphasis"
      >
        <FormattedMessage id="itemName" />
      </Typography.Body>
      <Flex mb={3}>
        <Typography.H5 mb={0}>{requirement.name}</Typography.H5>
        <Spacer />
        <Button
          type="button"
          variant="outline"
          ml={2}
          py={0}
          px={2}
          onClick={() => onDelete(requirement)}
        >
          <Icon as={TrashIcon} m={0} fill="danger" />
        </Button>
      </Flex>
      <Input
        label={useFormatMessage("quantity")}
        width="100%"
        small
        value={requirement.amount}
        type="number"
        onChange={e => handleAmountChange(parseInt(e.target.value, 10))}
      />
    </Box>
  )
}

export default DependentInventory
