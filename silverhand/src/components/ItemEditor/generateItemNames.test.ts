import { renderHook } from "@testing-library/react-hooks"
import nanoid from "nanoid"
import { generateItemNames } from "./generateItemNames"
import { VariantAttribute } from "../FormVariantAttribute/FormVariantAttribute"

describe("generateItemNames hook test", () => {
  const originalName = "T-Shirt"

  it("should return original name when attribute is empty", () => {
    const input: VariantAttribute[] = []
    const { result } = renderHook(() => generateItemNames(originalName, input))
    expect(result.current).toHaveLength(1)
    expect(result.current[0].name).toEqual(originalName)
  })

  it("should be able to generate item with a single variant", () => {
    const variants: VariantAttribute[] = [
      {
        id: nanoid(),
        name: "Colors",
        options: [
          {
            id: nanoid(),
            value: "White"
          }
        ]
      }
    ]
    const { result } = renderHook(() =>
      generateItemNames(originalName, variants)
    )
    expect(result.current).toHaveLength(1)
    expect(result.current[0].name.includes(originalName)).toBeTruthy()
    expect(
      result.current[0].name.includes(variants[0].options[0].value)
    ).toBeTruthy()
  })

  it("should be able to generate item with two variant", () => {
    const variants: VariantAttribute[] = [
      {
        id: nanoid(),
        name: "Colors",
        options: [
          {
            id: nanoid(),
            value: "White"
          },
          {
            id: nanoid(),
            value: "Black"
          }
        ]
      }
    ]
    const { result } = renderHook(() =>
      generateItemNames(originalName, variants)
    )
    expect(result.current).toHaveLength(2)
    expect(result.current[0].name.includes(originalName)).toBeTruthy()
    expect(
      variants[0].options.every(({ value }) =>
        result.current.some(item => item.name.includes(value))
      )
    ).toBeTruthy()
  })

  it("should be able to generate item with two variant type", () => {
    const variants: VariantAttribute[] = [
      {
        id: nanoid(),
        name: "Colors",
        options: [
          {
            id: nanoid(),
            value: "White"
          },
          {
            id: nanoid(),
            value: "Black"
          }
        ]
      },
      {
        id: nanoid(),
        name: "Size",
        options: [
          {
            id: nanoid(),
            value: "SM"
          },
          {
            id: nanoid(),
            value: "M"
          },
          {
            id: nanoid(),
            value: "XL"
          }
        ]
      }
    ]
    const { result } = renderHook(() =>
      generateItemNames(originalName, variants)
    )
    expect(result.current).toHaveLength(6)
    expect(
      result.current.every(item => item.name.includes(originalName))
    ).toBeTruthy()
    expect(
      variants[0].options.every(({ value }) =>
        result.current.some(item => item.name.includes(value))
      )
    ).toBeTruthy()
  })
})
