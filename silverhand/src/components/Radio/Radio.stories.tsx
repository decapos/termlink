import React, { FC } from "react"
import Radio from "./Radio"

export default {
  title: "Core|Forms/Radio",
  component: Radio,
  parameters: {
    componentSubtitle: "Just a simple Radio"
  }
}

export const Basic: FC = () => <Radio />
