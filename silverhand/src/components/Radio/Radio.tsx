import React, { FC } from "react"
import { Radio as BaseRadio } from "@rebass/forms"
import { BoxProps } from "../Box/Box"

export const Radio: FC<BoxProps> = props => <BaseRadio {...props} />

export default Radio
