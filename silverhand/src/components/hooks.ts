import { RefObject, useEffect, useState } from "react"
import { useThemeUI } from "theme-ui"
import { useIntl } from "gatsby-plugin-intl3"
import { SilverhandTheme } from "../gatsby-plugin-theme-ui"
import en from "../intl/en.json"
import { isString } from "../utils"

export const useOutsideClick = (
  ref: RefObject<HTMLElement>,
  callback: () => void
): void => {
  const handleClick: EventListener = e => {
    if (ref.current && !ref.current.contains(e.target as Node)) {
      callback()
    }
  }

  useEffect(() => {
    document.addEventListener("click", handleClick)
    return () => {
      document.removeEventListener("click", handleClick)
    }
  })
}

export const useFormatMessage = (
  id: keyof typeof en,
  values?: Record<string, string | number | boolean | null | undefined | Date>
): string => {
  const intl = useIntl()
  return intl.formatMessage({ id }, values)
}

export const useSilverhandTheme = (): SilverhandTheme => {
  const themeUi = useThemeUI()
  return themeUi.theme as SilverhandTheme
}

export const useFileUrl = (value?: File): string | undefined => {
  const [fileUrl, setFileUrl] = useState<string>()

  useEffect(() => {
    const reader = new FileReader()
    reader.onloadend = () => {
      const { result } = reader
      isString(result, "File reader result is not a string")
      setFileUrl(result ?? "")
    }
    if (value) {
      reader.readAsDataURL(value)
    } else {
      setFileUrl(undefined)
    }
  }, [value])

  return fileUrl
}
