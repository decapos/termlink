import React, { FC } from "react"
import PagePos from "./PagePos"

export default {
  title: "Pages|PagePos",
  component: PagePos,
  parameters: {
    componentSubtitle: "Just a simple PagePos"
  }
}

export const Basic: FC = () => <PagePos />
