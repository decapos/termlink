import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./PagePos.stories"

describe("PagePos", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
