import React, { FC } from "react"
import EmptyState from "../EmptyState/EmptyState"
import { useEmptyInventoryImage } from "../../gatsby/useEmptyInventoryImage"

export const PageInventory: FC = () => {
  const emptyInventoryImage = useEmptyInventoryImage()
  return (
    <EmptyState
      titleIntlId="startTrackingInventory"
      descriptionIntlId="startTrackingInventoryDesc"
      image={emptyInventoryImage}
      buttonTextIntlId="startTracking"
      buttonDestination="/items"
      imageProps={{ mt: [-50, -50, -90] }}
    />
  )
}

export default PageInventory
