import React, { FC } from "react"
import PageInventory from "./PageInventory"

export default {
  title: "Pages|PageInventory",
  component: PageInventory,
  parameters: {
    componentSubtitle: "Just a simple PageInventory"
  }
}

export const Basic: FC = () => <PageInventory />
