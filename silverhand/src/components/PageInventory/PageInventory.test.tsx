import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./PageInventory.stories"

describe("PageInventory", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
