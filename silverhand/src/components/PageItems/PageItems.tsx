import React, { FC, useState } from "react"

import {
  FormattedMessage,
  FormattedNumber,
  navigate
} from "gatsby-plugin-intl3"
import { gql } from "apollo-boost"
import { useQuery } from "@apollo/react-hooks"
import { Button } from "../Button/Button"
import Card from "../Card/Card"
import { Typography } from "../Typography/Typography"
import { Item, ItemsQuery } from "../../apollo/graphqlTypes"
import Spacer from "../Spacer/Spacer"
import Checkbox from "../Checkbox/Checkbox"
import SearchBar from "../SearchBar/SearchBar"
import { useNoItemImage } from "../../gatsby/useNoItemImage"
import { Image } from "../Image/Image"
import { ReactComponent as ArrowBackBig } from "../../icons/arrow-back-big.svg"
import { ReactComponent as ArrowForwardBig } from "../../icons/arrow-forward-big.svg"
import EmptyState from "../EmptyState/EmptyState"
import Box from "../Box/Box"
import Flex from "../Flex/Flex"
import Icon from "../Icon/Icon"
import LoadingPlaceholder from "../LoadingPlaceholder/LoadingPlaceholder"

export const GET_ITEMS = gql`
  query Items(
    $searchTerm: String!
    $first: Int
    $after: Cursor
    $last: Int
    $before: Cursor
  ) {
    items(
      filter: $searchTerm
      first: $first
      after: $after
      before: $before
      last: $last
    ) {
      pageInfo {
        endCursor
        hasNextPage
        hasPreviousPage
        startCursor
      }
      edges {
        cursor
        node {
          id
          isListed
          name
          price
          imagePath
        }
      }
    }
  }
`
export const PageItems: FC = () => {
  const noItemImage = useNoItemImage()
  const [searchTerm, setSearchTerm] = useState("")
  const [pagination, setPagination] = useState<PaginationArguments>()
  const [selectedIds, setSelectedIds] = useState<string[]>([])
  const [selectAll, setSelectAll] = useState(false)
  const { loading, data } = useQuery<ItemsQuery>(GET_ITEMS, {
    variables: { searchTerm, ...pagination }
  })

  if (loading) {
    return (
      <Box p={[0, 3]} maxWidth={1200} margin="auto">
        <Flex m={[3, 0]} py={3} alignItems="center">
          <LoadingPlaceholder height="4rem" width="10rem" />
          <Spacer />
          <LoadingPlaceholder height="4rem" width="10rem" />
        </Flex>
        <Box my={3} mx={[3, 0]}>
          <LoadingPlaceholder height="5rem" width="100%" />
        </Box>
        <Box my={3}>
          <LoadingPlaceholder height="60vh" width="100%" />
        </Box>
        <Flex m={3} justifyContent="center">
          <LoadingPlaceholder height="5rem" width="8rem" mr={3} />
          <LoadingPlaceholder height="5rem" width="8rem" />
        </Flex>
      </Box>
    )
  }
  if (!data || !data.items || !data.items.edges) {
    return <Box>Error</Box>
  }
  const items = data.items.edges.map(item => item.node)
  const paginationButtons = (
    <>
      <Button
        variant="outline"
        mr={2}
        disabled={!data?.items.pageInfo.hasPreviousPage}
        onClick={() =>
          setPagination({
            last: 10,
            before: data?.items.pageInfo.startCursor
          })
        }
      >
        <Icon as={ArrowBackBig} m={0} />
      </Button>
      <Button
        variant="outline"
        disabled={!data?.items.pageInfo.hasNextPage}
        onClick={() =>
          setPagination({
            first: 10,
            after: data?.items.pageInfo.endCursor
          })
        }
      >
        <Icon as={ArrowForwardBig} m={0} />
      </Button>
    </>
  )
  return items.length === 0 ? (
    <EmptyState
      titleIntlId="addYourFirstItem"
      descriptionIntlId="addYourFirstItemDesc"
      image={noItemImage}
      buttonTextIntlId="addItem"
      buttonDestination="/items/new"
    />
  ) : (
    <Box p={[0, 3]} maxWidth={1200} margin="auto">
      <Flex m={[3, 0]} pt={[2, 0]} alignItems="center">
        <Typography.H4 m={0}>
          <FormattedMessage id="items" />
        </Typography.H4>
        <Spacer />
        {selectedIds.length > 0 && (
          <Button variant="outline" color="danger" mr={2}>
            <FormattedMessage id="delete" />
            {` ${selectedIds.length}`}
          </Button>
        )}
        {selectAll && (
          <Button variant="outline" color="danger" mr={2}>
            <FormattedMessage id="deleteAll" />
          </Button>
        )}
        <Button variant="primary">
          <FormattedMessage id="addItem" />
        </Button>
      </Flex>
      <SearchBar
        px={[3, 0]}
        onChange={e => setSearchTerm(e.target.value)}
        value={searchTerm}
      />
      <ProductTable
        items={items}
        onItemSelected={id => setSelectedIds([...selectedIds, id ?? ""])}
        onItemDeselected={id =>
          setSelectedIds(selectedIds.filter(selectedId => id !== selectedId))
        }
        onSelectAllChange={value => {
          setSelectedIds([])
          setSelectAll(value)
        }}
        selectAll={selectAll}
        selectedIds={selectedIds}
      />
      <Flex p={3} justifyContent="center">
        {paginationButtons}
      </Flex>
    </Box>
  )
}

interface ProductTableProps {
  items: Partial<Item>[]
  onItemSelected: (id: string) => void
  onItemDeselected: (id: string) => void
  onSelectAllChange: (value: boolean) => void
  selectAll: boolean
  selectedIds: string[]
}
const ProductTable: FC<ProductTableProps> = ({
  items,
  selectedIds,
  onItemDeselected,
  onItemSelected,
  onSelectAllChange,
  selectAll
}) => (
  <Card mt={[0, 3]} p={3} pt={0} borderRadius={[0, "default"]}>
    <Box as="table" width="100%" sx={{ borderCollapse: "collapse" }}>
      <TableHeader
        onSelectAllChange={onSelectAllChange}
        selectAll={selectAll}
      />
      <tbody>
        {items.map(({ imagePath, name, price, id }) => (
          <TableRow
            id={id}
            price={price}
            name={name}
            key={id}
            thumbnailSrc={imagePath}
            isSelected={selectAll || selectedIds.includes(id ?? "n/a")}
            onSelectChange={value =>
              value
                ? onItemSelected(id ?? "n/a")
                : onItemDeselected(id ?? "n/a")
            }
          />
        ))}
      </tbody>
    </Box>
  </Card>
)

interface TableHeaderProps {
  onSelectAllChange: (value: boolean) => void
  selectAll: boolean
}
const TableHeader: FC<TableHeaderProps> = ({
  onSelectAllChange,
  selectAll
}) => (
  <Box as="thead" height="table.row">
    <tr>
      <Box as="th" width="24px" height="table.header">
        <Checkbox
          checked={selectAll}
          onClick={e => e.stopPropagation()}
          aria-label="select$ all"
          sx={{ alignItems: "center", cursor: "pointer", userSelect: "none" }}
          m={3}
          onChange={e => onSelectAllChange(e.target.checked)}
        />
      </Box>
      <Typography.Body
        height="table.header"
        as="th"
        fontWeight="bold"
        textAlign="center"
        width={50}
        fontSize={1}
        pl={2}
      >
        <FormattedMessage id="image" />
      </Typography.Body>
      <Typography.Body
        height="tabler.header"
        as="th"
        fontSize={1}
        fontWeight="bold"
        textAlign="left"
        px={3}
      >
        <FormattedMessage id="name" />
      </Typography.Body>
      <Typography.Body
        height="table.header"
        as="th"
        fontSize={1}
        fontWeight="bold"
        textAlign="right"
        px={3}
      >
        <FormattedMessage id="price" />
      </Typography.Body>
    </tr>
  </Box>
)

interface TableRowProps {
  id?: string
  name?: string
  price?: number
  thumbnailSrc?: string
  isSelected?: boolean
  onSelectChange: (value: boolean) => void
}
const TableRow: FC<TableRowProps> = ({
  id,
  name = "N/A",
  price = 0,
  thumbnailSrc = "",
  isSelected,
  onSelectChange
}) => {
  return (
    <Box
      as="tr"
      verticalAlign="center"
      p={3}
      onClick={() => navigate(`/items/edit?id=${id}`)}
      sx={{
        cursor: "pointer",
        "&:hover": {
          backgroundColor: "primaryLightest"
        }
      }}
    >
      <Box as="td" width="24px">
        <Checkbox
          checked={isSelected}
          onChange={e => onSelectChange(e.target.checked)}
          onClick={e => e.stopPropagation()}
          aria-label={`select${name}`}
          sx={{ alignItems: "center", cursor: "pointer", userSelect: "none" }}
          m={3}
        />
      </Box>
      <Box as="td" ml={2} onClick={() => navigate("")} width={50} sx={{}}>
        <Box
          width={50}
          pb={50}
          my={2}
          sx={{
            position: "relative",
            borderColor: "border",
            borderWidth: 1,
            borderStyle: "solid"
          }}
        >
          <Image
            alt={name}
            src={thumbnailSrc}
            margin="auto"
            maxHeight="100%"
            maxWidth="100%"
            sx={{ position: "absolute", top: 0, bottom: 0, left: 0, right: 0 }}
          />
        </Box>
      </Box>
      <Typography.Body as="td" mb={0} textAlign="left" px={3}>
        {name}
      </Typography.Body>
      <Typography.Body
        mb={0}
        as="td"
        textAlign="right"
        px={3}
        sx={{ whiteSpace: "nowrap" }}
      >
        <FormattedNumber
          value={price}
          style="currency"
          currency="IDR"
          currencyDisplay="code"
        />
      </Typography.Body>
    </Box>
  )
}

export default PageItems
