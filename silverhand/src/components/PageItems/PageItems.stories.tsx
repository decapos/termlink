import React, { FC } from "react"
import PageItems from "./PageItems"
import Layout from "../Layout/Layout"

export default {
  title: "Pages|PageItems",
  component: PageItems,
  parameters: {
    componentSubtitle: "Just a simple PageItems"
  }
}

export const Basic: FC = () => (
  <Layout pageTitleIntlId="items">
    <PageItems />
  </Layout>
)
