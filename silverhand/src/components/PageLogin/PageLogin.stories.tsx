import React, { FC } from "react"
import PageLogin from "./PageLogin"

export default {
  title: "Pages|PageLogin",
  component: PageLogin,
  parameters: {
    componentSubtitle: "Just a simple PageLogin"
  }
}

export const Basic: FC = () => <PageLogin />
