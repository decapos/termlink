import React, { FC } from "react"

import { FormattedMessage, Link } from "gatsby-plugin-intl3"
import { prefetchPathname } from "gatsby"
import { Typography } from "../Typography/Typography"
import Button from "../Button/Button"
import Input from "../Input/Input"
import Spacer from "../Spacer/Spacer"
import Checkbox from "../Checkbox/Checkbox"
import { useFormatMessage } from "../hooks"
import Flex from "../Flex/Flex"
import Box from "../Box/Box"

export const PageLogin: FC = () => (
  <Flex
    flexDirection="column"
    alignItems="center"
    justifyContent="center"
    minHeight={["100%", "100vh"]}
    backgroundColor="background"
  >
    <Header />
    <Flex as="form" flexDirection="column" p={3} width="100%" maxWidth="500px">
      <WelcomeMessage />
      <EmailField />
      <PasswordField />
      <Flex alignItems="center" mb={3}>
        <Checkbox
          id="remember-me"
          label={useFormatMessage("rememberMe")}
          type="checkbox"
          name="remember-me"
        />
        <ForgotPasswordLink />
      </Flex>
      <Button
        onMouseEnter={() => prefetchPathname("/")}
        variant="primaryBig"
        aria-label="submit"
        type="button"
      >
        LOGIN
      </Button>
    </Flex>
  </Flex>
)

const Header: FC = () => (
  <Flex
    width="100%"
    flexDirection="row"
    p={3}
    sx={{
      position: ["relative", "fixed"],
      top: 0,
      left: 0
    }}
  >
    <Spacer />
    <Box>
      <Link to="/early-access">
        <Button variant="outlineBig">Request Early Access</Button>
      </Link>
    </Box>
  </Flex>
)

const WelcomeMessage: FC = () => (
  <>
    <Typography.H3 mb={0}>
      <FormattedMessage id="welcomeBack" />,
    </Typography.H3>
    <Typography.Body mb={4}>
      <FormattedMessage id="loginWelcomeMessage" />
    </Typography.Body>
  </>
)

const EmailField: FC = () => (
  <Input
    label={useFormatMessage("email")}
    id="email"
    name="email"
    aria-label="email"
    width="100%"
  />
)

const PasswordField: FC = () => (
  <Input
    label={useFormatMessage("password")}
    my={3}
    id="email"
    name="email"
    aria-label="email"
    width="100%"
  />
)

const ForgotPasswordLink: FC = () => (
  <Link to="/forgot-password">
    <Typography.Body
      color="text"
      variant="body"
      fontSize={1}
      opacity={0.7}
      mb={0}
      lineHeight={1}
      sx={{
        whiteSpace: "nowrap",
        textDecoration: "underline",
        textDecorationColor: "text"
      }}
    >
      <FormattedMessage id="forgotPassword" />?
    </Typography.Body>
  </Link>
)

export default PageLogin
