import React, { FC, useState } from "react"
import { changeLocale, FormattedMessage, useIntl } from "gatsby-plugin-intl3"
import { useColorMode } from "theme-ui"
import AppBar, { StoreName } from "../AppBar/AppBar"
import SideBar from "../SideBar/SideBar"
import { ReactComponent as HomeIcon } from "../../icons/home.svg"
import { ReactComponent as ProductIcon } from "../../icons/product.svg"
import { ReactComponent as InventoryIcon } from "../../icons/inventory.svg"
import { ReactComponent as SettingsIcon } from "../../icons/settings.svg"
import { ReactComponent as PhoneIcon } from "../../icons/phone.svg"
import { ReactComponent as LightModeIcon } from "../../icons/light-mode.svg"
import { ReactComponent as DarkModeIcon } from "../../icons/dark-mode.svg"
import { ReactComponent as IntlIcon } from "../../icons/intl.svg"
import NavigationItem from "../NavigationItem/NavigationItem"
import { Typography } from "../Typography/Typography"
import Spacer from "../Spacer/Spacer"
import { useFormatMessage } from "../hooks"
import Icon from "../Icon/Icon"
import en from "../../intl/en.json"
import Box from "../Box/Box"
import Flex, { FlexProps } from "../Flex/Flex"

/** Top level component that encapsulate every page and provides navigation and
 * everything */
interface Props {
  pageTitleIntlId: keyof typeof en
}
export const Layout: FC<Props> = ({ pageTitleIntlId, children }) => {
  const [isSidebarShown, setShowSidebar] = useState(false)
  const toggleSidebar = (): void => setShowSidebar(!isSidebarShown)
  return (
    <>
      <AppBar
        title={useFormatMessage(pageTitleIntlId)}
        onMenuClick={toggleSidebar}
        position="fixed"
      />
      <MainSidebar closeSidebar={toggleSidebar} isShown={isSidebarShown} />
      <Box
        as="main"
        width="100%"
        mt="57px"
        pl={[0, 230]}
        backgroundColor="background"
      >
        {children}
      </Box>
    </>
  )
}

const MainSidebar: FC<{
  closeSidebar: () => void
  isShown: boolean
}> = ({ closeSidebar, isShown }) => {
  const [colorMode, setColorMode] = useColorMode()
  const intl = useIntl()
  return (
    <SideBar isShown={isShown} onOutsideClick={closeSidebar}>
      <StoreName
        backgroundColor="surface"
        mb={3}
        alignItems="center"
        width="100%"
        opacity={[1, 0]}
        sx={{ boxShadow: "low" }}
      />
      <NavigationItem
        onClick={closeSidebar}
        to="/"
        textIntlId="home"
        icon={HomeIcon}
      />
      <NavigationItem
        onClick={closeSidebar}
        to="/items"
        textIntlId="items"
        icon={ProductIcon}
      />
      <NavigationItem
        onClick={closeSidebar}
        to="/inventory"
        textIntlId="inventory"
        icon={InventoryIcon}
      />
      <Typography.Body mt={3} fontWeight="bold" fontSize="12px" mb={1} ml={3}>
        <FormattedMessage id="applications" />
      </Typography.Body>
      <NavigationItem
        onClick={closeSidebar}
        to="/pos"
        textIntlId="pos"
        icon={PhoneIcon}
      />
      <Spacer />
      <NavigationItem
        onClick={closeSidebar}
        to="/settings"
        textIntlId="settings"
        icon={SettingsIcon}
        mb={3}
      />
      <SidebarToggleItem
        onClick={() => {
          setColorMode(colorMode === "dark" ? "default" : "dark")
        }}
        textIntlId={colorMode === "dark" ? "lightMode" : "darkMode"}
        icon={colorMode === "dark" ? LightModeIcon : DarkModeIcon}
        data-cy="toggleTheme"
      />
      <SidebarToggleItem
        onClick={() => {
          changeLocale(intl.locale === "en" ? "id" : "")
        }}
        textIntlId={intl.locale === "en" ? "switchIndonesian" : "switchEnglish"}
        icon={IntlIcon}
        data-cy="toggleLang"
      />
    </SideBar>
  )
}

interface SidebarToggleItemProps extends FlexProps {
  textIntlId: string
  icon?: FC
}
export const SidebarToggleItem: FC<SidebarToggleItemProps> = ({
  textIntlId,
  icon,
  ...props
}) => {
  return (
    // eslint-disable-next-line react/jsx-no-undef
    <Flex
      as="button"
      alignItems="center"
      m={2}
      my={0}
      pl={2}
      sx={{
        borderStyle: "none",
        cursor: "pointer",
        borderRadius: "default",
        textDecoration: "none",
        backgroundColor: "transparent",
        "&:hover": {
          backgroundColor: "primaryLightest"
        }
      }}
      {...props}
    >
      <Icon
        alt={`${textIntlId} icon`}
        ml={0}
        my={2}
        as={icon}
        sx={{ fill: "textMediumEmphasis" }}
      />
      <Typography.Body m={0} fontSize={1} color="textMediumEmphasis">
        <FormattedMessage id={textIntlId} />
      </Typography.Body>
    </Flex>
  )
}

export default Layout
