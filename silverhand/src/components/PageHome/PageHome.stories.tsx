import React, { FC } from "react"
import PageHome from "./PageHome"
import Layout from "../Layout/Layout"

export default {
  title: "Pages|PageHome",
  component: PageHome,
  parameters: {
    componentSubtitle: "Where user landed on visit."
  }
}

export const Basic: FC = () => <PageHome />
export const WithNavigations: FC = () => {
  return (
    <Layout pageTitleIntlId="home">
      <PageHome />
    </Layout>
  )
}
