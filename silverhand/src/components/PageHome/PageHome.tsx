import React, { FC, useState } from "react"
import Tab from "../Tab/Tab"
import Overview from "../Overview/Overview"
import Activities from "../Activities/Activities"
import Typography from "../Typography/Typography"
import Flex from "../Flex/Flex"
import Box from "../Box/Box"

export const PageHome: FC = () => {
  const [mobileSelectedTab, setMobileSelectedTab] = useState(0)
  return (
    <Flex
      backgroundColor="background"
      flexDirection={["column", "column", "column", "row"]}
      height={["auto", "auto", "auto", "100%"]}
    >
      <Tab
        onTabClick={setMobileSelectedTab}
        selectedItemIdx={mobileSelectedTab}
        items={["Overview", "Activities"]}
        display={["inherit", "flex", "flex", "none"]}
      />
      <OverviewSection shouldBeShown={mobileSelectedTab === 0} />
      <ActivitySection shouldBeShown={mobileSelectedTab === 1} />
    </Flex>
  )
}

interface SectionProps {
  shouldBeShown: boolean
}
const OverviewSection: FC<SectionProps> = ({ shouldBeShown }) => {
  const mobileDisplayCSS = shouldBeShown ? "block" : "none"
  return (
    <Box
      p={[0, 3]}
      width="100%"
      overflowY="auto"
      display={[mobileDisplayCSS, mobileDisplayCSS, mobileDisplayCSS, "block"]}
    >
      <Typography.H4 m={0} display={["none", "none", "none", "block"]}>
        Overview
      </Typography.H4>
      <Overview />
    </Box>
  )
}

const ActivitySection: FC<SectionProps> = ({ shouldBeShown }) => {
  const mobileDisplayCSS = shouldBeShown ? "block" : "none"
  return (
    <Box
      width={["100%", "100%", 480]}
      display={[mobileDisplayCSS, mobileDisplayCSS, mobileDisplayCSS, "block"]}
      overflowY="auto"
      px={[0, 2]}
    >
      <Activities />
    </Box>
  )
}

export default PageHome
