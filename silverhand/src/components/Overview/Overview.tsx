import React, { FC, useState } from "react"
import ToggleButton from "../ToggleButton/ToggleButton"
import CardPopularProducts from "../CardPopularProducts/CardPopularProducts"
import { ReportTimeRange } from "../../apollo/graphqlTypes"
import CardTotalSales from "../CardTotalSales/CardTotalSales"
import { Flex } from "../Flex/Flex"

/**
 * Shows the bird eye view of the whole company. Viewing this section
 * should allow user to identify trends and problems that are currently occurring
 * in their business. User should also able to decide what action they should take
 * after looking at this section.
 * */
export const Overview: FC = () => {
  const [dateRangeIdx, setDateRangeIdx] = useState(2)

  let selectedRange: ReportTimeRange
  switch (dateRangeIdx) {
    case 1:
      selectedRange = ReportTimeRange.DAILY
      break
    case 2:
      selectedRange = ReportTimeRange.WEEKLY
      break
    case 3:
      selectedRange = ReportTimeRange.MONTHLY
      break
    default:
      selectedRange = ReportTimeRange.YEARLY
  }

  return (
    <>
      <Flex
        data-cy="overview"
        py={3}
        pt={[3, 0, 0, 3]}
        px={[3, 0]}
        flexDirection="row"
      >
        <ToggleButton
          width={["100%", "100%", "100%", "inherit"]}
          itemFlexProp={[1, 1, 1, "inherit"]}
          values={["24 Hours", "7 Days", "30 Days"]}
          selectedItemIdx={dateRangeIdx}
          onItemClick={setDateRangeIdx}
        />
      </Flex>
      <CardTotalSales timeRange={selectedRange} />
      <CardPopularProducts timeRange={selectedRange} />
    </>
  )
}

export default Overview
