import React, { FC } from "react"
import Overview from "./Overview"

export default {
  title: "Complex|Overview",
  component: Overview,
  parameters: {
    componentSubtitle: "Shows complete view of business health."
  }
}

export const Basic: FC = () => <Overview />
