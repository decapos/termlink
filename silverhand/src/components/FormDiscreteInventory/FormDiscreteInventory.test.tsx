import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./FormDiscreteInventory.stories"

describe("FormDiscreteInventory", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
