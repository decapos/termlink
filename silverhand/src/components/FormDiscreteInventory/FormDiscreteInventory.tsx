import React, { FC, useState } from "react"
import { FormattedMessage } from "gatsby-plugin-intl3"
import { useFormatMessage } from "../hooks"
import Select from "../Select/Select"
import Typography from "../Typography/Typography"
import Input from "../Input/Input"
import { MeasurementUnit } from "../../apollo/graphqlTypes"
import Box from "../Box/Box"
import Tab from "../Tab/Tab"
import LoadingPlaceholder from "../LoadingPlaceholder/LoadingPlaceholder"

interface Props {
  inventory: DiscreteInventoryData
  onInventoryChange: (amount: DiscreteInventoryData) => void
}
export interface DiscreteInventoryData {
  unit: MeasurementUnit
  stores: {
    id: string
    name: string
    items: ItemStock[]
  }[]
}
interface ItemStock {
  id: string
  name: string
  amount: number
}

export const FormDiscreteInventory: FC<Props> = ({
  inventory,
  onInventoryChange
}) => {
  const [selectedStoreIdx, setSelectedStoreIdx] = useState(0)
  const currentAmountLabel = useFormatMessage("currentAmount")
  const selectedStore = inventory.stores[selectedStoreIdx]

  function handleUnitChange(unit: MeasurementUnit): void {
    onInventoryChange({ ...inventory, unit })
  }
  function handleAmountChange(updatedItem: ItemStock): void {
    const idx = selectedStore.items.findIndex(({ id }) => id === updatedItem.id)
    const updatedInventory = { ...inventory }
    updatedInventory.stores[selectedStoreIdx].items[idx] = updatedItem
    onInventoryChange(updatedInventory)
  }

  const unitSelector = (
    <Select
      label={useFormatMessage("unit")}
      p={3}
      onChange={e => handleUnitChange({ id: e.target.value })}
      value={inventory.unit.id}
    >
      <option value={0}>{useFormatMessage("piece")}</option>
      <option value={1}>{useFormatMessage("ml")}</option>
      <option value={2}>{useFormatMessage("l")}</option>
      <option value={3}>{useFormatMessage("g")}</option>
      <option value={4}>{useFormatMessage("kg")}</option>
    </Select>
  )

  if (inventory.stores.length === 0) {
    return <LoadingPlaceholder width="100%" height="appbar" />
  }

  return (
    <Box
      backgroundColor="background"
      sx={{
        borderRadius: "default",
        borderWidth: 1,
        borderColor: "border",
        borderStyle: "solid"
      }}
    >
      {unitSelector}
      <Tab
        onTabClick={setSelectedStoreIdx}
        selectedItemIdx={selectedStoreIdx}
        items={inventory.stores.map(({ name }) => name)}
        mb={3}
        small
      />
      {selectedStore.items.length > 1 && (
        <Typography.H5 mx={3}>
          <FormattedMessage id="currentAmountsByVariants" />
        </Typography.H5>
      )}
      {selectedStore.items.map(item => (
        <Input
          key={item.id}
          p={3}
          pt={0}
          label={item.name === item.id ? currentAmountLabel : item.name}
          width="100%"
          value={item.amount}
          small
          onChange={e =>
            handleAmountChange({
              ...item,
              amount: parseInt(e.target.value, 10)
            })
          }
        />
      ))}
    </Box>
  )
}

export default FormDiscreteInventory
