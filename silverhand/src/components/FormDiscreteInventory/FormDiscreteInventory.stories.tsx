import React, { FC, useState } from "react"
import nanoid from "nanoid"
import FormDiscreteInventory, {
  DiscreteInventoryData
} from "./FormDiscreteInventory"

import Box from "../Box/Box"

export default {
  title: "Complex|Forms/FormDiscreteInventory",
  component: FormDiscreteInventory,
  parameters: {
    componentSubtitle: "Just a simple FormDiscreteInventory"
  }
}

export const Basic: FC = () => {
  const items = [
    {
      id: nanoid(),
      name: "T-Shirt White",
      amount: 12
    },
    {
      id: nanoid(),
      name: "T-Shirt Black",
      amount: 12
    },
    {
      id: nanoid(),
      name: "T-Shirt Blue",
      amount: 12
    }
  ]
  const [inventory, setInventory] = useState<DiscreteInventoryData>({
    unit: { id: "random", notation: "", name: "" },
    stores: [
      {
        id: nanoid(),
        name: "Mall Alam Sutra",
        items: [...items]
      },
      {
        id: nanoid(),
        name: "Living World",
        items: [...items]
      },
      {
        id: nanoid(),
        name: "Hasura",
        items: [...items]
      },
      {
        id: nanoid(),
        name: "Mall of Indonesia",
        items: [...items]
      }
    ]
  })

  return (
    <Box p={3}>
      <FormDiscreteInventory
        inventory={inventory}
        onInventoryChange={setInventory}
      />
    </Box>
  )
}

export const NoVariant: FC = () => {
  const items = [
    {
      id: nanoid(),
      name: "T-Shirt White",
      amount: 12
    }
  ]
  const [inventory, setInventory] = useState<DiscreteInventoryData>({
    unit: { id: "random", notation: "", name: "" },
    stores: [
      {
        id: nanoid(),
        name: "Mall Alam Sutra",
        items: [{ ...items[0] }]
      },
      {
        id: nanoid(),
        name: "Mall Alam Sutra",
        items: [{ ...items[0] }]
      },
      {
        id: nanoid(),
        name: "Mall Alam Sutra",
        items: [{ ...items[0] }]
      }
    ]
  })

  return (
    <Box p={3}>
      <FormDiscreteInventory
        inventory={inventory}
        onInventoryChange={setInventory}
      />
    </Box>
  )
}
