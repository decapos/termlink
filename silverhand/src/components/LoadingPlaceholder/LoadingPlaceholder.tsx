import React, { FC } from "react"
import { keyframes } from "@emotion/core"
import { Box, BoxProps } from "../Box/Box"

const fading = keyframes`
  0% {
    background-color: 
    rgba(165, 165, 165, 0.1);
  }
  50% {
    background-color: 
    rgba(165, 165, 165, 0.3);
  }
  100% {
    background-color: 
    rgba(165, 165, 165, 0.1);
  }
`
interface Props extends BoxProps {
  variant?: "loadingPlaceholder.text"
  height?: number | string
  width: number | string
}
export const LoadingPlaceholder: FC<Props> = props => (
  <Box
    sx={{
      borderRadius: "default",
      animation: `1s ease-in-out 0s infinite ${fading}`
    }}
    {...props}
  />
)

export default LoadingPlaceholder
