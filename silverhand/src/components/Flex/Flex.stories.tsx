import React, { FC } from "react"
import Flex from "./Flex"

export default {
  title: "Core|Flex",
  component: Flex,
  parameters: {
    componentSubtitle: "Just a simple Flex"
  }
}

export const Basic: FC = () => <Flex />
