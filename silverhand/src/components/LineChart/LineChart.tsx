import React, { FC } from "react"
import Charts from "../Charts/Charts"

interface Props {
  title?: string
  data: ChartData
  colors?: string[]
}
export const LineChart: FC<Props> = ({ data, colors }) => (
  <Charts
    data={data}
    type="line"
    height={200}
    lineOptions={{
      regionFill: true
    }}
    barOptions={{ spaceRatio: 0.2 }}
    axisOptions={{ xAxisMode: "span" }}
    colors={colors}
  />
)

export default LineChart
