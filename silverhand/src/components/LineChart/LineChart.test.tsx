import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./LineChart.stories"

describe("LineChart", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
