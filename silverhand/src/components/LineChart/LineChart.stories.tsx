import React, { FC } from "react"
import LineChart from "./LineChart"
import { generateMockChartData } from "../../__mocks__/data"

export default {
  title: "Basic|Charts/LineChart",
  component: LineChart,
  parameters: {
    componentSubtitle: "Just a simple LineChart"
  }
}

export const Basic: FC = () => <LineChart data={generateMockChartData()} />
