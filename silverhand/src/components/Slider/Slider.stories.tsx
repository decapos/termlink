import React, { FC } from "react"
import Slider from "./Slider"

export default {
  title: "Core|Forms/Slider",
  component: Slider,
  parameters: {
    componentSubtitle: "Just a simple Slider"
  }
}

export const Basic: FC = () => <Slider />
