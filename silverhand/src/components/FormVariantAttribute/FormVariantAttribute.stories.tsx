import React, { FC, useState } from "react"
import { action } from "@storybook/addon-actions"
import nanoid from "nanoid"
import FormVariantAttribute, { VariantAttribute } from "./FormVariantAttribute"
import Box from "../Box/Box"

export default {
  title: "Complex|Forms/FormVariantAttribute",
  component: FormVariantAttribute,
  parameters: {
    componentSubtitle: "Just a simple FormVariantAttribute"
  }
}

export const Basic: FC = () => {
  const [attribute, setAttribute] = useState<VariantAttribute>({
    id: nanoid(),
    options: [],
    name: ""
  })

  return (
    <Box p={3}>
      <FormVariantAttribute
        onChange={setAttribute}
        onDelete={action("deleteAttribute")}
        value={attribute}
      />
    </Box>
  )
}
