import React, { FC, useState } from "react"
import nanoid from "nanoid"
import { FormattedMessage } from "gatsby-plugin-intl3"
import { TextProps } from "rebass"
import Flex from "../Flex/Flex"
import Input from "../Input/Input"
import { useFormatMessage } from "../hooks"
import Icon from "../Icon/Icon"
import { ReactComponent as TrashIcon } from "../../icons/trash.svg"
import Box from "../Box/Box"
import { ReactComponent as PlusIcon } from "../../icons/plus.svg"
import { ReactComponent as CloseIcon } from "../../icons/close.svg"
import Typography from "../Typography/Typography"
import Button, { ButtonProps } from "../Button/Button"

interface Props {
  onDelete: () => void
  onChange: (variant: VariantAttribute) => void
  value: VariantAttribute
}
export interface VariantAttribute {
  id: string
  name: string
  options: {
    id: string
    value: string
  }[]
}
export const FormVariantAttribute: FC<Props> = ({
  onDelete,
  value,
  onChange
}) => {
  const [newOption, setNewOption] = useState("")

  function addOption(): void {
    const newOptionObject = { id: nanoid(), value: newOption }
    const updatedOptions = [...value.options, newOptionObject]
    onChange({ ...value, options: updatedOptions })
    setNewOption("")
  }
  function deleteOption(optionId: string): void {
    const updatedOptions = value.options.filter(({ id }) => id !== optionId)
    onChange({ ...value, options: updatedOptions })
  }

  const variantTypeField = (
    <>
      <SmallLabel>
        <FormattedMessage id="typeOfVariant" />
      </SmallLabel>
      <Flex mb={3} mx={3}>
        <Input
          aria-label={useFormatMessage("typeOfVariant")}
          placeholder={useFormatMessage("typeOfVariantExample")}
          width="100%"
          value={value.name}
          onChange={e => onChange({ ...value, name: e.target.value })}
          data-cy="typeOfVariant"
          small
        />
        <SmallIconButton
          onClick={() => onDelete()}
          aria-label={useFormatMessage("removeVariant")}
        >
          <Icon as={TrashIcon} m={0} fill="danger" />
        </SmallIconButton>
      </Flex>
    </>
  )

  const newOptionField = (
    <Box
      sx={{
        borderTopWidth: 1,
        borderTopColor: "border",
        borderTopStyle: "solid"
      }}
    >
      <SmallLabel>
        <FormattedMessage id="variants" />
      </SmallLabel>
      <Flex p={3} pt={0}>
        <Box flex={1}>
          <Input
            aria-label={useFormatMessage("variants")}
            placeholder={useFormatMessage("variantExample")}
            width="100%"
            value={newOption}
            onEnterPressed={addOption}
            onChange={e => setNewOption(e.target.value)}
            data-cy="variantOptionField"
            small
          />
        </Box>
        <SmallIconButton
          disabled={newOption === ""}
          onClick={addOption}
          aria-label={useFormatMessage("addVariantOption")}
          data-cy="addOption"
        >
          <Icon as={PlusIcon} m={0} fill="textPrimary" />
        </SmallIconButton>
      </Flex>
    </Box>
  )

  const optionList = value.options.map(option => (
    <Flex alignItems="center" key={option.id}>
      <Icon
        as={CloseIcon}
        m={0}
        mb={2}
        ml={3}
        onClick={() => deleteOption(option.id)}
        sx={{
          cursor: "pointer"
        }}
      />
      <Typography.Body fontSize={1} mb={2} ml={2}>
        {option.value}
      </Typography.Body>
    </Flex>
  ))

  return (
    <Box
      backgroundColor="background"
      mt={3}
      sx={{
        borderRadius: "default",
        borderWidth: 1,
        borderColor: "border",
        borderStyle: "solid"
      }}
    >
      {variantTypeField}
      {newOptionField}
      {optionList}
    </Box>
  )
}

const SmallIconButton: FC<ButtonProps> = props => (
  <Button type="button" variant="outline" ml={2} py={0} px={2} {...props} />
)

const SmallLabel: FC<TextProps> = props => (
  <Typography.Body
    pt={2}
    px={3}
    pb={1}
    mb={0}
    fontSize={0}
    color="textMediumEmphasis"
    {...props}
  />
)

export default FormVariantAttribute
