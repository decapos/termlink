import React from "react"
import { render } from "../../test-utils"
import { Basic } from "./FormVariantAttribute.stories"

describe("FormVariantAttribute", () => {
  it("should render correctly", () => {
    const { container } = render(<Basic />)
    expect(container).toMatchSnapshot()
  })
})
