// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isString(input: any, msg: string): asserts input is string {
  if (typeof input !== "string") {
    throw Error(msg)
  }
}
