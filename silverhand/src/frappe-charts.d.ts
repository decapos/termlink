interface Dataset {
  name: string
  type?: string
  values: number[]
}

interface ChartData {
  labels: string[]
  datasets: Dataset[]
  yMarkers?: Array<{
    label: string
    value: number
  }>
  yRegions?: Array<{
    label: string
    start: number
    end: number
  }>
}

type ChartTypes =
  | "axis-mixed"
  | "line"
  | "bar"
  | "percentage"
  | "heatmap"
  | "pie"
  | "donut"

interface AxisOptions {
  // Display axis points as short ticks or long spanning lines, default "span"
  xAxisMode?: "span" | "tick"
  yAxisMode?: "span" | "tick"

  // The X axis (often the time axis) is usually continuous. That means we can
  // reduce the redundancy of rendering every X label by setting xIsSeries to 1
  // and allowing only a few periodic ones.
  xIsSeries?: number
}

interface BarOptions {
  // min 0 max 1, default 0.5
  spaceRatio?: number

  // default: false
  stacked?: boolean

  // default: 5 (bar percentage chart shadow depth)
  depth?: number

  // default: 20 (bar percentage chart height)
  height?: number
}

interface LineOptions {
  hideDots?: boolean
  hideLine?: boolean
  heatLine?: boolean

  spline?: boolean
  regionFill?: boolean
}

interface ChartOptions {
  // Add title to the chart
  title?: string

  // Chart data,
  data: ChartData

  // Type of chart, Defaults to  'line"
  type?: ChartTypes

  // Height of the chart, defaults to 240
  height?: number

  // Color for the chart
  // Defaults: ['light-blue', 'blue', 'violet', 'red', 'orange', 'yellow', 'green',
  // 'light-green', 'purple', 'magenta', 'light-grey', 'dark-grey']
  colors?: string[]

  axisOptions?: AxisOptions

  tooltipOption?: { number: () => string }

  // Can be used to set properties on line plots.
  barOptions?: BarOptions

  // Makes the chart interactive with arrow keys, default to false
  isNavigable?: boolean

  // Display data values over bar or dots, defaults to false
  valuesOverPoints?: boolean

  lineOptions?: LineOptions
}

declare module "frappe-charts/dist/frappe-charts.min.esm" {
  import { MutableRefObject } from "react"

  // eslint-disable-next-line import/prefer-default-export
  export class Chart {
    constructor(
      container: MutableRefObject<HTMLDivElement>,
      options: ChartOptions
    )

    update(data: ChartData): void

    // Update individual data points
    addDataPoint(
      label: string,
      valueFromEachDataset: number[],
      index?: number // defaults to end
    ): void

    // remove individual data points
    // defaults to end
    removeDataPoint(index?: number): void
  }
}
