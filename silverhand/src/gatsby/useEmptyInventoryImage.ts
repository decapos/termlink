import { FluidObject } from "gatsby-image"
import { graphql, useStaticQuery } from "gatsby"

export const useEmptyInventoryImage = (): FluidObject | undefined => {
  const data = useStaticQuery(graphql`
    query EmptyInventoryImage {
      file: file(relativePath: { eq: "empty-inventory.png" }) {
        childImageSharp {
          fluid(maxWidth: 1400) {
            ...GatsbyImageSharpFluid_withWebp_tracedSVG
          }
        }
      }
    }
  `)

  if (data && data.file && data.file.childImageSharp) {
    return data.file.childImageSharp.fluid as FluidObject
  }
  return undefined
}
