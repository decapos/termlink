export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  /** A date string, such as 2007-12-03, compliant with the ISO 8601 standard for representation of dates and times using the Gregorian calendar. */
  Date: any,
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any,
};

export type BooleanQueryOperatorInput = {
  eq?: Maybe<Scalars['Boolean']>,
  in?: Maybe<Array<Maybe<Scalars['Boolean']>>>,
  ne?: Maybe<Scalars['Boolean']>,
  nin?: Maybe<Array<Maybe<Scalars['Boolean']>>>,
};


export type DateQueryOperatorInput = {
  eq?: Maybe<Scalars['Date']>,
  gt?: Maybe<Scalars['Date']>,
  gte?: Maybe<Scalars['Date']>,
  in?: Maybe<Array<Maybe<Scalars['Date']>>>,
  lt?: Maybe<Scalars['Date']>,
  lte?: Maybe<Scalars['Date']>,
  ne?: Maybe<Scalars['Date']>,
  nin?: Maybe<Array<Maybe<Scalars['Date']>>>,
};

export type Directory = Node & {
   __typename?: 'Directory',
  absolutePath?: Maybe<Scalars['String']>,
  accessTime?: Maybe<Scalars['Date']>,
  atime?: Maybe<Scalars['Date']>,
  atimeMs?: Maybe<Scalars['Float']>,
  base?: Maybe<Scalars['String']>,
  birthTime?: Maybe<Scalars['Date']>,
  birthtime?: Maybe<Scalars['Date']>,
  birthtimeMs?: Maybe<Scalars['Float']>,
  blksize?: Maybe<Scalars['Int']>,
  blocks?: Maybe<Scalars['Int']>,
  changeTime?: Maybe<Scalars['Date']>,
  children: Array<Node>,
  ctime?: Maybe<Scalars['Date']>,
  ctimeMs?: Maybe<Scalars['Float']>,
  dev?: Maybe<Scalars['Int']>,
  dir?: Maybe<Scalars['String']>,
  ext?: Maybe<Scalars['String']>,
  extension?: Maybe<Scalars['String']>,
  gid?: Maybe<Scalars['Int']>,
  id: Scalars['ID'],
  ino?: Maybe<Scalars['Int']>,
  internal: Internal,
  mode?: Maybe<Scalars['Int']>,
  modifiedTime?: Maybe<Scalars['Date']>,
  mtime?: Maybe<Scalars['Date']>,
  mtimeMs?: Maybe<Scalars['Float']>,
  name?: Maybe<Scalars['String']>,
  nlink?: Maybe<Scalars['Int']>,
  parent?: Maybe<Node>,
  prettySize?: Maybe<Scalars['String']>,
  rdev?: Maybe<Scalars['Int']>,
  relativeDirectory?: Maybe<Scalars['String']>,
  relativePath?: Maybe<Scalars['String']>,
  root?: Maybe<Scalars['String']>,
  size?: Maybe<Scalars['Int']>,
  sourceInstanceName?: Maybe<Scalars['String']>,
  uid?: Maybe<Scalars['Int']>,
};


export type DirectoryAccessTimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryAtimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryBirthTimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryBirthtimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryChangeTimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryCtimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryModifiedTimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type DirectoryMtimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};

export type DirectoryConnection = {
   __typename?: 'DirectoryConnection',
  distinct: Array<Scalars['String']>,
  edges: Array<DirectoryEdge>,
  group: Array<DirectoryGroupConnection>,
  nodes: Array<Directory>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};


export type DirectoryConnectionDistinctArgs = {
  field: DirectoryFieldsEnum
};


export type DirectoryConnectionGroupArgs = {
  field: DirectoryFieldsEnum,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>
};

export type DirectoryEdge = {
   __typename?: 'DirectoryEdge',
  next?: Maybe<Directory>,
  node: Directory,
  previous?: Maybe<Directory>,
};

export enum DirectoryFieldsEnum {
  absolutePath = 'absolutePath',
  accessTime = 'accessTime',
  atime = 'atime',
  atimeMs = 'atimeMs',
  base = 'base',
  birthTime = 'birthTime',
  birthtime = 'birthtime',
  birthtimeMs = 'birthtimeMs',
  blksize = 'blksize',
  blocks = 'blocks',
  changeTime = 'changeTime',
  children = 'children',
  children___children = 'children___children',
  children___children___children = 'children___children___children',
  children___children___children___children = 'children___children___children___children',
  children___children___children___id = 'children___children___children___id',
  children___children___id = 'children___children___id',
  children___children___internal___content = 'children___children___internal___content',
  children___children___internal___contentDigest = 'children___children___internal___contentDigest',
  children___children___internal___description = 'children___children___internal___description',
  children___children___internal___fieldOwners = 'children___children___internal___fieldOwners',
  children___children___internal___ignoreType = 'children___children___internal___ignoreType',
  children___children___internal___mediaType = 'children___children___internal___mediaType',
  children___children___internal___owner = 'children___children___internal___owner',
  children___children___internal___type = 'children___children___internal___type',
  children___children___parent___children = 'children___children___parent___children',
  children___children___parent___id = 'children___children___parent___id',
  children___id = 'children___id',
  children___internal___content = 'children___internal___content',
  children___internal___contentDigest = 'children___internal___contentDigest',
  children___internal___description = 'children___internal___description',
  children___internal___fieldOwners = 'children___internal___fieldOwners',
  children___internal___ignoreType = 'children___internal___ignoreType',
  children___internal___mediaType = 'children___internal___mediaType',
  children___internal___owner = 'children___internal___owner',
  children___internal___type = 'children___internal___type',
  children___parent___children = 'children___parent___children',
  children___parent___children___children = 'children___parent___children___children',
  children___parent___children___id = 'children___parent___children___id',
  children___parent___id = 'children___parent___id',
  children___parent___internal___content = 'children___parent___internal___content',
  children___parent___internal___contentDigest = 'children___parent___internal___contentDigest',
  children___parent___internal___description = 'children___parent___internal___description',
  children___parent___internal___fieldOwners = 'children___parent___internal___fieldOwners',
  children___parent___internal___ignoreType = 'children___parent___internal___ignoreType',
  children___parent___internal___mediaType = 'children___parent___internal___mediaType',
  children___parent___internal___owner = 'children___parent___internal___owner',
  children___parent___internal___type = 'children___parent___internal___type',
  children___parent___parent___children = 'children___parent___parent___children',
  children___parent___parent___id = 'children___parent___parent___id',
  ctime = 'ctime',
  ctimeMs = 'ctimeMs',
  dev = 'dev',
  dir = 'dir',
  ext = 'ext',
  extension = 'extension',
  gid = 'gid',
  id = 'id',
  ino = 'ino',
  internal___content = 'internal___content',
  internal___contentDigest = 'internal___contentDigest',
  internal___description = 'internal___description',
  internal___fieldOwners = 'internal___fieldOwners',
  internal___ignoreType = 'internal___ignoreType',
  internal___mediaType = 'internal___mediaType',
  internal___owner = 'internal___owner',
  internal___type = 'internal___type',
  mode = 'mode',
  modifiedTime = 'modifiedTime',
  mtime = 'mtime',
  mtimeMs = 'mtimeMs',
  name = 'name',
  nlink = 'nlink',
  parent___children = 'parent___children',
  parent___children___children = 'parent___children___children',
  parent___children___children___children = 'parent___children___children___children',
  parent___children___children___id = 'parent___children___children___id',
  parent___children___id = 'parent___children___id',
  parent___children___internal___content = 'parent___children___internal___content',
  parent___children___internal___contentDigest = 'parent___children___internal___contentDigest',
  parent___children___internal___description = 'parent___children___internal___description',
  parent___children___internal___fieldOwners = 'parent___children___internal___fieldOwners',
  parent___children___internal___ignoreType = 'parent___children___internal___ignoreType',
  parent___children___internal___mediaType = 'parent___children___internal___mediaType',
  parent___children___internal___owner = 'parent___children___internal___owner',
  parent___children___internal___type = 'parent___children___internal___type',
  parent___children___parent___children = 'parent___children___parent___children',
  parent___children___parent___id = 'parent___children___parent___id',
  parent___id = 'parent___id',
  parent___internal___content = 'parent___internal___content',
  parent___internal___contentDigest = 'parent___internal___contentDigest',
  parent___internal___description = 'parent___internal___description',
  parent___internal___fieldOwners = 'parent___internal___fieldOwners',
  parent___internal___ignoreType = 'parent___internal___ignoreType',
  parent___internal___mediaType = 'parent___internal___mediaType',
  parent___internal___owner = 'parent___internal___owner',
  parent___internal___type = 'parent___internal___type',
  parent___parent___children = 'parent___parent___children',
  parent___parent___children___children = 'parent___parent___children___children',
  parent___parent___children___id = 'parent___parent___children___id',
  parent___parent___id = 'parent___parent___id',
  parent___parent___internal___content = 'parent___parent___internal___content',
  parent___parent___internal___contentDigest = 'parent___parent___internal___contentDigest',
  parent___parent___internal___description = 'parent___parent___internal___description',
  parent___parent___internal___fieldOwners = 'parent___parent___internal___fieldOwners',
  parent___parent___internal___ignoreType = 'parent___parent___internal___ignoreType',
  parent___parent___internal___mediaType = 'parent___parent___internal___mediaType',
  parent___parent___internal___owner = 'parent___parent___internal___owner',
  parent___parent___internal___type = 'parent___parent___internal___type',
  parent___parent___parent___children = 'parent___parent___parent___children',
  parent___parent___parent___id = 'parent___parent___parent___id',
  prettySize = 'prettySize',
  rdev = 'rdev',
  relativeDirectory = 'relativeDirectory',
  relativePath = 'relativePath',
  root = 'root',
  size = 'size',
  sourceInstanceName = 'sourceInstanceName',
  uid = 'uid'
}

export type DirectoryFilterInput = {
  absolutePath?: Maybe<StringQueryOperatorInput>,
  accessTime?: Maybe<DateQueryOperatorInput>,
  atime?: Maybe<DateQueryOperatorInput>,
  atimeMs?: Maybe<FloatQueryOperatorInput>,
  base?: Maybe<StringQueryOperatorInput>,
  birthTime?: Maybe<DateQueryOperatorInput>,
  birthtime?: Maybe<DateQueryOperatorInput>,
  birthtimeMs?: Maybe<FloatQueryOperatorInput>,
  blksize?: Maybe<IntQueryOperatorInput>,
  blocks?: Maybe<IntQueryOperatorInput>,
  changeTime?: Maybe<DateQueryOperatorInput>,
  children?: Maybe<NodeFilterListInput>,
  ctime?: Maybe<DateQueryOperatorInput>,
  ctimeMs?: Maybe<FloatQueryOperatorInput>,
  dev?: Maybe<IntQueryOperatorInput>,
  dir?: Maybe<StringQueryOperatorInput>,
  ext?: Maybe<StringQueryOperatorInput>,
  extension?: Maybe<StringQueryOperatorInput>,
  gid?: Maybe<IntQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  ino?: Maybe<IntQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  mode?: Maybe<IntQueryOperatorInput>,
  modifiedTime?: Maybe<DateQueryOperatorInput>,
  mtime?: Maybe<DateQueryOperatorInput>,
  mtimeMs?: Maybe<FloatQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  nlink?: Maybe<IntQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  prettySize?: Maybe<StringQueryOperatorInput>,
  rdev?: Maybe<IntQueryOperatorInput>,
  relativeDirectory?: Maybe<StringQueryOperatorInput>,
  relativePath?: Maybe<StringQueryOperatorInput>,
  root?: Maybe<StringQueryOperatorInput>,
  size?: Maybe<IntQueryOperatorInput>,
  sourceInstanceName?: Maybe<StringQueryOperatorInput>,
  uid?: Maybe<IntQueryOperatorInput>,
};

export type DirectoryGroupConnection = {
   __typename?: 'DirectoryGroupConnection',
  edges: Array<DirectoryEdge>,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
  nodes: Array<Directory>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};

export type DirectorySortInput = {
  fields?: Maybe<Array<Maybe<DirectoryFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type DuotoneGradient = {
  highlight: Scalars['String'],
  opacity?: Maybe<Scalars['Int']>,
  shadow: Scalars['String'],
};

export type File = Node & {
   __typename?: 'File',
  absolutePath?: Maybe<Scalars['String']>,
  accessTime?: Maybe<Scalars['Date']>,
  atime?: Maybe<Scalars['Date']>,
  atimeMs?: Maybe<Scalars['Float']>,
  base?: Maybe<Scalars['String']>,
  birthTime?: Maybe<Scalars['Date']>,
  birthtime?: Maybe<Scalars['Date']>,
  birthtimeMs?: Maybe<Scalars['Float']>,
  blksize?: Maybe<Scalars['Int']>,
  blocks?: Maybe<Scalars['Int']>,
  changeTime?: Maybe<Scalars['Date']>,
  childImageSharp?: Maybe<ImageSharp>,
  children: Array<Node>,
  ctime?: Maybe<Scalars['Date']>,
  ctimeMs?: Maybe<Scalars['Float']>,
  dev?: Maybe<Scalars['Int']>,
  dir?: Maybe<Scalars['String']>,
  ext?: Maybe<Scalars['String']>,
  extension?: Maybe<Scalars['String']>,
  gid?: Maybe<Scalars['Int']>,
  id: Scalars['ID'],
  ino?: Maybe<Scalars['Int']>,
  internal: Internal,
  mode?: Maybe<Scalars['Int']>,
  modifiedTime?: Maybe<Scalars['Date']>,
  mtime?: Maybe<Scalars['Date']>,
  mtimeMs?: Maybe<Scalars['Float']>,
  name?: Maybe<Scalars['String']>,
  nlink?: Maybe<Scalars['Int']>,
  parent?: Maybe<Node>,
  prettySize?: Maybe<Scalars['String']>,
  /** Copy file to static directory and return public url to it */
  publicURL?: Maybe<Scalars['String']>,
  rdev?: Maybe<Scalars['Int']>,
  relativeDirectory?: Maybe<Scalars['String']>,
  relativePath?: Maybe<Scalars['String']>,
  root?: Maybe<Scalars['String']>,
  size?: Maybe<Scalars['Int']>,
  sourceInstanceName?: Maybe<Scalars['String']>,
  uid?: Maybe<Scalars['Int']>,
};


export type FileAccessTimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type FileAtimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type FileBirthTimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type FileChangeTimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type FileCtimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type FileModifiedTimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};


export type FileMtimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};

export type FileConnection = {
   __typename?: 'FileConnection',
  distinct: Array<Scalars['String']>,
  edges: Array<FileEdge>,
  group: Array<FileGroupConnection>,
  nodes: Array<File>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};


export type FileConnectionDistinctArgs = {
  field: FileFieldsEnum
};


export type FileConnectionGroupArgs = {
  field: FileFieldsEnum,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>
};

export type FileEdge = {
   __typename?: 'FileEdge',
  next?: Maybe<File>,
  node: File,
  previous?: Maybe<File>,
};

export enum FileFieldsEnum {
  absolutePath = 'absolutePath',
  accessTime = 'accessTime',
  atime = 'atime',
  atimeMs = 'atimeMs',
  base = 'base',
  birthTime = 'birthTime',
  birthtime = 'birthtime',
  birthtimeMs = 'birthtimeMs',
  blksize = 'blksize',
  blocks = 'blocks',
  changeTime = 'changeTime',
  childImageSharp___children = 'childImageSharp___children',
  childImageSharp___children___children = 'childImageSharp___children___children',
  childImageSharp___children___children___children = 'childImageSharp___children___children___children',
  childImageSharp___children___children___id = 'childImageSharp___children___children___id',
  childImageSharp___children___id = 'childImageSharp___children___id',
  childImageSharp___children___internal___content = 'childImageSharp___children___internal___content',
  childImageSharp___children___internal___contentDigest = 'childImageSharp___children___internal___contentDigest',
  childImageSharp___children___internal___description = 'childImageSharp___children___internal___description',
  childImageSharp___children___internal___fieldOwners = 'childImageSharp___children___internal___fieldOwners',
  childImageSharp___children___internal___ignoreType = 'childImageSharp___children___internal___ignoreType',
  childImageSharp___children___internal___mediaType = 'childImageSharp___children___internal___mediaType',
  childImageSharp___children___internal___owner = 'childImageSharp___children___internal___owner',
  childImageSharp___children___internal___type = 'childImageSharp___children___internal___type',
  childImageSharp___children___parent___children = 'childImageSharp___children___parent___children',
  childImageSharp___children___parent___id = 'childImageSharp___children___parent___id',
  childImageSharp___fixed___aspectRatio = 'childImageSharp___fixed___aspectRatio',
  childImageSharp___fixed___base64 = 'childImageSharp___fixed___base64',
  childImageSharp___fixed___height = 'childImageSharp___fixed___height',
  childImageSharp___fixed___originalName = 'childImageSharp___fixed___originalName',
  childImageSharp___fixed___src = 'childImageSharp___fixed___src',
  childImageSharp___fixed___srcSet = 'childImageSharp___fixed___srcSet',
  childImageSharp___fixed___srcSetWebp = 'childImageSharp___fixed___srcSetWebp',
  childImageSharp___fixed___srcWebp = 'childImageSharp___fixed___srcWebp',
  childImageSharp___fixed___tracedSVG = 'childImageSharp___fixed___tracedSVG',
  childImageSharp___fixed___width = 'childImageSharp___fixed___width',
  childImageSharp___fluid___aspectRatio = 'childImageSharp___fluid___aspectRatio',
  childImageSharp___fluid___base64 = 'childImageSharp___fluid___base64',
  childImageSharp___fluid___originalImg = 'childImageSharp___fluid___originalImg',
  childImageSharp___fluid___originalName = 'childImageSharp___fluid___originalName',
  childImageSharp___fluid___presentationHeight = 'childImageSharp___fluid___presentationHeight',
  childImageSharp___fluid___presentationWidth = 'childImageSharp___fluid___presentationWidth',
  childImageSharp___fluid___sizes = 'childImageSharp___fluid___sizes',
  childImageSharp___fluid___src = 'childImageSharp___fluid___src',
  childImageSharp___fluid___srcSet = 'childImageSharp___fluid___srcSet',
  childImageSharp___fluid___srcSetWebp = 'childImageSharp___fluid___srcSetWebp',
  childImageSharp___fluid___srcWebp = 'childImageSharp___fluid___srcWebp',
  childImageSharp___fluid___tracedSVG = 'childImageSharp___fluid___tracedSVG',
  childImageSharp___id = 'childImageSharp___id',
  childImageSharp___internal___content = 'childImageSharp___internal___content',
  childImageSharp___internal___contentDigest = 'childImageSharp___internal___contentDigest',
  childImageSharp___internal___description = 'childImageSharp___internal___description',
  childImageSharp___internal___fieldOwners = 'childImageSharp___internal___fieldOwners',
  childImageSharp___internal___ignoreType = 'childImageSharp___internal___ignoreType',
  childImageSharp___internal___mediaType = 'childImageSharp___internal___mediaType',
  childImageSharp___internal___owner = 'childImageSharp___internal___owner',
  childImageSharp___internal___type = 'childImageSharp___internal___type',
  childImageSharp___original___height = 'childImageSharp___original___height',
  childImageSharp___original___src = 'childImageSharp___original___src',
  childImageSharp___original___width = 'childImageSharp___original___width',
  childImageSharp___parent___children = 'childImageSharp___parent___children',
  childImageSharp___parent___children___children = 'childImageSharp___parent___children___children',
  childImageSharp___parent___children___id = 'childImageSharp___parent___children___id',
  childImageSharp___parent___id = 'childImageSharp___parent___id',
  childImageSharp___parent___internal___content = 'childImageSharp___parent___internal___content',
  childImageSharp___parent___internal___contentDigest = 'childImageSharp___parent___internal___contentDigest',
  childImageSharp___parent___internal___description = 'childImageSharp___parent___internal___description',
  childImageSharp___parent___internal___fieldOwners = 'childImageSharp___parent___internal___fieldOwners',
  childImageSharp___parent___internal___ignoreType = 'childImageSharp___parent___internal___ignoreType',
  childImageSharp___parent___internal___mediaType = 'childImageSharp___parent___internal___mediaType',
  childImageSharp___parent___internal___owner = 'childImageSharp___parent___internal___owner',
  childImageSharp___parent___internal___type = 'childImageSharp___parent___internal___type',
  childImageSharp___parent___parent___children = 'childImageSharp___parent___parent___children',
  childImageSharp___parent___parent___id = 'childImageSharp___parent___parent___id',
  childImageSharp___resize___aspectRatio = 'childImageSharp___resize___aspectRatio',
  childImageSharp___resize___height = 'childImageSharp___resize___height',
  childImageSharp___resize___originalName = 'childImageSharp___resize___originalName',
  childImageSharp___resize___src = 'childImageSharp___resize___src',
  childImageSharp___resize___tracedSVG = 'childImageSharp___resize___tracedSVG',
  childImageSharp___resize___width = 'childImageSharp___resize___width',
  childImageSharp___resolutions___aspectRatio = 'childImageSharp___resolutions___aspectRatio',
  childImageSharp___resolutions___base64 = 'childImageSharp___resolutions___base64',
  childImageSharp___resolutions___height = 'childImageSharp___resolutions___height',
  childImageSharp___resolutions___originalName = 'childImageSharp___resolutions___originalName',
  childImageSharp___resolutions___src = 'childImageSharp___resolutions___src',
  childImageSharp___resolutions___srcSet = 'childImageSharp___resolutions___srcSet',
  childImageSharp___resolutions___srcSetWebp = 'childImageSharp___resolutions___srcSetWebp',
  childImageSharp___resolutions___srcWebp = 'childImageSharp___resolutions___srcWebp',
  childImageSharp___resolutions___tracedSVG = 'childImageSharp___resolutions___tracedSVG',
  childImageSharp___resolutions___width = 'childImageSharp___resolutions___width',
  childImageSharp___sizes___aspectRatio = 'childImageSharp___sizes___aspectRatio',
  childImageSharp___sizes___base64 = 'childImageSharp___sizes___base64',
  childImageSharp___sizes___originalImg = 'childImageSharp___sizes___originalImg',
  childImageSharp___sizes___originalName = 'childImageSharp___sizes___originalName',
  childImageSharp___sizes___presentationHeight = 'childImageSharp___sizes___presentationHeight',
  childImageSharp___sizes___presentationWidth = 'childImageSharp___sizes___presentationWidth',
  childImageSharp___sizes___sizes = 'childImageSharp___sizes___sizes',
  childImageSharp___sizes___src = 'childImageSharp___sizes___src',
  childImageSharp___sizes___srcSet = 'childImageSharp___sizes___srcSet',
  childImageSharp___sizes___srcSetWebp = 'childImageSharp___sizes___srcSetWebp',
  childImageSharp___sizes___srcWebp = 'childImageSharp___sizes___srcWebp',
  childImageSharp___sizes___tracedSVG = 'childImageSharp___sizes___tracedSVG',
  children = 'children',
  children___children = 'children___children',
  children___children___children = 'children___children___children',
  children___children___children___children = 'children___children___children___children',
  children___children___children___id = 'children___children___children___id',
  children___children___id = 'children___children___id',
  children___children___internal___content = 'children___children___internal___content',
  children___children___internal___contentDigest = 'children___children___internal___contentDigest',
  children___children___internal___description = 'children___children___internal___description',
  children___children___internal___fieldOwners = 'children___children___internal___fieldOwners',
  children___children___internal___ignoreType = 'children___children___internal___ignoreType',
  children___children___internal___mediaType = 'children___children___internal___mediaType',
  children___children___internal___owner = 'children___children___internal___owner',
  children___children___internal___type = 'children___children___internal___type',
  children___children___parent___children = 'children___children___parent___children',
  children___children___parent___id = 'children___children___parent___id',
  children___id = 'children___id',
  children___internal___content = 'children___internal___content',
  children___internal___contentDigest = 'children___internal___contentDigest',
  children___internal___description = 'children___internal___description',
  children___internal___fieldOwners = 'children___internal___fieldOwners',
  children___internal___ignoreType = 'children___internal___ignoreType',
  children___internal___mediaType = 'children___internal___mediaType',
  children___internal___owner = 'children___internal___owner',
  children___internal___type = 'children___internal___type',
  children___parent___children = 'children___parent___children',
  children___parent___children___children = 'children___parent___children___children',
  children___parent___children___id = 'children___parent___children___id',
  children___parent___id = 'children___parent___id',
  children___parent___internal___content = 'children___parent___internal___content',
  children___parent___internal___contentDigest = 'children___parent___internal___contentDigest',
  children___parent___internal___description = 'children___parent___internal___description',
  children___parent___internal___fieldOwners = 'children___parent___internal___fieldOwners',
  children___parent___internal___ignoreType = 'children___parent___internal___ignoreType',
  children___parent___internal___mediaType = 'children___parent___internal___mediaType',
  children___parent___internal___owner = 'children___parent___internal___owner',
  children___parent___internal___type = 'children___parent___internal___type',
  children___parent___parent___children = 'children___parent___parent___children',
  children___parent___parent___id = 'children___parent___parent___id',
  ctime = 'ctime',
  ctimeMs = 'ctimeMs',
  dev = 'dev',
  dir = 'dir',
  ext = 'ext',
  extension = 'extension',
  gid = 'gid',
  id = 'id',
  ino = 'ino',
  internal___content = 'internal___content',
  internal___contentDigest = 'internal___contentDigest',
  internal___description = 'internal___description',
  internal___fieldOwners = 'internal___fieldOwners',
  internal___ignoreType = 'internal___ignoreType',
  internal___mediaType = 'internal___mediaType',
  internal___owner = 'internal___owner',
  internal___type = 'internal___type',
  mode = 'mode',
  modifiedTime = 'modifiedTime',
  mtime = 'mtime',
  mtimeMs = 'mtimeMs',
  name = 'name',
  nlink = 'nlink',
  parent___children = 'parent___children',
  parent___children___children = 'parent___children___children',
  parent___children___children___children = 'parent___children___children___children',
  parent___children___children___id = 'parent___children___children___id',
  parent___children___id = 'parent___children___id',
  parent___children___internal___content = 'parent___children___internal___content',
  parent___children___internal___contentDigest = 'parent___children___internal___contentDigest',
  parent___children___internal___description = 'parent___children___internal___description',
  parent___children___internal___fieldOwners = 'parent___children___internal___fieldOwners',
  parent___children___internal___ignoreType = 'parent___children___internal___ignoreType',
  parent___children___internal___mediaType = 'parent___children___internal___mediaType',
  parent___children___internal___owner = 'parent___children___internal___owner',
  parent___children___internal___type = 'parent___children___internal___type',
  parent___children___parent___children = 'parent___children___parent___children',
  parent___children___parent___id = 'parent___children___parent___id',
  parent___id = 'parent___id',
  parent___internal___content = 'parent___internal___content',
  parent___internal___contentDigest = 'parent___internal___contentDigest',
  parent___internal___description = 'parent___internal___description',
  parent___internal___fieldOwners = 'parent___internal___fieldOwners',
  parent___internal___ignoreType = 'parent___internal___ignoreType',
  parent___internal___mediaType = 'parent___internal___mediaType',
  parent___internal___owner = 'parent___internal___owner',
  parent___internal___type = 'parent___internal___type',
  parent___parent___children = 'parent___parent___children',
  parent___parent___children___children = 'parent___parent___children___children',
  parent___parent___children___id = 'parent___parent___children___id',
  parent___parent___id = 'parent___parent___id',
  parent___parent___internal___content = 'parent___parent___internal___content',
  parent___parent___internal___contentDigest = 'parent___parent___internal___contentDigest',
  parent___parent___internal___description = 'parent___parent___internal___description',
  parent___parent___internal___fieldOwners = 'parent___parent___internal___fieldOwners',
  parent___parent___internal___ignoreType = 'parent___parent___internal___ignoreType',
  parent___parent___internal___mediaType = 'parent___parent___internal___mediaType',
  parent___parent___internal___owner = 'parent___parent___internal___owner',
  parent___parent___internal___type = 'parent___parent___internal___type',
  parent___parent___parent___children = 'parent___parent___parent___children',
  parent___parent___parent___id = 'parent___parent___parent___id',
  prettySize = 'prettySize',
  publicURL = 'publicURL',
  rdev = 'rdev',
  relativeDirectory = 'relativeDirectory',
  relativePath = 'relativePath',
  root = 'root',
  size = 'size',
  sourceInstanceName = 'sourceInstanceName',
  uid = 'uid'
}

export type FileFilterInput = {
  absolutePath?: Maybe<StringQueryOperatorInput>,
  accessTime?: Maybe<DateQueryOperatorInput>,
  atime?: Maybe<DateQueryOperatorInput>,
  atimeMs?: Maybe<FloatQueryOperatorInput>,
  base?: Maybe<StringQueryOperatorInput>,
  birthTime?: Maybe<DateQueryOperatorInput>,
  birthtime?: Maybe<DateQueryOperatorInput>,
  birthtimeMs?: Maybe<FloatQueryOperatorInput>,
  blksize?: Maybe<IntQueryOperatorInput>,
  blocks?: Maybe<IntQueryOperatorInput>,
  changeTime?: Maybe<DateQueryOperatorInput>,
  childImageSharp?: Maybe<ImageSharpFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  ctime?: Maybe<DateQueryOperatorInput>,
  ctimeMs?: Maybe<FloatQueryOperatorInput>,
  dev?: Maybe<IntQueryOperatorInput>,
  dir?: Maybe<StringQueryOperatorInput>,
  ext?: Maybe<StringQueryOperatorInput>,
  extension?: Maybe<StringQueryOperatorInput>,
  gid?: Maybe<IntQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  ino?: Maybe<IntQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  mode?: Maybe<IntQueryOperatorInput>,
  modifiedTime?: Maybe<DateQueryOperatorInput>,
  mtime?: Maybe<DateQueryOperatorInput>,
  mtimeMs?: Maybe<FloatQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  nlink?: Maybe<IntQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  prettySize?: Maybe<StringQueryOperatorInput>,
  publicURL?: Maybe<StringQueryOperatorInput>,
  rdev?: Maybe<IntQueryOperatorInput>,
  relativeDirectory?: Maybe<StringQueryOperatorInput>,
  relativePath?: Maybe<StringQueryOperatorInput>,
  root?: Maybe<StringQueryOperatorInput>,
  size?: Maybe<IntQueryOperatorInput>,
  sourceInstanceName?: Maybe<StringQueryOperatorInput>,
  uid?: Maybe<IntQueryOperatorInput>,
};

export type FileGroupConnection = {
   __typename?: 'FileGroupConnection',
  edges: Array<FileEdge>,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
  nodes: Array<File>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};

export type FileSortInput = {
  fields?: Maybe<Array<Maybe<FileFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type FloatQueryOperatorInput = {
  eq?: Maybe<Scalars['Float']>,
  gt?: Maybe<Scalars['Float']>,
  gte?: Maybe<Scalars['Float']>,
  in?: Maybe<Array<Maybe<Scalars['Float']>>>,
  lt?: Maybe<Scalars['Float']>,
  lte?: Maybe<Scalars['Float']>,
  ne?: Maybe<Scalars['Float']>,
  nin?: Maybe<Array<Maybe<Scalars['Float']>>>,
};

export enum ImageCropFocus {
  ATTENTION = 'ATTENTION',
  CENTER = 'CENTER',
  EAST = 'EAST',
  ENTROPY = 'ENTROPY',
  NORTH = 'NORTH',
  NORTHEAST = 'NORTHEAST',
  NORTHWEST = 'NORTHWEST',
  SOUTH = 'SOUTH',
  SOUTHEAST = 'SOUTHEAST',
  SOUTHWEST = 'SOUTHWEST',
  WEST = 'WEST'
}

export enum ImageFit {
  CONTAIN = 'CONTAIN',
  COVER = 'COVER',
  FILL = 'FILL'
}

export enum ImageFormat {
  JPG = 'JPG',
  NO_CHANGE = 'NO_CHANGE',
  PNG = 'PNG',
  WEBP = 'WEBP'
}

export type ImageSharp = Node & {
   __typename?: 'ImageSharp',
  children: Array<Node>,
  fixed?: Maybe<ImageSharpFixed>,
  fluid?: Maybe<ImageSharpFluid>,
  id: Scalars['ID'],
  internal: Internal,
  original?: Maybe<ImageSharpOriginal>,
  parent?: Maybe<Node>,
  resize?: Maybe<ImageSharpResize>,
  resolutions?: Maybe<ImageSharpResolutions>,
  sizes?: Maybe<ImageSharpSizes>,
};


export type ImageSharpFixedArgs = {
  background?: Maybe<Scalars['String']>,
  base64Width?: Maybe<Scalars['Int']>,
  cropFocus?: Maybe<ImageCropFocus>,
  duotone?: Maybe<DuotoneGradient>,
  fit?: Maybe<ImageFit>,
  grayscale?: Maybe<Scalars['Boolean']>,
  height?: Maybe<Scalars['Int']>,
  jpegProgressive?: Maybe<Scalars['Boolean']>,
  pngCompressionSpeed?: Maybe<Scalars['Int']>,
  quality?: Maybe<Scalars['Int']>,
  rotate?: Maybe<Scalars['Int']>,
  toFormat?: Maybe<ImageFormat>,
  toFormatBase64?: Maybe<ImageFormat>,
  traceSVG?: Maybe<Potrace>,
  trim?: Maybe<Scalars['Float']>,
  width?: Maybe<Scalars['Int']>
};


export type ImageSharpFluidArgs = {
  background?: Maybe<Scalars['String']>,
  base64Width?: Maybe<Scalars['Int']>,
  cropFocus?: Maybe<ImageCropFocus>,
  duotone?: Maybe<DuotoneGradient>,
  fit?: Maybe<ImageFit>,
  grayscale?: Maybe<Scalars['Boolean']>,
  jpegProgressive?: Maybe<Scalars['Boolean']>,
  maxHeight?: Maybe<Scalars['Int']>,
  maxWidth?: Maybe<Scalars['Int']>,
  pngCompressionSpeed?: Maybe<Scalars['Int']>,
  quality?: Maybe<Scalars['Int']>,
  rotate?: Maybe<Scalars['Int']>,
  sizes?: Maybe<Scalars['String']>,
  srcSetBreakpoints?: Maybe<Array<Maybe<Scalars['Int']>>>,
  toFormat?: Maybe<ImageFormat>,
  toFormatBase64?: Maybe<ImageFormat>,
  traceSVG?: Maybe<Potrace>,
  trim?: Maybe<Scalars['Float']>
};


export type ImageSharpResizeArgs = {
  background?: Maybe<Scalars['String']>,
  base64?: Maybe<Scalars['Boolean']>,
  cropFocus?: Maybe<ImageCropFocus>,
  duotone?: Maybe<DuotoneGradient>,
  fit?: Maybe<ImageFit>,
  grayscale?: Maybe<Scalars['Boolean']>,
  height?: Maybe<Scalars['Int']>,
  jpegProgressive?: Maybe<Scalars['Boolean']>,
  pngCompressionLevel?: Maybe<Scalars['Int']>,
  pngCompressionSpeed?: Maybe<Scalars['Int']>,
  quality?: Maybe<Scalars['Int']>,
  rotate?: Maybe<Scalars['Int']>,
  toFormat?: Maybe<ImageFormat>,
  traceSVG?: Maybe<Potrace>,
  trim?: Maybe<Scalars['Float']>,
  width?: Maybe<Scalars['Int']>
};


export type ImageSharpResolutionsArgs = {
  background?: Maybe<Scalars['String']>,
  base64Width?: Maybe<Scalars['Int']>,
  cropFocus?: Maybe<ImageCropFocus>,
  duotone?: Maybe<DuotoneGradient>,
  fit?: Maybe<ImageFit>,
  grayscale?: Maybe<Scalars['Boolean']>,
  height?: Maybe<Scalars['Int']>,
  jpegProgressive?: Maybe<Scalars['Boolean']>,
  pngCompressionSpeed?: Maybe<Scalars['Int']>,
  quality?: Maybe<Scalars['Int']>,
  rotate?: Maybe<Scalars['Int']>,
  toFormat?: Maybe<ImageFormat>,
  toFormatBase64?: Maybe<ImageFormat>,
  traceSVG?: Maybe<Potrace>,
  trim?: Maybe<Scalars['Float']>,
  width?: Maybe<Scalars['Int']>
};


export type ImageSharpSizesArgs = {
  background?: Maybe<Scalars['String']>,
  base64Width?: Maybe<Scalars['Int']>,
  cropFocus?: Maybe<ImageCropFocus>,
  duotone?: Maybe<DuotoneGradient>,
  fit?: Maybe<ImageFit>,
  grayscale?: Maybe<Scalars['Boolean']>,
  jpegProgressive?: Maybe<Scalars['Boolean']>,
  maxHeight?: Maybe<Scalars['Int']>,
  maxWidth?: Maybe<Scalars['Int']>,
  pngCompressionSpeed?: Maybe<Scalars['Int']>,
  quality?: Maybe<Scalars['Int']>,
  rotate?: Maybe<Scalars['Int']>,
  sizes?: Maybe<Scalars['String']>,
  srcSetBreakpoints?: Maybe<Array<Maybe<Scalars['Int']>>>,
  toFormat?: Maybe<ImageFormat>,
  toFormatBase64?: Maybe<ImageFormat>,
  traceSVG?: Maybe<Potrace>,
  trim?: Maybe<Scalars['Float']>
};

export type ImageSharpConnection = {
   __typename?: 'ImageSharpConnection',
  distinct: Array<Scalars['String']>,
  edges: Array<ImageSharpEdge>,
  group: Array<ImageSharpGroupConnection>,
  nodes: Array<ImageSharp>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};


export type ImageSharpConnectionDistinctArgs = {
  field: ImageSharpFieldsEnum
};


export type ImageSharpConnectionGroupArgs = {
  field: ImageSharpFieldsEnum,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>
};

export type ImageSharpEdge = {
   __typename?: 'ImageSharpEdge',
  next?: Maybe<ImageSharp>,
  node: ImageSharp,
  previous?: Maybe<ImageSharp>,
};

export enum ImageSharpFieldsEnum {
  children = 'children',
  children___children = 'children___children',
  children___children___children = 'children___children___children',
  children___children___children___children = 'children___children___children___children',
  children___children___children___id = 'children___children___children___id',
  children___children___id = 'children___children___id',
  children___children___internal___content = 'children___children___internal___content',
  children___children___internal___contentDigest = 'children___children___internal___contentDigest',
  children___children___internal___description = 'children___children___internal___description',
  children___children___internal___fieldOwners = 'children___children___internal___fieldOwners',
  children___children___internal___ignoreType = 'children___children___internal___ignoreType',
  children___children___internal___mediaType = 'children___children___internal___mediaType',
  children___children___internal___owner = 'children___children___internal___owner',
  children___children___internal___type = 'children___children___internal___type',
  children___children___parent___children = 'children___children___parent___children',
  children___children___parent___id = 'children___children___parent___id',
  children___id = 'children___id',
  children___internal___content = 'children___internal___content',
  children___internal___contentDigest = 'children___internal___contentDigest',
  children___internal___description = 'children___internal___description',
  children___internal___fieldOwners = 'children___internal___fieldOwners',
  children___internal___ignoreType = 'children___internal___ignoreType',
  children___internal___mediaType = 'children___internal___mediaType',
  children___internal___owner = 'children___internal___owner',
  children___internal___type = 'children___internal___type',
  children___parent___children = 'children___parent___children',
  children___parent___children___children = 'children___parent___children___children',
  children___parent___children___id = 'children___parent___children___id',
  children___parent___id = 'children___parent___id',
  children___parent___internal___content = 'children___parent___internal___content',
  children___parent___internal___contentDigest = 'children___parent___internal___contentDigest',
  children___parent___internal___description = 'children___parent___internal___description',
  children___parent___internal___fieldOwners = 'children___parent___internal___fieldOwners',
  children___parent___internal___ignoreType = 'children___parent___internal___ignoreType',
  children___parent___internal___mediaType = 'children___parent___internal___mediaType',
  children___parent___internal___owner = 'children___parent___internal___owner',
  children___parent___internal___type = 'children___parent___internal___type',
  children___parent___parent___children = 'children___parent___parent___children',
  children___parent___parent___id = 'children___parent___parent___id',
  fixed___aspectRatio = 'fixed___aspectRatio',
  fixed___base64 = 'fixed___base64',
  fixed___height = 'fixed___height',
  fixed___originalName = 'fixed___originalName',
  fixed___src = 'fixed___src',
  fixed___srcSet = 'fixed___srcSet',
  fixed___srcSetWebp = 'fixed___srcSetWebp',
  fixed___srcWebp = 'fixed___srcWebp',
  fixed___tracedSVG = 'fixed___tracedSVG',
  fixed___width = 'fixed___width',
  fluid___aspectRatio = 'fluid___aspectRatio',
  fluid___base64 = 'fluid___base64',
  fluid___originalImg = 'fluid___originalImg',
  fluid___originalName = 'fluid___originalName',
  fluid___presentationHeight = 'fluid___presentationHeight',
  fluid___presentationWidth = 'fluid___presentationWidth',
  fluid___sizes = 'fluid___sizes',
  fluid___src = 'fluid___src',
  fluid___srcSet = 'fluid___srcSet',
  fluid___srcSetWebp = 'fluid___srcSetWebp',
  fluid___srcWebp = 'fluid___srcWebp',
  fluid___tracedSVG = 'fluid___tracedSVG',
  id = 'id',
  internal___content = 'internal___content',
  internal___contentDigest = 'internal___contentDigest',
  internal___description = 'internal___description',
  internal___fieldOwners = 'internal___fieldOwners',
  internal___ignoreType = 'internal___ignoreType',
  internal___mediaType = 'internal___mediaType',
  internal___owner = 'internal___owner',
  internal___type = 'internal___type',
  original___height = 'original___height',
  original___src = 'original___src',
  original___width = 'original___width',
  parent___children = 'parent___children',
  parent___children___children = 'parent___children___children',
  parent___children___children___children = 'parent___children___children___children',
  parent___children___children___id = 'parent___children___children___id',
  parent___children___id = 'parent___children___id',
  parent___children___internal___content = 'parent___children___internal___content',
  parent___children___internal___contentDigest = 'parent___children___internal___contentDigest',
  parent___children___internal___description = 'parent___children___internal___description',
  parent___children___internal___fieldOwners = 'parent___children___internal___fieldOwners',
  parent___children___internal___ignoreType = 'parent___children___internal___ignoreType',
  parent___children___internal___mediaType = 'parent___children___internal___mediaType',
  parent___children___internal___owner = 'parent___children___internal___owner',
  parent___children___internal___type = 'parent___children___internal___type',
  parent___children___parent___children = 'parent___children___parent___children',
  parent___children___parent___id = 'parent___children___parent___id',
  parent___id = 'parent___id',
  parent___internal___content = 'parent___internal___content',
  parent___internal___contentDigest = 'parent___internal___contentDigest',
  parent___internal___description = 'parent___internal___description',
  parent___internal___fieldOwners = 'parent___internal___fieldOwners',
  parent___internal___ignoreType = 'parent___internal___ignoreType',
  parent___internal___mediaType = 'parent___internal___mediaType',
  parent___internal___owner = 'parent___internal___owner',
  parent___internal___type = 'parent___internal___type',
  parent___parent___children = 'parent___parent___children',
  parent___parent___children___children = 'parent___parent___children___children',
  parent___parent___children___id = 'parent___parent___children___id',
  parent___parent___id = 'parent___parent___id',
  parent___parent___internal___content = 'parent___parent___internal___content',
  parent___parent___internal___contentDigest = 'parent___parent___internal___contentDigest',
  parent___parent___internal___description = 'parent___parent___internal___description',
  parent___parent___internal___fieldOwners = 'parent___parent___internal___fieldOwners',
  parent___parent___internal___ignoreType = 'parent___parent___internal___ignoreType',
  parent___parent___internal___mediaType = 'parent___parent___internal___mediaType',
  parent___parent___internal___owner = 'parent___parent___internal___owner',
  parent___parent___internal___type = 'parent___parent___internal___type',
  parent___parent___parent___children = 'parent___parent___parent___children',
  parent___parent___parent___id = 'parent___parent___parent___id',
  resize___aspectRatio = 'resize___aspectRatio',
  resize___height = 'resize___height',
  resize___originalName = 'resize___originalName',
  resize___src = 'resize___src',
  resize___tracedSVG = 'resize___tracedSVG',
  resize___width = 'resize___width',
  resolutions___aspectRatio = 'resolutions___aspectRatio',
  resolutions___base64 = 'resolutions___base64',
  resolutions___height = 'resolutions___height',
  resolutions___originalName = 'resolutions___originalName',
  resolutions___src = 'resolutions___src',
  resolutions___srcSet = 'resolutions___srcSet',
  resolutions___srcSetWebp = 'resolutions___srcSetWebp',
  resolutions___srcWebp = 'resolutions___srcWebp',
  resolutions___tracedSVG = 'resolutions___tracedSVG',
  resolutions___width = 'resolutions___width',
  sizes___aspectRatio = 'sizes___aspectRatio',
  sizes___base64 = 'sizes___base64',
  sizes___originalImg = 'sizes___originalImg',
  sizes___originalName = 'sizes___originalName',
  sizes___presentationHeight = 'sizes___presentationHeight',
  sizes___presentationWidth = 'sizes___presentationWidth',
  sizes___sizes = 'sizes___sizes',
  sizes___src = 'sizes___src',
  sizes___srcSet = 'sizes___srcSet',
  sizes___srcSetWebp = 'sizes___srcSetWebp',
  sizes___srcWebp = 'sizes___srcWebp',
  sizes___tracedSVG = 'sizes___tracedSVG'
}

export type ImageSharpFilterInput = {
  children?: Maybe<NodeFilterListInput>,
  fixed?: Maybe<ImageSharpFixedFilterInput>,
  fluid?: Maybe<ImageSharpFluidFilterInput>,
  id?: Maybe<StringQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  original?: Maybe<ImageSharpOriginalFilterInput>,
  parent?: Maybe<NodeFilterInput>,
  resize?: Maybe<ImageSharpResizeFilterInput>,
  resolutions?: Maybe<ImageSharpResolutionsFilterInput>,
  sizes?: Maybe<ImageSharpSizesFilterInput>,
};

export type ImageSharpFixed = {
   __typename?: 'ImageSharpFixed',
  aspectRatio?: Maybe<Scalars['Float']>,
  base64?: Maybe<Scalars['String']>,
  height?: Maybe<Scalars['Float']>,
  originalName?: Maybe<Scalars['String']>,
  src?: Maybe<Scalars['String']>,
  srcSet?: Maybe<Scalars['String']>,
  srcSetWebp?: Maybe<Scalars['String']>,
  srcWebp?: Maybe<Scalars['String']>,
  tracedSVG?: Maybe<Scalars['String']>,
  width?: Maybe<Scalars['Float']>,
};

export type ImageSharpFixedFilterInput = {
  aspectRatio?: Maybe<FloatQueryOperatorInput>,
  base64?: Maybe<StringQueryOperatorInput>,
  height?: Maybe<FloatQueryOperatorInput>,
  originalName?: Maybe<StringQueryOperatorInput>,
  src?: Maybe<StringQueryOperatorInput>,
  srcSet?: Maybe<StringQueryOperatorInput>,
  srcSetWebp?: Maybe<StringQueryOperatorInput>,
  srcWebp?: Maybe<StringQueryOperatorInput>,
  tracedSVG?: Maybe<StringQueryOperatorInput>,
  width?: Maybe<FloatQueryOperatorInput>,
};

export type ImageSharpFluid = {
   __typename?: 'ImageSharpFluid',
  aspectRatio?: Maybe<Scalars['Float']>,
  base64?: Maybe<Scalars['String']>,
  originalImg?: Maybe<Scalars['String']>,
  originalName?: Maybe<Scalars['String']>,
  presentationHeight?: Maybe<Scalars['Int']>,
  presentationWidth?: Maybe<Scalars['Int']>,
  sizes?: Maybe<Scalars['String']>,
  src?: Maybe<Scalars['String']>,
  srcSet?: Maybe<Scalars['String']>,
  srcSetWebp?: Maybe<Scalars['String']>,
  srcWebp?: Maybe<Scalars['String']>,
  tracedSVG?: Maybe<Scalars['String']>,
};

export type ImageSharpFluidFilterInput = {
  aspectRatio?: Maybe<FloatQueryOperatorInput>,
  base64?: Maybe<StringQueryOperatorInput>,
  originalImg?: Maybe<StringQueryOperatorInput>,
  originalName?: Maybe<StringQueryOperatorInput>,
  presentationHeight?: Maybe<IntQueryOperatorInput>,
  presentationWidth?: Maybe<IntQueryOperatorInput>,
  sizes?: Maybe<StringQueryOperatorInput>,
  src?: Maybe<StringQueryOperatorInput>,
  srcSet?: Maybe<StringQueryOperatorInput>,
  srcSetWebp?: Maybe<StringQueryOperatorInput>,
  srcWebp?: Maybe<StringQueryOperatorInput>,
  tracedSVG?: Maybe<StringQueryOperatorInput>,
};

export type ImageSharpGroupConnection = {
   __typename?: 'ImageSharpGroupConnection',
  edges: Array<ImageSharpEdge>,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
  nodes: Array<ImageSharp>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};

export type ImageSharpOriginal = {
   __typename?: 'ImageSharpOriginal',
  height?: Maybe<Scalars['Float']>,
  src?: Maybe<Scalars['String']>,
  width?: Maybe<Scalars['Float']>,
};

export type ImageSharpOriginalFilterInput = {
  height?: Maybe<FloatQueryOperatorInput>,
  src?: Maybe<StringQueryOperatorInput>,
  width?: Maybe<FloatQueryOperatorInput>,
};

export type ImageSharpResize = {
   __typename?: 'ImageSharpResize',
  aspectRatio?: Maybe<Scalars['Float']>,
  height?: Maybe<Scalars['Int']>,
  originalName?: Maybe<Scalars['String']>,
  src?: Maybe<Scalars['String']>,
  tracedSVG?: Maybe<Scalars['String']>,
  width?: Maybe<Scalars['Int']>,
};

export type ImageSharpResizeFilterInput = {
  aspectRatio?: Maybe<FloatQueryOperatorInput>,
  height?: Maybe<IntQueryOperatorInput>,
  originalName?: Maybe<StringQueryOperatorInput>,
  src?: Maybe<StringQueryOperatorInput>,
  tracedSVG?: Maybe<StringQueryOperatorInput>,
  width?: Maybe<IntQueryOperatorInput>,
};

export type ImageSharpResolutions = {
   __typename?: 'ImageSharpResolutions',
  aspectRatio?: Maybe<Scalars['Float']>,
  base64?: Maybe<Scalars['String']>,
  height?: Maybe<Scalars['Float']>,
  originalName?: Maybe<Scalars['String']>,
  src?: Maybe<Scalars['String']>,
  srcSet?: Maybe<Scalars['String']>,
  srcSetWebp?: Maybe<Scalars['String']>,
  srcWebp?: Maybe<Scalars['String']>,
  tracedSVG?: Maybe<Scalars['String']>,
  width?: Maybe<Scalars['Float']>,
};

export type ImageSharpResolutionsFilterInput = {
  aspectRatio?: Maybe<FloatQueryOperatorInput>,
  base64?: Maybe<StringQueryOperatorInput>,
  height?: Maybe<FloatQueryOperatorInput>,
  originalName?: Maybe<StringQueryOperatorInput>,
  src?: Maybe<StringQueryOperatorInput>,
  srcSet?: Maybe<StringQueryOperatorInput>,
  srcSetWebp?: Maybe<StringQueryOperatorInput>,
  srcWebp?: Maybe<StringQueryOperatorInput>,
  tracedSVG?: Maybe<StringQueryOperatorInput>,
  width?: Maybe<FloatQueryOperatorInput>,
};

export type ImageSharpSizes = {
   __typename?: 'ImageSharpSizes',
  aspectRatio?: Maybe<Scalars['Float']>,
  base64?: Maybe<Scalars['String']>,
  originalImg?: Maybe<Scalars['String']>,
  originalName?: Maybe<Scalars['String']>,
  presentationHeight?: Maybe<Scalars['Int']>,
  presentationWidth?: Maybe<Scalars['Int']>,
  sizes?: Maybe<Scalars['String']>,
  src?: Maybe<Scalars['String']>,
  srcSet?: Maybe<Scalars['String']>,
  srcSetWebp?: Maybe<Scalars['String']>,
  srcWebp?: Maybe<Scalars['String']>,
  tracedSVG?: Maybe<Scalars['String']>,
};

export type ImageSharpSizesFilterInput = {
  aspectRatio?: Maybe<FloatQueryOperatorInput>,
  base64?: Maybe<StringQueryOperatorInput>,
  originalImg?: Maybe<StringQueryOperatorInput>,
  originalName?: Maybe<StringQueryOperatorInput>,
  presentationHeight?: Maybe<IntQueryOperatorInput>,
  presentationWidth?: Maybe<IntQueryOperatorInput>,
  sizes?: Maybe<StringQueryOperatorInput>,
  src?: Maybe<StringQueryOperatorInput>,
  srcSet?: Maybe<StringQueryOperatorInput>,
  srcSetWebp?: Maybe<StringQueryOperatorInput>,
  srcWebp?: Maybe<StringQueryOperatorInput>,
  tracedSVG?: Maybe<StringQueryOperatorInput>,
};

export type ImageSharpSortInput = {
  fields?: Maybe<Array<Maybe<ImageSharpFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type Internal = {
   __typename?: 'Internal',
  content?: Maybe<Scalars['String']>,
  contentDigest: Scalars['String'],
  description?: Maybe<Scalars['String']>,
  fieldOwners?: Maybe<Array<Maybe<Scalars['String']>>>,
  ignoreType?: Maybe<Scalars['Boolean']>,
  mediaType?: Maybe<Scalars['String']>,
  owner: Scalars['String'],
  type: Scalars['String'],
};

export type InternalFilterInput = {
  content?: Maybe<StringQueryOperatorInput>,
  contentDigest?: Maybe<StringQueryOperatorInput>,
  description?: Maybe<StringQueryOperatorInput>,
  fieldOwners?: Maybe<StringQueryOperatorInput>,
  ignoreType?: Maybe<BooleanQueryOperatorInput>,
  mediaType?: Maybe<StringQueryOperatorInput>,
  owner?: Maybe<StringQueryOperatorInput>,
  type?: Maybe<StringQueryOperatorInput>,
};

export type IntQueryOperatorInput = {
  eq?: Maybe<Scalars['Int']>,
  gt?: Maybe<Scalars['Int']>,
  gte?: Maybe<Scalars['Int']>,
  in?: Maybe<Array<Maybe<Scalars['Int']>>>,
  lt?: Maybe<Scalars['Int']>,
  lte?: Maybe<Scalars['Int']>,
  ne?: Maybe<Scalars['Int']>,
  nin?: Maybe<Array<Maybe<Scalars['Int']>>>,
};


/** Node Interface */
export type Node = {
  children: Array<Node>,
  id: Scalars['ID'],
  internal: Internal,
  parent?: Maybe<Node>,
};

export type NodeFilterInput = {
  children?: Maybe<NodeFilterListInput>,
  id?: Maybe<StringQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  parent?: Maybe<NodeFilterInput>,
};

export type NodeFilterListInput = {
  elemMatch?: Maybe<NodeFilterInput>,
};

export type PageInfo = {
   __typename?: 'PageInfo',
  currentPage: Scalars['Int'],
  hasNextPage: Scalars['Boolean'],
  hasPreviousPage: Scalars['Boolean'],
  itemCount: Scalars['Int'],
  pageCount: Scalars['Int'],
  perPage?: Maybe<Scalars['Int']>,
};

export type Potrace = {
  alphaMax?: Maybe<Scalars['Float']>,
  background?: Maybe<Scalars['String']>,
  blackOnWhite?: Maybe<Scalars['Boolean']>,
  color?: Maybe<Scalars['String']>,
  optCurve?: Maybe<Scalars['Boolean']>,
  optTolerance?: Maybe<Scalars['Float']>,
  threshold?: Maybe<Scalars['Int']>,
  turdSize?: Maybe<Scalars['Float']>,
  turnPolicy?: Maybe<PotraceTurnPolicy>,
};

export enum PotraceTurnPolicy {
  TURNPOLICY_BLACK = 'TURNPOLICY_BLACK',
  TURNPOLICY_LEFT = 'TURNPOLICY_LEFT',
  TURNPOLICY_MAJORITY = 'TURNPOLICY_MAJORITY',
  TURNPOLICY_MINORITY = 'TURNPOLICY_MINORITY',
  TURNPOLICY_RIGHT = 'TURNPOLICY_RIGHT',
  TURNPOLICY_WHITE = 'TURNPOLICY_WHITE'
}

export type Query = {
   __typename?: 'Query',
  allDirectory: DirectoryConnection,
  allFile: FileConnection,
  allImageSharp: ImageSharpConnection,
  allSite: SiteConnection,
  allSitePage: SitePageConnection,
  allSitePlugin: SitePluginConnection,
  directory?: Maybe<Directory>,
  file?: Maybe<File>,
  imageSharp?: Maybe<ImageSharp>,
  site?: Maybe<Site>,
  sitePage?: Maybe<SitePage>,
  sitePlugin?: Maybe<SitePlugin>,
};


export type QueryAllDirectoryArgs = {
  filter?: Maybe<DirectoryFilterInput>,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>,
  sort?: Maybe<DirectorySortInput>
};


export type QueryAllFileArgs = {
  filter?: Maybe<FileFilterInput>,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>,
  sort?: Maybe<FileSortInput>
};


export type QueryAllImageSharpArgs = {
  filter?: Maybe<ImageSharpFilterInput>,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>,
  sort?: Maybe<ImageSharpSortInput>
};


export type QueryAllSiteArgs = {
  filter?: Maybe<SiteFilterInput>,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>,
  sort?: Maybe<SiteSortInput>
};


export type QueryAllSitePageArgs = {
  filter?: Maybe<SitePageFilterInput>,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>,
  sort?: Maybe<SitePageSortInput>
};


export type QueryAllSitePluginArgs = {
  filter?: Maybe<SitePluginFilterInput>,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>,
  sort?: Maybe<SitePluginSortInput>
};


export type QueryDirectoryArgs = {
  absolutePath?: Maybe<StringQueryOperatorInput>,
  accessTime?: Maybe<DateQueryOperatorInput>,
  atime?: Maybe<DateQueryOperatorInput>,
  atimeMs?: Maybe<FloatQueryOperatorInput>,
  base?: Maybe<StringQueryOperatorInput>,
  birthTime?: Maybe<DateQueryOperatorInput>,
  birthtime?: Maybe<DateQueryOperatorInput>,
  birthtimeMs?: Maybe<FloatQueryOperatorInput>,
  blksize?: Maybe<IntQueryOperatorInput>,
  blocks?: Maybe<IntQueryOperatorInput>,
  changeTime?: Maybe<DateQueryOperatorInput>,
  children?: Maybe<NodeFilterListInput>,
  ctime?: Maybe<DateQueryOperatorInput>,
  ctimeMs?: Maybe<FloatQueryOperatorInput>,
  dev?: Maybe<IntQueryOperatorInput>,
  dir?: Maybe<StringQueryOperatorInput>,
  ext?: Maybe<StringQueryOperatorInput>,
  extension?: Maybe<StringQueryOperatorInput>,
  gid?: Maybe<IntQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  ino?: Maybe<IntQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  mode?: Maybe<IntQueryOperatorInput>,
  modifiedTime?: Maybe<DateQueryOperatorInput>,
  mtime?: Maybe<DateQueryOperatorInput>,
  mtimeMs?: Maybe<FloatQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  nlink?: Maybe<IntQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  prettySize?: Maybe<StringQueryOperatorInput>,
  rdev?: Maybe<IntQueryOperatorInput>,
  relativeDirectory?: Maybe<StringQueryOperatorInput>,
  relativePath?: Maybe<StringQueryOperatorInput>,
  root?: Maybe<StringQueryOperatorInput>,
  size?: Maybe<IntQueryOperatorInput>,
  sourceInstanceName?: Maybe<StringQueryOperatorInput>,
  uid?: Maybe<IntQueryOperatorInput>
};


export type QueryFileArgs = {
  absolutePath?: Maybe<StringQueryOperatorInput>,
  accessTime?: Maybe<DateQueryOperatorInput>,
  atime?: Maybe<DateQueryOperatorInput>,
  atimeMs?: Maybe<FloatQueryOperatorInput>,
  base?: Maybe<StringQueryOperatorInput>,
  birthTime?: Maybe<DateQueryOperatorInput>,
  birthtime?: Maybe<DateQueryOperatorInput>,
  birthtimeMs?: Maybe<FloatQueryOperatorInput>,
  blksize?: Maybe<IntQueryOperatorInput>,
  blocks?: Maybe<IntQueryOperatorInput>,
  changeTime?: Maybe<DateQueryOperatorInput>,
  childImageSharp?: Maybe<ImageSharpFilterInput>,
  children?: Maybe<NodeFilterListInput>,
  ctime?: Maybe<DateQueryOperatorInput>,
  ctimeMs?: Maybe<FloatQueryOperatorInput>,
  dev?: Maybe<IntQueryOperatorInput>,
  dir?: Maybe<StringQueryOperatorInput>,
  ext?: Maybe<StringQueryOperatorInput>,
  extension?: Maybe<StringQueryOperatorInput>,
  gid?: Maybe<IntQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  ino?: Maybe<IntQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  mode?: Maybe<IntQueryOperatorInput>,
  modifiedTime?: Maybe<DateQueryOperatorInput>,
  mtime?: Maybe<DateQueryOperatorInput>,
  mtimeMs?: Maybe<FloatQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  nlink?: Maybe<IntQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  prettySize?: Maybe<StringQueryOperatorInput>,
  publicURL?: Maybe<StringQueryOperatorInput>,
  rdev?: Maybe<IntQueryOperatorInput>,
  relativeDirectory?: Maybe<StringQueryOperatorInput>,
  relativePath?: Maybe<StringQueryOperatorInput>,
  root?: Maybe<StringQueryOperatorInput>,
  size?: Maybe<IntQueryOperatorInput>,
  sourceInstanceName?: Maybe<StringQueryOperatorInput>,
  uid?: Maybe<IntQueryOperatorInput>
};


export type QueryImageSharpArgs = {
  children?: Maybe<NodeFilterListInput>,
  fixed?: Maybe<ImageSharpFixedFilterInput>,
  fluid?: Maybe<ImageSharpFluidFilterInput>,
  id?: Maybe<StringQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  original?: Maybe<ImageSharpOriginalFilterInput>,
  parent?: Maybe<NodeFilterInput>,
  resize?: Maybe<ImageSharpResizeFilterInput>,
  resolutions?: Maybe<ImageSharpResolutionsFilterInput>,
  sizes?: Maybe<ImageSharpSizesFilterInput>
};


export type QuerySiteArgs = {
  buildTime?: Maybe<DateQueryOperatorInput>,
  children?: Maybe<NodeFilterListInput>,
  host?: Maybe<StringQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  parent?: Maybe<NodeFilterInput>,
  pathPrefix?: Maybe<StringQueryOperatorInput>,
  polyfill?: Maybe<BooleanQueryOperatorInput>,
  port?: Maybe<IntQueryOperatorInput>,
  siteMetadata?: Maybe<SiteSiteMetadataFilterInput>
};


export type QuerySitePageArgs = {
  children?: Maybe<NodeFilterListInput>,
  component?: Maybe<StringQueryOperatorInput>,
  componentChunkName?: Maybe<StringQueryOperatorInput>,
  componentPath?: Maybe<StringQueryOperatorInput>,
  context?: Maybe<SitePageContextFilterInput>,
  id?: Maybe<StringQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  internalComponentName?: Maybe<StringQueryOperatorInput>,
  isCreatedByStatefulCreatePages?: Maybe<BooleanQueryOperatorInput>,
  matchPath?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  path?: Maybe<StringQueryOperatorInput>,
  pluginCreator?: Maybe<SitePluginFilterInput>,
  pluginCreatorId?: Maybe<StringQueryOperatorInput>
};


export type QuerySitePluginArgs = {
  browserAPIs?: Maybe<StringQueryOperatorInput>,
  children?: Maybe<NodeFilterListInput>,
  id?: Maybe<StringQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  name?: Maybe<StringQueryOperatorInput>,
  nodeAPIs?: Maybe<StringQueryOperatorInput>,
  packageJson?: Maybe<SitePluginPackageJsonFilterInput>,
  parent?: Maybe<NodeFilterInput>,
  pluginFilepath?: Maybe<StringQueryOperatorInput>,
  pluginOptions?: Maybe<SitePluginPluginOptionsFilterInput>,
  resolve?: Maybe<StringQueryOperatorInput>,
  ssrAPIs?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>
};

export type Site = Node & {
   __typename?: 'Site',
  buildTime?: Maybe<Scalars['Date']>,
  children: Array<Node>,
  host?: Maybe<Scalars['String']>,
  id: Scalars['ID'],
  internal: Internal,
  parent?: Maybe<Node>,
  pathPrefix?: Maybe<Scalars['String']>,
  polyfill?: Maybe<Scalars['Boolean']>,
  port?: Maybe<Scalars['Int']>,
  siteMetadata?: Maybe<SiteSiteMetadata>,
};


export type SiteBuildTimeArgs = {
  difference?: Maybe<Scalars['String']>,
  formatString?: Maybe<Scalars['String']>,
  fromNow?: Maybe<Scalars['Boolean']>,
  locale?: Maybe<Scalars['String']>
};

export type SiteConnection = {
   __typename?: 'SiteConnection',
  distinct: Array<Scalars['String']>,
  edges: Array<SiteEdge>,
  group: Array<SiteGroupConnection>,
  nodes: Array<Site>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};


export type SiteConnectionDistinctArgs = {
  field: SiteFieldsEnum
};


export type SiteConnectionGroupArgs = {
  field: SiteFieldsEnum,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>
};

export type SiteEdge = {
   __typename?: 'SiteEdge',
  next?: Maybe<Site>,
  node: Site,
  previous?: Maybe<Site>,
};

export enum SiteFieldsEnum {
  buildTime = 'buildTime',
  children = 'children',
  children___children = 'children___children',
  children___children___children = 'children___children___children',
  children___children___children___children = 'children___children___children___children',
  children___children___children___id = 'children___children___children___id',
  children___children___id = 'children___children___id',
  children___children___internal___content = 'children___children___internal___content',
  children___children___internal___contentDigest = 'children___children___internal___contentDigest',
  children___children___internal___description = 'children___children___internal___description',
  children___children___internal___fieldOwners = 'children___children___internal___fieldOwners',
  children___children___internal___ignoreType = 'children___children___internal___ignoreType',
  children___children___internal___mediaType = 'children___children___internal___mediaType',
  children___children___internal___owner = 'children___children___internal___owner',
  children___children___internal___type = 'children___children___internal___type',
  children___children___parent___children = 'children___children___parent___children',
  children___children___parent___id = 'children___children___parent___id',
  children___id = 'children___id',
  children___internal___content = 'children___internal___content',
  children___internal___contentDigest = 'children___internal___contentDigest',
  children___internal___description = 'children___internal___description',
  children___internal___fieldOwners = 'children___internal___fieldOwners',
  children___internal___ignoreType = 'children___internal___ignoreType',
  children___internal___mediaType = 'children___internal___mediaType',
  children___internal___owner = 'children___internal___owner',
  children___internal___type = 'children___internal___type',
  children___parent___children = 'children___parent___children',
  children___parent___children___children = 'children___parent___children___children',
  children___parent___children___id = 'children___parent___children___id',
  children___parent___id = 'children___parent___id',
  children___parent___internal___content = 'children___parent___internal___content',
  children___parent___internal___contentDigest = 'children___parent___internal___contentDigest',
  children___parent___internal___description = 'children___parent___internal___description',
  children___parent___internal___fieldOwners = 'children___parent___internal___fieldOwners',
  children___parent___internal___ignoreType = 'children___parent___internal___ignoreType',
  children___parent___internal___mediaType = 'children___parent___internal___mediaType',
  children___parent___internal___owner = 'children___parent___internal___owner',
  children___parent___internal___type = 'children___parent___internal___type',
  children___parent___parent___children = 'children___parent___parent___children',
  children___parent___parent___id = 'children___parent___parent___id',
  host = 'host',
  id = 'id',
  internal___content = 'internal___content',
  internal___contentDigest = 'internal___contentDigest',
  internal___description = 'internal___description',
  internal___fieldOwners = 'internal___fieldOwners',
  internal___ignoreType = 'internal___ignoreType',
  internal___mediaType = 'internal___mediaType',
  internal___owner = 'internal___owner',
  internal___type = 'internal___type',
  parent___children = 'parent___children',
  parent___children___children = 'parent___children___children',
  parent___children___children___children = 'parent___children___children___children',
  parent___children___children___id = 'parent___children___children___id',
  parent___children___id = 'parent___children___id',
  parent___children___internal___content = 'parent___children___internal___content',
  parent___children___internal___contentDigest = 'parent___children___internal___contentDigest',
  parent___children___internal___description = 'parent___children___internal___description',
  parent___children___internal___fieldOwners = 'parent___children___internal___fieldOwners',
  parent___children___internal___ignoreType = 'parent___children___internal___ignoreType',
  parent___children___internal___mediaType = 'parent___children___internal___mediaType',
  parent___children___internal___owner = 'parent___children___internal___owner',
  parent___children___internal___type = 'parent___children___internal___type',
  parent___children___parent___children = 'parent___children___parent___children',
  parent___children___parent___id = 'parent___children___parent___id',
  parent___id = 'parent___id',
  parent___internal___content = 'parent___internal___content',
  parent___internal___contentDigest = 'parent___internal___contentDigest',
  parent___internal___description = 'parent___internal___description',
  parent___internal___fieldOwners = 'parent___internal___fieldOwners',
  parent___internal___ignoreType = 'parent___internal___ignoreType',
  parent___internal___mediaType = 'parent___internal___mediaType',
  parent___internal___owner = 'parent___internal___owner',
  parent___internal___type = 'parent___internal___type',
  parent___parent___children = 'parent___parent___children',
  parent___parent___children___children = 'parent___parent___children___children',
  parent___parent___children___id = 'parent___parent___children___id',
  parent___parent___id = 'parent___parent___id',
  parent___parent___internal___content = 'parent___parent___internal___content',
  parent___parent___internal___contentDigest = 'parent___parent___internal___contentDigest',
  parent___parent___internal___description = 'parent___parent___internal___description',
  parent___parent___internal___fieldOwners = 'parent___parent___internal___fieldOwners',
  parent___parent___internal___ignoreType = 'parent___parent___internal___ignoreType',
  parent___parent___internal___mediaType = 'parent___parent___internal___mediaType',
  parent___parent___internal___owner = 'parent___parent___internal___owner',
  parent___parent___internal___type = 'parent___parent___internal___type',
  parent___parent___parent___children = 'parent___parent___parent___children',
  parent___parent___parent___id = 'parent___parent___parent___id',
  pathPrefix = 'pathPrefix',
  polyfill = 'polyfill',
  port = 'port',
  siteMetadata___author = 'siteMetadata___author',
  siteMetadata___description = 'siteMetadata___description',
  siteMetadata___siteUrl = 'siteMetadata___siteUrl',
  siteMetadata___title = 'siteMetadata___title'
}

export type SiteFilterInput = {
  buildTime?: Maybe<DateQueryOperatorInput>,
  children?: Maybe<NodeFilterListInput>,
  host?: Maybe<StringQueryOperatorInput>,
  id?: Maybe<StringQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  parent?: Maybe<NodeFilterInput>,
  pathPrefix?: Maybe<StringQueryOperatorInput>,
  polyfill?: Maybe<BooleanQueryOperatorInput>,
  port?: Maybe<IntQueryOperatorInput>,
  siteMetadata?: Maybe<SiteSiteMetadataFilterInput>,
};

export type SiteGroupConnection = {
   __typename?: 'SiteGroupConnection',
  edges: Array<SiteEdge>,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
  nodes: Array<Site>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};

export type SitePage = Node & {
   __typename?: 'SitePage',
  children: Array<Node>,
  component?: Maybe<Scalars['String']>,
  componentChunkName?: Maybe<Scalars['String']>,
  componentPath?: Maybe<Scalars['String']>,
  context?: Maybe<SitePageContext>,
  id: Scalars['ID'],
  internal: Internal,
  internalComponentName?: Maybe<Scalars['String']>,
  isCreatedByStatefulCreatePages?: Maybe<Scalars['Boolean']>,
  matchPath?: Maybe<Scalars['String']>,
  parent?: Maybe<Node>,
  path?: Maybe<Scalars['String']>,
  pluginCreator?: Maybe<SitePlugin>,
  pluginCreatorId?: Maybe<Scalars['String']>,
};

export type SitePageConnection = {
   __typename?: 'SitePageConnection',
  distinct: Array<Scalars['String']>,
  edges: Array<SitePageEdge>,
  group: Array<SitePageGroupConnection>,
  nodes: Array<SitePage>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};


export type SitePageConnectionDistinctArgs = {
  field: SitePageFieldsEnum
};


export type SitePageConnectionGroupArgs = {
  field: SitePageFieldsEnum,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>
};

export type SitePageContext = {
   __typename?: 'SitePageContext',
  intl?: Maybe<SitePageContextIntl>,
};

export type SitePageContextFilterInput = {
  intl?: Maybe<SitePageContextIntlFilterInput>,
};

export type SitePageContextIntl = {
   __typename?: 'SitePageContextIntl',
  language?: Maybe<Scalars['String']>,
  languages?: Maybe<Array<Maybe<Scalars['String']>>>,
  messages?: Maybe<SitePageContextIntlMessages>,
  originalPath?: Maybe<Scalars['String']>,
  redirect?: Maybe<Scalars['Boolean']>,
  routed?: Maybe<Scalars['Boolean']>,
};

export type SitePageContextIntlFilterInput = {
  language?: Maybe<StringQueryOperatorInput>,
  languages?: Maybe<StringQueryOperatorInput>,
  messages?: Maybe<SitePageContextIntlMessagesFilterInput>,
  originalPath?: Maybe<StringQueryOperatorInput>,
  redirect?: Maybe<BooleanQueryOperatorInput>,
  routed?: Maybe<BooleanQueryOperatorInput>,
};

export type SitePageContextIntlMessages = {
   __typename?: 'SitePageContextIntlMessages',
  _404?: Maybe<Scalars['String']>,
  email?: Maybe<Scalars['String']>,
  forgotPassword?: Maybe<Scalars['String']>,
  home?: Maybe<Scalars['String']>,
  image?: Maybe<Scalars['String']>,
  login?: Maybe<Scalars['String']>,
  loginWelcomeMessage?: Maybe<Scalars['String']>,
  manageInventory?: Maybe<Scalars['String']>,
  manageItems?: Maybe<Scalars['String']>,
  name?: Maybe<Scalars['String']>,
  password?: Maybe<Scalars['String']>,
  price?: Maybe<Scalars['String']>,
  rememberMe?: Maybe<Scalars['String']>,
  requestEarlyAccess?: Maybe<Scalars['String']>,
  settings?: Maybe<Scalars['String']>,
  welcomeBack?: Maybe<Scalars['String']>,
};

export type SitePageContextIntlMessagesFilterInput = {
  _404?: Maybe<StringQueryOperatorInput>,
  email?: Maybe<StringQueryOperatorInput>,
  forgotPassword?: Maybe<StringQueryOperatorInput>,
  home?: Maybe<StringQueryOperatorInput>,
  image?: Maybe<StringQueryOperatorInput>,
  login?: Maybe<StringQueryOperatorInput>,
  loginWelcomeMessage?: Maybe<StringQueryOperatorInput>,
  manageInventory?: Maybe<StringQueryOperatorInput>,
  manageItems?: Maybe<StringQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  password?: Maybe<StringQueryOperatorInput>,
  price?: Maybe<StringQueryOperatorInput>,
  rememberMe?: Maybe<StringQueryOperatorInput>,
  requestEarlyAccess?: Maybe<StringQueryOperatorInput>,
  settings?: Maybe<StringQueryOperatorInput>,
  welcomeBack?: Maybe<StringQueryOperatorInput>,
};

export type SitePageEdge = {
   __typename?: 'SitePageEdge',
  next?: Maybe<SitePage>,
  node: SitePage,
  previous?: Maybe<SitePage>,
};

export enum SitePageFieldsEnum {
  children = 'children',
  children___children = 'children___children',
  children___children___children = 'children___children___children',
  children___children___children___children = 'children___children___children___children',
  children___children___children___id = 'children___children___children___id',
  children___children___id = 'children___children___id',
  children___children___internal___content = 'children___children___internal___content',
  children___children___internal___contentDigest = 'children___children___internal___contentDigest',
  children___children___internal___description = 'children___children___internal___description',
  children___children___internal___fieldOwners = 'children___children___internal___fieldOwners',
  children___children___internal___ignoreType = 'children___children___internal___ignoreType',
  children___children___internal___mediaType = 'children___children___internal___mediaType',
  children___children___internal___owner = 'children___children___internal___owner',
  children___children___internal___type = 'children___children___internal___type',
  children___children___parent___children = 'children___children___parent___children',
  children___children___parent___id = 'children___children___parent___id',
  children___id = 'children___id',
  children___internal___content = 'children___internal___content',
  children___internal___contentDigest = 'children___internal___contentDigest',
  children___internal___description = 'children___internal___description',
  children___internal___fieldOwners = 'children___internal___fieldOwners',
  children___internal___ignoreType = 'children___internal___ignoreType',
  children___internal___mediaType = 'children___internal___mediaType',
  children___internal___owner = 'children___internal___owner',
  children___internal___type = 'children___internal___type',
  children___parent___children = 'children___parent___children',
  children___parent___children___children = 'children___parent___children___children',
  children___parent___children___id = 'children___parent___children___id',
  children___parent___id = 'children___parent___id',
  children___parent___internal___content = 'children___parent___internal___content',
  children___parent___internal___contentDigest = 'children___parent___internal___contentDigest',
  children___parent___internal___description = 'children___parent___internal___description',
  children___parent___internal___fieldOwners = 'children___parent___internal___fieldOwners',
  children___parent___internal___ignoreType = 'children___parent___internal___ignoreType',
  children___parent___internal___mediaType = 'children___parent___internal___mediaType',
  children___parent___internal___owner = 'children___parent___internal___owner',
  children___parent___internal___type = 'children___parent___internal___type',
  children___parent___parent___children = 'children___parent___parent___children',
  children___parent___parent___id = 'children___parent___parent___id',
  component = 'component',
  componentChunkName = 'componentChunkName',
  componentPath = 'componentPath',
  context___intl___language = 'context___intl___language',
  context___intl___languages = 'context___intl___languages',
  context___intl___messages____404 = 'context___intl___messages____404',
  context___intl___messages___email = 'context___intl___messages___email',
  context___intl___messages___forgotPassword = 'context___intl___messages___forgotPassword',
  context___intl___messages___home = 'context___intl___messages___home',
  context___intl___messages___image = 'context___intl___messages___image',
  context___intl___messages___login = 'context___intl___messages___login',
  context___intl___messages___loginWelcomeMessage = 'context___intl___messages___loginWelcomeMessage',
  context___intl___messages___manageInventory = 'context___intl___messages___manageInventory',
  context___intl___messages___manageItems = 'context___intl___messages___manageItems',
  context___intl___messages___name = 'context___intl___messages___name',
  context___intl___messages___password = 'context___intl___messages___password',
  context___intl___messages___price = 'context___intl___messages___price',
  context___intl___messages___rememberMe = 'context___intl___messages___rememberMe',
  context___intl___messages___requestEarlyAccess = 'context___intl___messages___requestEarlyAccess',
  context___intl___messages___settings = 'context___intl___messages___settings',
  context___intl___messages___welcomeBack = 'context___intl___messages___welcomeBack',
  context___intl___originalPath = 'context___intl___originalPath',
  context___intl___redirect = 'context___intl___redirect',
  context___intl___routed = 'context___intl___routed',
  id = 'id',
  internalComponentName = 'internalComponentName',
  internal___content = 'internal___content',
  internal___contentDigest = 'internal___contentDigest',
  internal___description = 'internal___description',
  internal___fieldOwners = 'internal___fieldOwners',
  internal___ignoreType = 'internal___ignoreType',
  internal___mediaType = 'internal___mediaType',
  internal___owner = 'internal___owner',
  internal___type = 'internal___type',
  isCreatedByStatefulCreatePages = 'isCreatedByStatefulCreatePages',
  matchPath = 'matchPath',
  parent___children = 'parent___children',
  parent___children___children = 'parent___children___children',
  parent___children___children___children = 'parent___children___children___children',
  parent___children___children___id = 'parent___children___children___id',
  parent___children___id = 'parent___children___id',
  parent___children___internal___content = 'parent___children___internal___content',
  parent___children___internal___contentDigest = 'parent___children___internal___contentDigest',
  parent___children___internal___description = 'parent___children___internal___description',
  parent___children___internal___fieldOwners = 'parent___children___internal___fieldOwners',
  parent___children___internal___ignoreType = 'parent___children___internal___ignoreType',
  parent___children___internal___mediaType = 'parent___children___internal___mediaType',
  parent___children___internal___owner = 'parent___children___internal___owner',
  parent___children___internal___type = 'parent___children___internal___type',
  parent___children___parent___children = 'parent___children___parent___children',
  parent___children___parent___id = 'parent___children___parent___id',
  parent___id = 'parent___id',
  parent___internal___content = 'parent___internal___content',
  parent___internal___contentDigest = 'parent___internal___contentDigest',
  parent___internal___description = 'parent___internal___description',
  parent___internal___fieldOwners = 'parent___internal___fieldOwners',
  parent___internal___ignoreType = 'parent___internal___ignoreType',
  parent___internal___mediaType = 'parent___internal___mediaType',
  parent___internal___owner = 'parent___internal___owner',
  parent___internal___type = 'parent___internal___type',
  parent___parent___children = 'parent___parent___children',
  parent___parent___children___children = 'parent___parent___children___children',
  parent___parent___children___id = 'parent___parent___children___id',
  parent___parent___id = 'parent___parent___id',
  parent___parent___internal___content = 'parent___parent___internal___content',
  parent___parent___internal___contentDigest = 'parent___parent___internal___contentDigest',
  parent___parent___internal___description = 'parent___parent___internal___description',
  parent___parent___internal___fieldOwners = 'parent___parent___internal___fieldOwners',
  parent___parent___internal___ignoreType = 'parent___parent___internal___ignoreType',
  parent___parent___internal___mediaType = 'parent___parent___internal___mediaType',
  parent___parent___internal___owner = 'parent___parent___internal___owner',
  parent___parent___internal___type = 'parent___parent___internal___type',
  parent___parent___parent___children = 'parent___parent___parent___children',
  parent___parent___parent___id = 'parent___parent___parent___id',
  path = 'path',
  pluginCreatorId = 'pluginCreatorId',
  pluginCreator___browserAPIs = 'pluginCreator___browserAPIs',
  pluginCreator___children = 'pluginCreator___children',
  pluginCreator___children___children = 'pluginCreator___children___children',
  pluginCreator___children___children___children = 'pluginCreator___children___children___children',
  pluginCreator___children___children___id = 'pluginCreator___children___children___id',
  pluginCreator___children___id = 'pluginCreator___children___id',
  pluginCreator___children___internal___content = 'pluginCreator___children___internal___content',
  pluginCreator___children___internal___contentDigest = 'pluginCreator___children___internal___contentDigest',
  pluginCreator___children___internal___description = 'pluginCreator___children___internal___description',
  pluginCreator___children___internal___fieldOwners = 'pluginCreator___children___internal___fieldOwners',
  pluginCreator___children___internal___ignoreType = 'pluginCreator___children___internal___ignoreType',
  pluginCreator___children___internal___mediaType = 'pluginCreator___children___internal___mediaType',
  pluginCreator___children___internal___owner = 'pluginCreator___children___internal___owner',
  pluginCreator___children___internal___type = 'pluginCreator___children___internal___type',
  pluginCreator___children___parent___children = 'pluginCreator___children___parent___children',
  pluginCreator___children___parent___id = 'pluginCreator___children___parent___id',
  pluginCreator___id = 'pluginCreator___id',
  pluginCreator___internal___content = 'pluginCreator___internal___content',
  pluginCreator___internal___contentDigest = 'pluginCreator___internal___contentDigest',
  pluginCreator___internal___description = 'pluginCreator___internal___description',
  pluginCreator___internal___fieldOwners = 'pluginCreator___internal___fieldOwners',
  pluginCreator___internal___ignoreType = 'pluginCreator___internal___ignoreType',
  pluginCreator___internal___mediaType = 'pluginCreator___internal___mediaType',
  pluginCreator___internal___owner = 'pluginCreator___internal___owner',
  pluginCreator___internal___type = 'pluginCreator___internal___type',
  pluginCreator___name = 'pluginCreator___name',
  pluginCreator___nodeAPIs = 'pluginCreator___nodeAPIs',
  pluginCreator___packageJson___dependencies = 'pluginCreator___packageJson___dependencies',
  pluginCreator___packageJson___dependencies___name = 'pluginCreator___packageJson___dependencies___name',
  pluginCreator___packageJson___dependencies___version = 'pluginCreator___packageJson___dependencies___version',
  pluginCreator___packageJson___description = 'pluginCreator___packageJson___description',
  pluginCreator___packageJson___devDependencies = 'pluginCreator___packageJson___devDependencies',
  pluginCreator___packageJson___devDependencies___name = 'pluginCreator___packageJson___devDependencies___name',
  pluginCreator___packageJson___devDependencies___version = 'pluginCreator___packageJson___devDependencies___version',
  pluginCreator___packageJson___keywords = 'pluginCreator___packageJson___keywords',
  pluginCreator___packageJson___license = 'pluginCreator___packageJson___license',
  pluginCreator___packageJson___main = 'pluginCreator___packageJson___main',
  pluginCreator___packageJson___name = 'pluginCreator___packageJson___name',
  pluginCreator___packageJson___peerDependencies = 'pluginCreator___packageJson___peerDependencies',
  pluginCreator___packageJson___peerDependencies___name = 'pluginCreator___packageJson___peerDependencies___name',
  pluginCreator___packageJson___peerDependencies___version = 'pluginCreator___packageJson___peerDependencies___version',
  pluginCreator___packageJson___version = 'pluginCreator___packageJson___version',
  pluginCreator___parent___children = 'pluginCreator___parent___children',
  pluginCreator___parent___children___children = 'pluginCreator___parent___children___children',
  pluginCreator___parent___children___id = 'pluginCreator___parent___children___id',
  pluginCreator___parent___id = 'pluginCreator___parent___id',
  pluginCreator___parent___internal___content = 'pluginCreator___parent___internal___content',
  pluginCreator___parent___internal___contentDigest = 'pluginCreator___parent___internal___contentDigest',
  pluginCreator___parent___internal___description = 'pluginCreator___parent___internal___description',
  pluginCreator___parent___internal___fieldOwners = 'pluginCreator___parent___internal___fieldOwners',
  pluginCreator___parent___internal___ignoreType = 'pluginCreator___parent___internal___ignoreType',
  pluginCreator___parent___internal___mediaType = 'pluginCreator___parent___internal___mediaType',
  pluginCreator___parent___internal___owner = 'pluginCreator___parent___internal___owner',
  pluginCreator___parent___internal___type = 'pluginCreator___parent___internal___type',
  pluginCreator___parent___parent___children = 'pluginCreator___parent___parent___children',
  pluginCreator___parent___parent___id = 'pluginCreator___parent___parent___id',
  pluginCreator___pluginFilepath = 'pluginCreator___pluginFilepath',
  pluginCreator___pluginOptions___analyzerPort = 'pluginCreator___pluginOptions___analyzerPort',
  pluginCreator___pluginOptions___background_color = 'pluginCreator___pluginOptions___background_color',
  pluginCreator___pluginOptions___color = 'pluginCreator___pluginOptions___color',
  pluginCreator___pluginOptions___defaultLanguage = 'pluginCreator___pluginOptions___defaultLanguage',
  pluginCreator___pluginOptions___description = 'pluginCreator___pluginOptions___description',
  pluginCreator___pluginOptions___devKey = 'pluginCreator___pluginOptions___devKey',
  pluginCreator___pluginOptions___disable = 'pluginCreator___pluginOptions___disable',
  pluginCreator___pluginOptions___display = 'pluginCreator___pluginOptions___display',
  pluginCreator___pluginOptions___fonts = 'pluginCreator___pluginOptions___fonts',
  pluginCreator___pluginOptions___fonts___family = 'pluginCreator___pluginOptions___fonts___family',
  pluginCreator___pluginOptions___fonts___variants = 'pluginCreator___pluginOptions___fonts___variants',
  pluginCreator___pluginOptions___icon = 'pluginCreator___pluginOptions___icon',
  pluginCreator___pluginOptions___lang = 'pluginCreator___pluginOptions___lang',
  pluginCreator___pluginOptions___languages = 'pluginCreator___pluginOptions___languages',
  pluginCreator___pluginOptions___localize = 'pluginCreator___pluginOptions___localize',
  pluginCreator___pluginOptions___localize___description = 'pluginCreator___pluginOptions___localize___description',
  pluginCreator___pluginOptions___localize___lang = 'pluginCreator___pluginOptions___localize___lang',
  pluginCreator___pluginOptions___localize___name = 'pluginCreator___pluginOptions___localize___name',
  pluginCreator___pluginOptions___localize___short_name = 'pluginCreator___pluginOptions___localize___short_name',
  pluginCreator___pluginOptions___localize___start_url = 'pluginCreator___pluginOptions___localize___start_url',
  pluginCreator___pluginOptions___name = 'pluginCreator___pluginOptions___name',
  pluginCreator___pluginOptions___path = 'pluginCreator___pluginOptions___path',
  pluginCreator___pluginOptions___pathCheck = 'pluginCreator___pluginOptions___pathCheck',
  pluginCreator___pluginOptions___prettier = 'pluginCreator___pluginOptions___prettier',
  pluginCreator___pluginOptions___prodKey = 'pluginCreator___pluginOptions___prodKey',
  pluginCreator___pluginOptions___production = 'pluginCreator___pluginOptions___production',
  pluginCreator___pluginOptions___redirect = 'pluginCreator___pluginOptions___redirect',
  pluginCreator___pluginOptions___short_name = 'pluginCreator___pluginOptions___short_name',
  pluginCreator___pluginOptions___showSpinner = 'pluginCreator___pluginOptions___showSpinner',
  pluginCreator___pluginOptions___siteUrl = 'pluginCreator___pluginOptions___siteUrl',
  pluginCreator___pluginOptions___start_url = 'pluginCreator___pluginOptions___start_url',
  pluginCreator___pluginOptions___svgo = 'pluginCreator___pluginOptions___svgo',
  pluginCreator___pluginOptions___svgoConfig___cleanupIDs = 'pluginCreator___pluginOptions___svgoConfig___cleanupIDs',
  pluginCreator___pluginOptions___svgoConfig___removeViewBox = 'pluginCreator___pluginOptions___svgoConfig___removeViewBox',
  pluginCreator___pluginOptions___theme_color = 'pluginCreator___pluginOptions___theme_color',
  pluginCreator___pluginOptions___trackPage = 'pluginCreator___pluginOptions___trackPage',
  pluginCreator___resolve = 'pluginCreator___resolve',
  pluginCreator___ssrAPIs = 'pluginCreator___ssrAPIs',
  pluginCreator___version = 'pluginCreator___version'
}

export type SitePageFilterInput = {
  children?: Maybe<NodeFilterListInput>,
  component?: Maybe<StringQueryOperatorInput>,
  componentChunkName?: Maybe<StringQueryOperatorInput>,
  componentPath?: Maybe<StringQueryOperatorInput>,
  context?: Maybe<SitePageContextFilterInput>,
  id?: Maybe<StringQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  internalComponentName?: Maybe<StringQueryOperatorInput>,
  isCreatedByStatefulCreatePages?: Maybe<BooleanQueryOperatorInput>,
  matchPath?: Maybe<StringQueryOperatorInput>,
  parent?: Maybe<NodeFilterInput>,
  path?: Maybe<StringQueryOperatorInput>,
  pluginCreator?: Maybe<SitePluginFilterInput>,
  pluginCreatorId?: Maybe<StringQueryOperatorInput>,
};

export type SitePageGroupConnection = {
   __typename?: 'SitePageGroupConnection',
  edges: Array<SitePageEdge>,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
  nodes: Array<SitePage>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};

export type SitePageSortInput = {
  fields?: Maybe<Array<Maybe<SitePageFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type SitePlugin = Node & {
   __typename?: 'SitePlugin',
  browserAPIs?: Maybe<Array<Maybe<Scalars['String']>>>,
  children: Array<Node>,
  id: Scalars['ID'],
  internal: Internal,
  name?: Maybe<Scalars['String']>,
  nodeAPIs?: Maybe<Array<Maybe<Scalars['String']>>>,
  packageJson?: Maybe<SitePluginPackageJson>,
  parent?: Maybe<Node>,
  pluginFilepath?: Maybe<Scalars['String']>,
  pluginOptions?: Maybe<SitePluginPluginOptions>,
  resolve?: Maybe<Scalars['String']>,
  ssrAPIs?: Maybe<Array<Maybe<Scalars['String']>>>,
  version?: Maybe<Scalars['String']>,
};

export type SitePluginConnection = {
   __typename?: 'SitePluginConnection',
  distinct: Array<Scalars['String']>,
  edges: Array<SitePluginEdge>,
  group: Array<SitePluginGroupConnection>,
  nodes: Array<SitePlugin>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};


export type SitePluginConnectionDistinctArgs = {
  field: SitePluginFieldsEnum
};


export type SitePluginConnectionGroupArgs = {
  field: SitePluginFieldsEnum,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>
};

export type SitePluginEdge = {
   __typename?: 'SitePluginEdge',
  next?: Maybe<SitePlugin>,
  node: SitePlugin,
  previous?: Maybe<SitePlugin>,
};

export enum SitePluginFieldsEnum {
  browserAPIs = 'browserAPIs',
  children = 'children',
  children___children = 'children___children',
  children___children___children = 'children___children___children',
  children___children___children___children = 'children___children___children___children',
  children___children___children___id = 'children___children___children___id',
  children___children___id = 'children___children___id',
  children___children___internal___content = 'children___children___internal___content',
  children___children___internal___contentDigest = 'children___children___internal___contentDigest',
  children___children___internal___description = 'children___children___internal___description',
  children___children___internal___fieldOwners = 'children___children___internal___fieldOwners',
  children___children___internal___ignoreType = 'children___children___internal___ignoreType',
  children___children___internal___mediaType = 'children___children___internal___mediaType',
  children___children___internal___owner = 'children___children___internal___owner',
  children___children___internal___type = 'children___children___internal___type',
  children___children___parent___children = 'children___children___parent___children',
  children___children___parent___id = 'children___children___parent___id',
  children___id = 'children___id',
  children___internal___content = 'children___internal___content',
  children___internal___contentDigest = 'children___internal___contentDigest',
  children___internal___description = 'children___internal___description',
  children___internal___fieldOwners = 'children___internal___fieldOwners',
  children___internal___ignoreType = 'children___internal___ignoreType',
  children___internal___mediaType = 'children___internal___mediaType',
  children___internal___owner = 'children___internal___owner',
  children___internal___type = 'children___internal___type',
  children___parent___children = 'children___parent___children',
  children___parent___children___children = 'children___parent___children___children',
  children___parent___children___id = 'children___parent___children___id',
  children___parent___id = 'children___parent___id',
  children___parent___internal___content = 'children___parent___internal___content',
  children___parent___internal___contentDigest = 'children___parent___internal___contentDigest',
  children___parent___internal___description = 'children___parent___internal___description',
  children___parent___internal___fieldOwners = 'children___parent___internal___fieldOwners',
  children___parent___internal___ignoreType = 'children___parent___internal___ignoreType',
  children___parent___internal___mediaType = 'children___parent___internal___mediaType',
  children___parent___internal___owner = 'children___parent___internal___owner',
  children___parent___internal___type = 'children___parent___internal___type',
  children___parent___parent___children = 'children___parent___parent___children',
  children___parent___parent___id = 'children___parent___parent___id',
  id = 'id',
  internal___content = 'internal___content',
  internal___contentDigest = 'internal___contentDigest',
  internal___description = 'internal___description',
  internal___fieldOwners = 'internal___fieldOwners',
  internal___ignoreType = 'internal___ignoreType',
  internal___mediaType = 'internal___mediaType',
  internal___owner = 'internal___owner',
  internal___type = 'internal___type',
  name = 'name',
  nodeAPIs = 'nodeAPIs',
  packageJson___dependencies = 'packageJson___dependencies',
  packageJson___dependencies___name = 'packageJson___dependencies___name',
  packageJson___dependencies___version = 'packageJson___dependencies___version',
  packageJson___description = 'packageJson___description',
  packageJson___devDependencies = 'packageJson___devDependencies',
  packageJson___devDependencies___name = 'packageJson___devDependencies___name',
  packageJson___devDependencies___version = 'packageJson___devDependencies___version',
  packageJson___keywords = 'packageJson___keywords',
  packageJson___license = 'packageJson___license',
  packageJson___main = 'packageJson___main',
  packageJson___name = 'packageJson___name',
  packageJson___peerDependencies = 'packageJson___peerDependencies',
  packageJson___peerDependencies___name = 'packageJson___peerDependencies___name',
  packageJson___peerDependencies___version = 'packageJson___peerDependencies___version',
  packageJson___version = 'packageJson___version',
  parent___children = 'parent___children',
  parent___children___children = 'parent___children___children',
  parent___children___children___children = 'parent___children___children___children',
  parent___children___children___id = 'parent___children___children___id',
  parent___children___id = 'parent___children___id',
  parent___children___internal___content = 'parent___children___internal___content',
  parent___children___internal___contentDigest = 'parent___children___internal___contentDigest',
  parent___children___internal___description = 'parent___children___internal___description',
  parent___children___internal___fieldOwners = 'parent___children___internal___fieldOwners',
  parent___children___internal___ignoreType = 'parent___children___internal___ignoreType',
  parent___children___internal___mediaType = 'parent___children___internal___mediaType',
  parent___children___internal___owner = 'parent___children___internal___owner',
  parent___children___internal___type = 'parent___children___internal___type',
  parent___children___parent___children = 'parent___children___parent___children',
  parent___children___parent___id = 'parent___children___parent___id',
  parent___id = 'parent___id',
  parent___internal___content = 'parent___internal___content',
  parent___internal___contentDigest = 'parent___internal___contentDigest',
  parent___internal___description = 'parent___internal___description',
  parent___internal___fieldOwners = 'parent___internal___fieldOwners',
  parent___internal___ignoreType = 'parent___internal___ignoreType',
  parent___internal___mediaType = 'parent___internal___mediaType',
  parent___internal___owner = 'parent___internal___owner',
  parent___internal___type = 'parent___internal___type',
  parent___parent___children = 'parent___parent___children',
  parent___parent___children___children = 'parent___parent___children___children',
  parent___parent___children___id = 'parent___parent___children___id',
  parent___parent___id = 'parent___parent___id',
  parent___parent___internal___content = 'parent___parent___internal___content',
  parent___parent___internal___contentDigest = 'parent___parent___internal___contentDigest',
  parent___parent___internal___description = 'parent___parent___internal___description',
  parent___parent___internal___fieldOwners = 'parent___parent___internal___fieldOwners',
  parent___parent___internal___ignoreType = 'parent___parent___internal___ignoreType',
  parent___parent___internal___mediaType = 'parent___parent___internal___mediaType',
  parent___parent___internal___owner = 'parent___parent___internal___owner',
  parent___parent___internal___type = 'parent___parent___internal___type',
  parent___parent___parent___children = 'parent___parent___parent___children',
  parent___parent___parent___id = 'parent___parent___parent___id',
  pluginFilepath = 'pluginFilepath',
  pluginOptions___analyzerPort = 'pluginOptions___analyzerPort',
  pluginOptions___background_color = 'pluginOptions___background_color',
  pluginOptions___color = 'pluginOptions___color',
  pluginOptions___defaultLanguage = 'pluginOptions___defaultLanguage',
  pluginOptions___description = 'pluginOptions___description',
  pluginOptions___devKey = 'pluginOptions___devKey',
  pluginOptions___disable = 'pluginOptions___disable',
  pluginOptions___display = 'pluginOptions___display',
  pluginOptions___fonts = 'pluginOptions___fonts',
  pluginOptions___fonts___family = 'pluginOptions___fonts___family',
  pluginOptions___fonts___variants = 'pluginOptions___fonts___variants',
  pluginOptions___icon = 'pluginOptions___icon',
  pluginOptions___lang = 'pluginOptions___lang',
  pluginOptions___languages = 'pluginOptions___languages',
  pluginOptions___localize = 'pluginOptions___localize',
  pluginOptions___localize___description = 'pluginOptions___localize___description',
  pluginOptions___localize___lang = 'pluginOptions___localize___lang',
  pluginOptions___localize___name = 'pluginOptions___localize___name',
  pluginOptions___localize___short_name = 'pluginOptions___localize___short_name',
  pluginOptions___localize___start_url = 'pluginOptions___localize___start_url',
  pluginOptions___name = 'pluginOptions___name',
  pluginOptions___path = 'pluginOptions___path',
  pluginOptions___pathCheck = 'pluginOptions___pathCheck',
  pluginOptions___prettier = 'pluginOptions___prettier',
  pluginOptions___prodKey = 'pluginOptions___prodKey',
  pluginOptions___production = 'pluginOptions___production',
  pluginOptions___redirect = 'pluginOptions___redirect',
  pluginOptions___short_name = 'pluginOptions___short_name',
  pluginOptions___showSpinner = 'pluginOptions___showSpinner',
  pluginOptions___siteUrl = 'pluginOptions___siteUrl',
  pluginOptions___start_url = 'pluginOptions___start_url',
  pluginOptions___svgo = 'pluginOptions___svgo',
  pluginOptions___svgoConfig___cleanupIDs = 'pluginOptions___svgoConfig___cleanupIDs',
  pluginOptions___svgoConfig___removeViewBox = 'pluginOptions___svgoConfig___removeViewBox',
  pluginOptions___theme_color = 'pluginOptions___theme_color',
  pluginOptions___trackPage = 'pluginOptions___trackPage',
  resolve = 'resolve',
  ssrAPIs = 'ssrAPIs',
  version = 'version'
}

export type SitePluginFilterInput = {
  browserAPIs?: Maybe<StringQueryOperatorInput>,
  children?: Maybe<NodeFilterListInput>,
  id?: Maybe<StringQueryOperatorInput>,
  internal?: Maybe<InternalFilterInput>,
  name?: Maybe<StringQueryOperatorInput>,
  nodeAPIs?: Maybe<StringQueryOperatorInput>,
  packageJson?: Maybe<SitePluginPackageJsonFilterInput>,
  parent?: Maybe<NodeFilterInput>,
  pluginFilepath?: Maybe<StringQueryOperatorInput>,
  pluginOptions?: Maybe<SitePluginPluginOptionsFilterInput>,
  resolve?: Maybe<StringQueryOperatorInput>,
  ssrAPIs?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginGroupConnection = {
   __typename?: 'SitePluginGroupConnection',
  edges: Array<SitePluginEdge>,
  field: Scalars['String'],
  fieldValue?: Maybe<Scalars['String']>,
  nodes: Array<SitePlugin>,
  pageInfo: PageInfo,
  totalCount: Scalars['Int'],
};

export type SitePluginPackageJson = {
   __typename?: 'SitePluginPackageJson',
  dependencies?: Maybe<Array<Maybe<SitePluginPackageJsonDependencies>>>,
  description?: Maybe<Scalars['String']>,
  devDependencies?: Maybe<Array<Maybe<SitePluginPackageJsonDevDependencies>>>,
  keywords?: Maybe<Array<Maybe<Scalars['String']>>>,
  license?: Maybe<Scalars['String']>,
  main?: Maybe<Scalars['String']>,
  name?: Maybe<Scalars['String']>,
  peerDependencies?: Maybe<Array<Maybe<SitePluginPackageJsonPeerDependencies>>>,
  version?: Maybe<Scalars['String']>,
};

export type SitePluginPackageJsonDependencies = {
   __typename?: 'SitePluginPackageJsonDependencies',
  name?: Maybe<Scalars['String']>,
  version?: Maybe<Scalars['String']>,
};

export type SitePluginPackageJsonDependenciesFilterInput = {
  name?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPackageJsonDependenciesFilterListInput = {
  elemMatch?: Maybe<SitePluginPackageJsonDependenciesFilterInput>,
};

export type SitePluginPackageJsonDevDependencies = {
   __typename?: 'SitePluginPackageJsonDevDependencies',
  name?: Maybe<Scalars['String']>,
  version?: Maybe<Scalars['String']>,
};

export type SitePluginPackageJsonDevDependenciesFilterInput = {
  name?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPackageJsonDevDependenciesFilterListInput = {
  elemMatch?: Maybe<SitePluginPackageJsonDevDependenciesFilterInput>,
};

export type SitePluginPackageJsonFilterInput = {
  dependencies?: Maybe<SitePluginPackageJsonDependenciesFilterListInput>,
  description?: Maybe<StringQueryOperatorInput>,
  devDependencies?: Maybe<SitePluginPackageJsonDevDependenciesFilterListInput>,
  keywords?: Maybe<StringQueryOperatorInput>,
  license?: Maybe<StringQueryOperatorInput>,
  main?: Maybe<StringQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  peerDependencies?: Maybe<SitePluginPackageJsonPeerDependenciesFilterListInput>,
  version?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPackageJsonPeerDependencies = {
   __typename?: 'SitePluginPackageJsonPeerDependencies',
  name?: Maybe<Scalars['String']>,
  version?: Maybe<Scalars['String']>,
};

export type SitePluginPackageJsonPeerDependenciesFilterInput = {
  name?: Maybe<StringQueryOperatorInput>,
  version?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPackageJsonPeerDependenciesFilterListInput = {
  elemMatch?: Maybe<SitePluginPackageJsonPeerDependenciesFilterInput>,
};

export type SitePluginPluginOptions = {
   __typename?: 'SitePluginPluginOptions',
  analyzerPort?: Maybe<Scalars['Int']>,
  background_color?: Maybe<Scalars['String']>,
  color?: Maybe<Scalars['String']>,
  defaultLanguage?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  devKey?: Maybe<Scalars['String']>,
  disable?: Maybe<Scalars['Boolean']>,
  display?: Maybe<Scalars['String']>,
  fonts?: Maybe<Array<Maybe<SitePluginPluginOptionsFonts>>>,
  icon?: Maybe<Scalars['String']>,
  lang?: Maybe<Scalars['String']>,
  languages?: Maybe<Array<Maybe<Scalars['String']>>>,
  localize?: Maybe<Array<Maybe<SitePluginPluginOptionsLocalize>>>,
  name?: Maybe<Scalars['String']>,
  path?: Maybe<Scalars['String']>,
  pathCheck?: Maybe<Scalars['Boolean']>,
  prettier?: Maybe<Scalars['Boolean']>,
  prodKey?: Maybe<Scalars['String']>,
  production?: Maybe<Scalars['Boolean']>,
  redirect?: Maybe<Scalars['Boolean']>,
  short_name?: Maybe<Scalars['String']>,
  showSpinner?: Maybe<Scalars['Boolean']>,
  siteUrl?: Maybe<Scalars['String']>,
  start_url?: Maybe<Scalars['String']>,
  svgo?: Maybe<Scalars['Boolean']>,
  svgoConfig?: Maybe<SitePluginPluginOptionsSvgoConfig>,
  theme_color?: Maybe<Scalars['String']>,
  trackPage?: Maybe<Scalars['Boolean']>,
};

export type SitePluginPluginOptionsFilterInput = {
  analyzerPort?: Maybe<IntQueryOperatorInput>,
  background_color?: Maybe<StringQueryOperatorInput>,
  color?: Maybe<StringQueryOperatorInput>,
  defaultLanguage?: Maybe<StringQueryOperatorInput>,
  description?: Maybe<StringQueryOperatorInput>,
  devKey?: Maybe<StringQueryOperatorInput>,
  disable?: Maybe<BooleanQueryOperatorInput>,
  display?: Maybe<StringQueryOperatorInput>,
  fonts?: Maybe<SitePluginPluginOptionsFontsFilterListInput>,
  icon?: Maybe<StringQueryOperatorInput>,
  lang?: Maybe<StringQueryOperatorInput>,
  languages?: Maybe<StringQueryOperatorInput>,
  localize?: Maybe<SitePluginPluginOptionsLocalizeFilterListInput>,
  name?: Maybe<StringQueryOperatorInput>,
  path?: Maybe<StringQueryOperatorInput>,
  pathCheck?: Maybe<BooleanQueryOperatorInput>,
  prettier?: Maybe<BooleanQueryOperatorInput>,
  prodKey?: Maybe<StringQueryOperatorInput>,
  production?: Maybe<BooleanQueryOperatorInput>,
  redirect?: Maybe<BooleanQueryOperatorInput>,
  short_name?: Maybe<StringQueryOperatorInput>,
  showSpinner?: Maybe<BooleanQueryOperatorInput>,
  siteUrl?: Maybe<StringQueryOperatorInput>,
  start_url?: Maybe<StringQueryOperatorInput>,
  svgo?: Maybe<BooleanQueryOperatorInput>,
  svgoConfig?: Maybe<SitePluginPluginOptionsSvgoConfigFilterInput>,
  theme_color?: Maybe<StringQueryOperatorInput>,
  trackPage?: Maybe<BooleanQueryOperatorInput>,
};

export type SitePluginPluginOptionsFonts = {
   __typename?: 'SitePluginPluginOptionsFonts',
  family?: Maybe<Scalars['String']>,
  variants?: Maybe<Array<Maybe<Scalars['String']>>>,
};

export type SitePluginPluginOptionsFontsFilterInput = {
  family?: Maybe<StringQueryOperatorInput>,
  variants?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPluginOptionsFontsFilterListInput = {
  elemMatch?: Maybe<SitePluginPluginOptionsFontsFilterInput>,
};

export type SitePluginPluginOptionsLocalize = {
   __typename?: 'SitePluginPluginOptionsLocalize',
  description?: Maybe<Scalars['String']>,
  lang?: Maybe<Scalars['String']>,
  name?: Maybe<Scalars['String']>,
  short_name?: Maybe<Scalars['String']>,
  start_url?: Maybe<Scalars['String']>,
};

export type SitePluginPluginOptionsLocalizeFilterInput = {
  description?: Maybe<StringQueryOperatorInput>,
  lang?: Maybe<StringQueryOperatorInput>,
  name?: Maybe<StringQueryOperatorInput>,
  short_name?: Maybe<StringQueryOperatorInput>,
  start_url?: Maybe<StringQueryOperatorInput>,
};

export type SitePluginPluginOptionsLocalizeFilterListInput = {
  elemMatch?: Maybe<SitePluginPluginOptionsLocalizeFilterInput>,
};

export type SitePluginPluginOptionsSvgoConfig = {
   __typename?: 'SitePluginPluginOptionsSvgoConfig',
  cleanupIDs?: Maybe<Scalars['Boolean']>,
  removeViewBox?: Maybe<Scalars['Boolean']>,
};

export type SitePluginPluginOptionsSvgoConfigFilterInput = {
  cleanupIDs?: Maybe<BooleanQueryOperatorInput>,
  removeViewBox?: Maybe<BooleanQueryOperatorInput>,
};

export type SitePluginSortInput = {
  fields?: Maybe<Array<Maybe<SitePluginFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export type SiteSiteMetadata = {
   __typename?: 'SiteSiteMetadata',
  author?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  siteUrl?: Maybe<Scalars['String']>,
  title?: Maybe<Scalars['String']>,
};

export type SiteSiteMetadataFilterInput = {
  author?: Maybe<StringQueryOperatorInput>,
  description?: Maybe<StringQueryOperatorInput>,
  siteUrl?: Maybe<StringQueryOperatorInput>,
  title?: Maybe<StringQueryOperatorInput>,
};

export type SiteSortInput = {
  fields?: Maybe<Array<Maybe<SiteFieldsEnum>>>,
  order?: Maybe<Array<Maybe<SortOrderEnum>>>,
};

export enum SortOrderEnum {
  ASC = 'ASC',
  DESC = 'DESC'
}

export type StringQueryOperatorInput = {
  eq?: Maybe<Scalars['String']>,
  glob?: Maybe<Scalars['String']>,
  in?: Maybe<Array<Maybe<Scalars['String']>>>,
  ne?: Maybe<Scalars['String']>,
  nin?: Maybe<Array<Maybe<Scalars['String']>>>,
  regex?: Maybe<Scalars['String']>,
};

export type GatsbyImageSharpFixedFragment = (
  { __typename?: 'ImageSharpFixed' }
  & Pick<ImageSharpFixed, 'base64' | 'width' | 'height' | 'src' | 'srcSet'>
);

export type GatsbyImageSharpFixed_TracedSvgFragment = (
  { __typename?: 'ImageSharpFixed' }
  & Pick<ImageSharpFixed, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet'>
);

export type GatsbyImageSharpFixed_WithWebpFragment = (
  { __typename?: 'ImageSharpFixed' }
  & Pick<ImageSharpFixed, 'base64' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>
);

export type GatsbyImageSharpFixed_WithWebp_TracedSvgFragment = (
  { __typename?: 'ImageSharpFixed' }
  & Pick<ImageSharpFixed, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>
);

export type GatsbyImageSharpFixed_NoBase64Fragment = (
  { __typename?: 'ImageSharpFixed' }
  & Pick<ImageSharpFixed, 'width' | 'height' | 'src' | 'srcSet'>
);

export type GatsbyImageSharpFixed_WithWebp_NoBase64Fragment = (
  { __typename?: 'ImageSharpFixed' }
  & Pick<ImageSharpFixed, 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>
);

export type GatsbyImageSharpFluidFragment = (
  { __typename?: 'ImageSharpFluid' }
  & Pick<ImageSharpFluid, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>
);

export type GatsbyImageSharpFluid_TracedSvgFragment = (
  { __typename?: 'ImageSharpFluid' }
  & Pick<ImageSharpFluid, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>
);

export type GatsbyImageSharpFluid_WithWebpFragment = (
  { __typename?: 'ImageSharpFluid' }
  & Pick<ImageSharpFluid, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>
);

export type GatsbyImageSharpFluid_WithWebp_TracedSvgFragment = (
  { __typename?: 'ImageSharpFluid' }
  & Pick<ImageSharpFluid, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>
);

export type GatsbyImageSharpFluid_NoBase64Fragment = (
  { __typename?: 'ImageSharpFluid' }
  & Pick<ImageSharpFluid, 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>
);

export type GatsbyImageSharpFluid_WithWebp_NoBase64Fragment = (
  { __typename?: 'ImageSharpFluid' }
  & Pick<ImageSharpFluid, 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>
);

export type GatsbyImageSharpResolutionsFragment = (
  { __typename?: 'ImageSharpResolutions' }
  & Pick<ImageSharpResolutions, 'base64' | 'width' | 'height' | 'src' | 'srcSet'>
);

export type GatsbyImageSharpResolutions_TracedSvgFragment = (
  { __typename?: 'ImageSharpResolutions' }
  & Pick<ImageSharpResolutions, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet'>
);

export type GatsbyImageSharpResolutions_WithWebpFragment = (
  { __typename?: 'ImageSharpResolutions' }
  & Pick<ImageSharpResolutions, 'base64' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>
);

export type GatsbyImageSharpResolutions_WithWebp_TracedSvgFragment = (
  { __typename?: 'ImageSharpResolutions' }
  & Pick<ImageSharpResolutions, 'tracedSVG' | 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>
);

export type GatsbyImageSharpResolutions_NoBase64Fragment = (
  { __typename?: 'ImageSharpResolutions' }
  & Pick<ImageSharpResolutions, 'width' | 'height' | 'src' | 'srcSet'>
);

export type GatsbyImageSharpResolutions_WithWebp_NoBase64Fragment = (
  { __typename?: 'ImageSharpResolutions' }
  & Pick<ImageSharpResolutions, 'width' | 'height' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp'>
);

export type GatsbyImageSharpSizesFragment = (
  { __typename?: 'ImageSharpSizes' }
  & Pick<ImageSharpSizes, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>
);

export type GatsbyImageSharpSizes_TracedSvgFragment = (
  { __typename?: 'ImageSharpSizes' }
  & Pick<ImageSharpSizes, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>
);

export type GatsbyImageSharpSizes_WithWebpFragment = (
  { __typename?: 'ImageSharpSizes' }
  & Pick<ImageSharpSizes, 'base64' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>
);

export type GatsbyImageSharpSizes_WithWebp_TracedSvgFragment = (
  { __typename?: 'ImageSharpSizes' }
  & Pick<ImageSharpSizes, 'tracedSVG' | 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>
);

export type GatsbyImageSharpSizes_NoBase64Fragment = (
  { __typename?: 'ImageSharpSizes' }
  & Pick<ImageSharpSizes, 'aspectRatio' | 'src' | 'srcSet' | 'sizes'>
);

export type GatsbyImageSharpSizes_WithWebp_NoBase64Fragment = (
  { __typename?: 'ImageSharpSizes' }
  & Pick<ImageSharpSizes, 'aspectRatio' | 'src' | 'srcSet' | 'srcWebp' | 'srcSetWebp' | 'sizes'>
);

export type AvatarPlaceholderQueryVariables = {};


export type AvatarPlaceholderQuery = (
  { __typename?: 'Query' }
  & { file: Maybe<(
    { __typename?: 'File' }
    & { childImageSharp: Maybe<(
      { __typename?: 'ImageSharp' }
      & { fixed: Maybe<(
        { __typename?: 'ImageSharpFixed' }
        & GatsbyImageSharpFixed_WithWebpFragment
      )> }
    )> }
  )> }
);

export type EmptyInventoryImageQueryVariables = {};


export type EmptyInventoryImageQuery = (
  { __typename?: 'Query' }
  & { file: Maybe<(
    { __typename?: 'File' }
    & { childImageSharp: Maybe<(
      { __typename?: 'ImageSharp' }
      & { fluid: Maybe<(
        { __typename?: 'ImageSharpFluid' }
        & GatsbyImageSharpFluid_WithWebp_TracedSvgFragment
      )> }
    )> }
  )> }
);

export type NoItemImageQueryQueryVariables = {};


export type NoItemImageQueryQuery = (
  { __typename?: 'Query' }
  & { file: Maybe<(
    { __typename?: 'File' }
    & { childImageSharp: Maybe<(
      { __typename?: 'ImageSharp' }
      & { fluid: Maybe<(
        { __typename?: 'ImageSharpFluid' }
        & GatsbyImageSharpFluid_WithWebp_TracedSvgFragment
      )> }
    )> }
  )> }
);
