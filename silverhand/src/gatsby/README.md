# What is this folder?

This folder contains functions that are specific for gatsby. This is the only folder where we could
write graphql queries to gatsby. This is done because we use apollo and gatsby. Both of these
tools have it's own graphql endpoint with different schema and it's own grapql query function
(`graphql()` and `useStaticQuery` for gatsby, `gql` and `useQuery` for apollo).

To avoid confusion, and to make our editor works correctly for both graphql use cases, Gatsby's graphql
query will be limited to this folder only.

Since apollo queries will be more complex, used more frequently, and more relevant to each components needs,
it will be done directly on the component fie.
