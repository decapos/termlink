# Silverhand

Deca one and only dashboard SPA. Below are the guidelines for the development of this project.

## Tech Stack and tooling

### [React (v16.9.0)](https://reactjs.org)

React should be used in the modern way, with hooks and functional components, avoiding the more bloated class components with life cycle. This leads to more readable code with less boilerplate.

Though, there are some magic involved in using react hooks and there are some non-intuitive stuffs that should be avoided, read the [docs](https://reactjs.org/docs/hooks-intro.html) and rely on eslint to spot these traps.

### [Typescript](https://www.typescriptlang.org)

Typescript should be used to type as much stuffs as possible, avoid having `any` anywhere in the code. Sometimes its impossible or too complicated to avoid using an `any` type, such as when using an untyped library, though this should be rare.

### [Rebass](https://rebassjs.org) (powered by emotion and styled-system) for styling

Use Rebass for creating all components. Do not specify your own values for padding, margin, color and etc. Use Rebass's responsive and predefined values so that all the styles are consistent across the whole site and can be changed through the theming system.

### [Theme-UI](https://theme-ui.com) (powered by Rebass) for theming

Theme is located on the `src/gatsby-plugin-theme-ui/index.ts` file. This contains all the values that you can later use on rebass by referencing the key on the css property, check the rebass's [theming doc](https://rebassjs.org/theming).

### Other libs

- [Frappe Charts](https://theme-ui.com) for charting
- [Apollo](https://www.apollographql.com/docs/react/) for graphql client

## Linting and formatting ([eslint](https://eslint.org) and [prettier](https://prettier.io))

For ease of development, enable eslint and prettier integration on your editor. These tools will help you to avoid common mistakes on developing on typescript and react with hooks, and also format the code consistently across the whole codebase. Eslint will shows error whenever there are codes that doesn't follow prettier's formatting rules.

## Code Generation

To make development faster, easier, and more type safe, you can generate components easily with the tools below.

### [hygen](http://www.hygen.io) for generating components

This will generate a folder containing three files, the react component, a jest testing file with snapshot testing, and storybook [CSF](https://medium.com/storybookjs/component-story-format-66f4c32366df) file.

- `yarn gen:component [your_component_name]`

### [graphql-codegen](https://graphql-code-generator.com) for generating Graphql typing

The following component will generate a typescript definition file that contains all types from every graphql query that we do in our code. Run it every time you change an Apollo graphql query.

- `yarn gen:graphql-types`

## Development

We are developing using two different tools, Gatsby and Storybook. Gatsby is used to build the final optimized production app, storybook is used for testing, debugging and developing individual components

### [Storybook](https://storybook.js.org)

Use storybook if you want to test/develop individual components, without the need to care about where the data is coming from. You can mock data easily, run components in isolation, and tests individual components using Storybook. To develop on storybook use:

1. `yarn storybook`, this will run storybook on `localhost:6006`

### [Gatsby](https://www.gatsbyjs.org)

Use gatsby if you want to test/develop the final production app. Gatsby are being used to build the final production app as a collection of static pages, complete with pre-rendered html with content and font pre-fetching, image optimization, code-splitting, service worker powered by workbox, and a bunch of other goodies. Here are the commands for developing on gatsby for testing the final production app:

1. `yarn develop` to run the development build of gatsby.
2. `yarn build` to build the final production app. Output will be on `public` folder.
3. `yarn serve build` to

## Testing

### [Cypress](https://www.cypress.io) for running tests

Use cypress when you want to do tests that needs to simulate user interaction such as touches. mouse hover, keyboard typing, and mouse clicks. You can evaluate the styles, accessibility, and html markup anyway you like after what ever events you simulate with this.

To run cypress, you will need a server serving the component to be tests running. You can do cypress tests on both storybook(to test individual components) and gatsby (to test the final app with real data).

- `yarn cy:open` will open the cypress browser, use this during development, it will show the various UI state while the test is being run (similar to android's robotest), and stops in the exact state where a test fails so we can debug it.
- `yarn test:integration` will run cypress in headless mode, it will automatically runs all test. Use this when you need to quickly verify that all the tests are passing.

### [Jest](https://jestjs.io) for unit tests

Use jest whenever you have a piece of logic that need to be tested. Jest can also be used for snapshot testing of every components, this will help us narrow down when a component style or html markup changes. Snapshot tests will be automatically generated when you generate a component using hygen.

- `yarn test:unit` will run all jest unit tests, including snapshot tests.

## Commands Summary

1. `yarn develop` runs gatsby
2. `yarn test` runs all unit and integration test
3. `yarn test:integration` run integration test with cypress in headless mode
4. `yarn test:unit` run unit test with jest
5. `yarn cy:open` run full cypress with dashboard on browser
6. `yarn storybook` run storybook to see all components we built
