/* eslint-disable */
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`
})
let proxy = require("http-proxy-middleware")

module.exports = {
  // TODO: Don't forget to update metadata when released
  siteMetadata: {
    title: `Deca`,
    description: `Deca Dashboard`,
    author: `Deca`,
    siteUrl: `https://app.decapos.io`
  },
  // TODO: Temporary for deploying to gitlab pages
  pathPrefix: `silverhand`,
  plugins: [
    `gatsby-plugin-typescript`,
    // ASSETS / IMAGE ==========================================================
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: "gatsby-plugin-svgr",
      options: {
        prettier: true,         // use prettier to format JS code output (default)
        svgo: true,             // use svgo to optimize SVGs (default)
        svgoConfig: {
          removeViewBox: true, // remove viewBox when possible (default)
          cleanupIDs: true    // remove unused IDs and minify remaining IDs (default)
        }
      }
    },
    // SEO =====================================================================
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    "gatsby-plugin-robots-txt",
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        // TODO: Choose better name later
        name: `Deca Dashboard`,
        short_name: `Deca`,
        description: ``,
        start_url: `/`,
        lang: `en`,
        background_color: `#fff`,
        theme_color: `#3A2B95`,
        display: `standalone`,
        icon: `src/images/logo.png`,
        localize: [
          {
            start_url: `/id/`,
            lang: `id`,
            name: `Deca Dashboard`,
            short_name: `Deca`,
            description: ``
          }
        ]
      }
    },
    "gatsby-plugin-offline",
    // STYLING =================================================================
    // {
    //   resolve: `gatsby-plugin-typography`,
    //   options: {
    //     pathToConfigModule: `src/typography.ts`,
    //     omitGoogleFont: true
    //   }
    // },
    `gatsby-plugin-emotion`,
    "gatsby-plugin-theme-ui",
    // OTHER ===================================================================
    `gatsby-plugin-remove-trailing-slashes`,
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://app.decapos.io`
      }
    },
    {
      resolve: `gatsby-plugin-nprogress`,
      options: {
        // Setting a color is optional.
        color: `#3A2B95`,
        // Disable the loading spinner.
        showSpinner: false
      }
    },
    // PERFORMANCE =============================================================
    `gatsby-plugin-preact`,
    {
      resolve: `gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts: [
          {
            family: `Open Sans`,
            variants: [`300`, `400`]
          }
        ]
      }
    },
    // ANALYTICS ===============================================================
    {
      resolve: `gatsby-plugin-segment-js`,
      options: {
        // your segment write key for your production environment
        // when process.env.NODE_ENV === 'production'
        // required; non-empty string
        prodKey: `MgCQSKcnTGi49UZuSWuVKHUm1bNXmraG`,

        // if you have a development env for your segment account, paste that key here
        // when process.env.NODE_ENV === 'development'
        // optional; non-empty string
        devKey: `z6eiymQRiS6izcZRwKktIjnX17ZwDLpk`
      }
    },
    // INTL  ==========================================================
    {
      resolve: `gatsby-plugin-intl3`,
      options: {
        // language JSON resource path
        path: `${__dirname}/src/intl`,
        // supported language
        languages: [`en`, `id`],
        // language file path
        defaultLanguage: `en`,
        // option to redirect to `/ko` when connecting `/`
        redirect: false
      }
    },
    // DEVTOOLS ================================================================
    {
      resolve: "gatsby-plugin-webpack-bundle-analyzer",
      options: {
        analyzerPort: 3000,
        production: true,
        disable: true
      }
    }
  ],
  developMiddleware: app => {
    app.use(
      "/api",
      proxy({
        target: "http://localhost:8080"
      })
    )
  }
}
