// Code generated by github.com/99designs/gqlgen, DO NOT EDIT.

package termlink

import (
	"fmt"
	"io"
	"strconv"
	"time"
)

type Connection interface {
	IsConnection()
}

type Edge interface {
	IsEdge()
}

type Activity struct {
	Timestamp time.Time `json:"timestamp"`
	Action    string    `json:"action"`
	Actor     string    `json:"actor"`
	Store     *Store    `json:"store"`
}

type Company struct {
	ID             string           `json:"id"`
	Name           string           `json:"name"`
	Stores         *StoreConnection `json:"stores"`
	PaymentMethods []*PaymentMethod `json:"paymentMethods"`
	Items          []*Item          `json:"items"`
	Activities     []*Activity      `json:"activities"`
}

type ConnectedDevice struct {
	ID    string  `json:"id"`
	Store *Store  `json:"store"`
	Name  *string `json:"name"`
	Token *string `json:"token"`
}

type Expense struct {
	ID     string    `json:"id"`
	Amount *int      `json:"amount"`
	Date   time.Time `json:"date"`
}

type FixExpense struct {
	ID         string     `json:"id"`
	StartDate  *time.Time `json:"startDate"`
	EndDate    *time.Time `json:"endDate"`
	Amoount    *int       `json:"amoount"`
	Repetition *int       `json:"repetition"`
}

type Inventory struct {
	ID                string             `json:"id"`
	Store             *Store             `json:"store"`
	Item              *Item              `json:"item"`
	ResupplyHistories []*ResupplyHistory `json:"resupplyHistories"`
	Stock             *int               `json:"stock"`
}

type Item struct {
	ID               string             `json:"id"`
	Name             string             `json:"name"`
	Price            int                `json:"price"`
	ItemRequirements []*ItemRequirement `json:"itemRequirements"`
	IsListed         bool               `json:"isListed"`
	VariantItems     []*Item            `json:"variantItems"`
	Tags             []*Tag             `json:"tags"`
	MeasurementUnit  *MeasurementUnit   `json:"measurementUnit"`
	ImagePath        *string            `json:"imagePath"`
}

type ItemConnection struct {
	PageInfo *PageInfo   `json:"pageInfo"`
	Edges    []*ItemEdge `json:"edges"`
}

func (ItemConnection) IsConnection() {}

type ItemEdge struct {
	Cursor string `json:"cursor"`
	Node   *Item  `json:"node"`
}

func (ItemEdge) IsEdge() {}

type ItemInput struct {
	ID              *string         `json:"id"`
	Name            string          `json:"name"`
	Price           int             `json:"price"`
	RequiredItem    []*RequiredItem `json:"requiredItem"`
	IsListed        bool            `json:"isListed"`
	VariantItems    []*ItemInput    `json:"variantItems"`
	Tags            []int           `json:"tags"`
	MeasurementUnit int             `json:"measurementUnit"`
	ImagePath       *string         `json:"imagePath"`
	CompanyID       int             `json:"companyID"`
}

type ItemPerformance struct {
	SalesCount int    `json:"salesCount"`
	ItemName   string `json:"itemName"`
	Item       *Item  `json:"item"`
}

type ItemRequirement struct {
	ID           string `json:"id"`
	Item         *Item  `json:"item"`
	RequiredItem *Item  `json:"requiredItem"`
	Amount       *int   `json:"amount"`
}

type MeasurementUnit struct {
	ID       string  `json:"id"`
	Name     *string `json:"name"`
	Notation *string `json:"notation"`
}

type Order struct {
	ID           string         `json:"id"`
	Status       *OrderStatus   `json:"status"`
	Items        []*Item        `json:"items"`
	TotalPrice   *int           `json:"totalPrice"`
	PurchaseDate time.Time      `json:"purchaseDate"`
	Labels       []*OrderLabels `json:"labels"`
}

type OrderLabels struct {
	ID            string   `json:"id"`
	Name          *string  `json:"name"`
	Values        []string `json:"values"`
	SelectedValue *string  `json:"selectedValue"`
}

type OrderStatus struct {
	ID      string       `json:"id"`
	Name    *string      `json:"name"`
	After   *OrderStatus `json:"after"`
	Before  *OrderStatus `json:"before"`
	Company *Company     `json:"company"`
}

type PageInfo struct {
	HasNextPage     bool   `json:"hasNextPage"`
	HasPreviousPage bool   `json:"hasPreviousPage"`
	StartCursor     string `json:"startCursor"`
	EndCursor       string `json:"endCursor"`
}

type PaymentMethod struct {
	ID    string  `json:"id"`
	Store *Store  `json:"store"`
	Token *string `json:"token"`
	Type  *string `json:"type"`
}

type Report struct {
	Sales *ReportSales `json:"sales"`
	Item  *ReportItem  `json:"item"`
}

type ReportItem struct {
	TimeRange        ReportTimeRange    `json:"timeRange"`
	ItemPerformances []*ItemPerformance `json:"itemPerformances"`
}

type ReportSales struct {
	TimeRange  ReportTimeRange `json:"timeRange"`
	TotalSales int             `json:"totalSales"`
	Summaries  []*SalesSummary `json:"summaries"`
}

type RequiredItem struct {
	ID       string `json:"id"`
	Quantity int    `json:"quantity"`
}

type ResupplyHistory struct {
	ID       string     `json:"id"`
	Date     *time.Time `json:"date"`
	Price    *int       `json:"price"`
	Amount   *int       `json:"amount"`
	Supplier *Supplier  `json:"supplier"`
}

type SalesSummary struct {
	Time       string   `json:"time"`
	TotalValue int      `json:"totalValue"`
	Orders     []*Order `json:"orders"`
}

type Store struct {
	ID              string             `json:"id"`
	Name            string             `json:"name"`
	Address         string             `json:"address"`
	Expenses        []*Expense         `json:"expenses"`
	ConnectedDevice []*ConnectedDevice `json:"connectedDevice"`
	FixExpenses     []*FixExpense      `json:"fixExpenses"`
	Inventory       []*Inventory       `json:"inventory"`
	Orders          []*Order           `json:"orders"`
}

type StoreConnection struct {
	PageInfo *PageInfo    `json:"pageInfo"`
	Edges    []*StoreEdge `json:"edges"`
}

func (StoreConnection) IsConnection() {}

type StoreEdge struct {
	Cursor string `json:"cursor"`
	Node   *Store `json:"node"`
}

func (StoreEdge) IsEdge() {}

type Supplier struct {
	ID      string  `json:"id"`
	Name    *string `json:"name"`
	Type    *string `json:"type"`
	URL     *string `json:"url"`
	Address *string `json:"address"`
	Email   *string `json:"email"`
	Phone   *string `json:"phone"`
}

type Tag struct {
	ID    string  `json:"id"`
	Name  *string `json:"name"`
	Value *string `json:"value"`
}

type User struct {
	ID        string     `json:"id"`
	Name      string     `json:"name"`
	Email     string     `json:"email"`
	Companies []*Company `json:"companies"`
	Avatar    string     `json:"avatar"`
}

type ReportTimeRange string

const (
	ReportTimeRangeDaily   ReportTimeRange = "DAILY"
	ReportTimeRangeWeekly  ReportTimeRange = "WEEKLY"
	ReportTimeRangeMonthly ReportTimeRange = "MONTHLY"
	ReportTimeRangeYearly  ReportTimeRange = "YEARLY"
)

var AllReportTimeRange = []ReportTimeRange{
	ReportTimeRangeDaily,
	ReportTimeRangeWeekly,
	ReportTimeRangeMonthly,
	ReportTimeRangeYearly,
}

func (e ReportTimeRange) IsValid() bool {
	switch e {
	case ReportTimeRangeDaily, ReportTimeRangeWeekly, ReportTimeRangeMonthly, ReportTimeRangeYearly:
		return true
	}
	return false
}

func (e ReportTimeRange) String() string {
	return string(e)
}

func (e *ReportTimeRange) UnmarshalGQL(v interface{}) error {
	str, ok := v.(string)
	if !ok {
		return fmt.Errorf("enums must be strings")
	}

	*e = ReportTimeRange(str)
	if !e.IsValid() {
		return fmt.Errorf("%s is not a valid ReportTimeRange", str)
	}
	return nil
}

func (e ReportTimeRange) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(e.String()))
}
