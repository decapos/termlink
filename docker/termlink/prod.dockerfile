FROM golang:1.13

WORKDIR /usr/src/termlink

ENV GIN_MODE=release

COPY termlink .
RUN mkdir silverhand
COPY silverhand/public ./silverhand/public

EXPOSE 8080
CMD ./termlink