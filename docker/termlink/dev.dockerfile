FROM golang:1.13
WORKDIR /usr/src/termlink

ENV GO111MODULE=on

RUN curl -fLo /usr/bin/air https://raw.githubusercontent.com/cosmtrek/air/master/bin/linux/air
RUN chmod +x /usr/bin/air

RUN go get github.com/go-delve/delve/cmd/dlv

EXPOSE 8080 40000
CMD ["air"]
