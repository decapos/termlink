scalar Cursor
scalar Upload
scalar Time

interface Connection {
  pageInfo: PageInfo!
  edges: [Edge!]!
}

interface Edge {
  cursor: Cursor!
}

type PageInfo {
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: Cursor!
  endCursor: Cursor!
}

type Query {
  company: Company!
  store: Store!
  user: User!
  orders: [Order!]!
  report: Report!
  items(
    filter: String
    first: Int
    after: Cursor
    last: Int
    before: Cursor
  ): ItemConnection!
}

type Mutation {
  addItem(
    input: ItemInput!
  ): Item!
}

# ===============================================================
# Copmany
# ===============================================================
type Company {
  id: ID!
  name: String!
  stores: StoreConnection!
  paymentMethods: [PaymentMethod!]
  items: [Item!]
  activities: [Activity!]
}

type StoreConnection implements Connection {
  pageInfo: PageInfo!
  edges: [StoreEdge!]!
}

type StoreEdge implements Edge {
  cursor: Cursor!
  node: Store!
}

type Store {
  id: ID!
  name: String!
  address: String!
  expenses: [Expense!]
  connectedDevice: [ConnectedDevice!]
  fixExpenses: [FixExpense!]
  inventory: [Inventory!]
  orders: [Order!]
}

# ===============================================================
# Item
# ===============================================================
type ItemConnection implements Connection {
  pageInfo: PageInfo!
  edges: [ItemEdge!]!
}

type ItemEdge implements Edge {
  cursor: Cursor!
  node: Item!
}

type Item {
  id: ID!
  name: String!
  # TODO: Might be better to use something more robust than Int
  #   (eg. Shopify uses custom Money schema)
  price: Int!
  itemRequirements: [ItemRequirement!]!
  # Signify wheter the item is available for purchase
  isListed: Boolean!
  variantItems: [Item!]
  tags: [Tag!]!
  # FIXME: is there better name?
  measurementUnit: MeasurementUnit!
  imagePath: String
}

input ItemInput {
  id: ID
  name: String!
  price: Int!
  requiredItem: [RequiredItem!]!
  isListed: Boolean!
  variantItems: [ItemInput!]!
  tags: [Int!]!
  measurementUnit: Int!
  imagePath: String
  companyID: Int!
}

input RequiredItem {
  id: ID!
  quantity: Int!
}
# ===============================================================
# User
# ===============================================================
type User {
  id: ID!
  name: String!
  email: String!
  companies: [Company!]
  avatar: String!
}

type Activity {
  timestamp: Time!

  # The action being done
  action: String!

  # The source of the activity
  actor: String!

  # The store affected by the activity
  store: Store!
}

type Order {
  id: ID!
  status: OrderStatus
  items: [Item!]
  # TODO: Might be better to use something more robust than Int
  #   (eg. Shopify uses custom Money schema)
  totalPrice: Int
  purchaseDate: Time!
  labels: [OrderLabels!]
}

type Inventory {
  id: ID!
  store: Store
  item: Item
  resupplyHistories: [ResupplyHistory!]
  stock: Int
}

type PaymentMethod {
  id: ID!
  store: Store
  token: String
  # String? Another object on the schema?
  type: String
}

type Expense {
  id: ID!
  amount: Int
  date: Time!
}

type ConnectedDevice {
  id: ID!
  store: Store
  name: String
  token: String
}

type FixExpense {
  id: ID!
  startDate: Time
  endDate: Time
  amoount: Int
  # FIXME: Monthly? Yearly? that kind of thing. Better name?
  repetition: Int
}

type ItemRequirement {
  id: ID!
  item: Item
  requiredItem: Item
  amount: Int
}

type Tag {
  id: ID!
  name: String
  value: String
}

type MeasurementUnit {
  id: ID!

  # Name of the unit
  name: String
  notation: String
}

# Each outlet have different OrderStatus
type OrderStatus {
  id: ID!
  name: String
  after: OrderStatus
  before: OrderStatus
  company: Company
}

type OrderLabels {
  id: ID!
  name: String
  values: [String!]
  selectedValue: String
}

type ResupplyHistory {
  id: ID!
  date: Time
  # Again, int? custom Money object like shopify?
  price: Int
  amount: Int
  supplier: Supplier
}

type Supplier {
  id: ID!
  name: String
  type: String
  url: String
  address: String
  email: String
  phone: String
}

# ==================================================
# All possible report data
# =================================================
enum ReportTimeRange {
  DAILY
  WEEKLY
  MONTHLY
  YEARLY
}

type Report {
  # Sales report of the business
  sales(timeRange: ReportTimeRange): ReportSales!

  # Reports regarding the performance of each product
  item(timeRange: ReportTimeRange): ReportItem!
}

type ReportSales {
  timeRange: ReportTimeRange!

  # Total sales in the given time range
  totalSales: Int!

  # All sales info in the given time range
  summaries: [SalesSummary!]!
}

# Successful transactions
type SalesSummary {
  # The time of the sales (can be day, hours or month depending of the time range, eg.
  # MONTHLY, WEEKLY, or DAILY)
  time: String!

  # Total value involves in the sale
  totalValue: Int!

  # Order that is involved on the sales
  orders: [Order!]!
}

type ReportItem {
  timeRange: ReportTimeRange!
  itemPerformances: [ItemPerformance!]!
}

# Data about the performance of each items
type ItemPerformance {
  salesCount: Int!
  itemName: String!
  item: Item!
}
