package schema

import "fmt"

// schema of all tables
var schema string

// InitSchema will init all schema table to be execute
func InitSchema() {
	// base fields idealy will be on every table
	base := `
		id serial primary key not null,
		created_at timestamp not null,
		updated_at timestamp,
		deleted_at timestamp
	`

	activityLog := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS activity_log (
		%s,
		actor varchar(50) not null,
		action varchar(255) not null,
		company_id int4 not null,
		store_id int4 not null,
		device_id int4 not null,
		permission int2 not null
	);`, base)
	schema += activityLog

	billing := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS billing (
		%s,
		company_id int4 not null,
		status int2 not null
	);`, base)
	schema += billing

	company := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS company (
		%s,
		name varchar(50),
		address varchar(255),
		company_type varchar(255),
		phone_number varchar(20) not null,
		default_subscription_id int4,
		owner_name varchar(50),
		is_premium int2
	);`, base)
	schema += company

	connectedDevice := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS connected_device (
		%s,
		username varchar(50) not null,
		device_name varchar(255) not null,
		pin varchar(6) not null,
		store_id int4 not null,
		is_allowed int2 not null
	);`, base)
	schema += connectedDevice

	customLabelName := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS custom_label_name (
		%s,
		name varchar(255) not null,
		company_id int4 not null
	);`, base)
	schema += customLabelName

	customLabelValue := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS custom_label_value (
		%s,
		custom_label_name_id int4 not null,
		custom_label_value varchar(255) not null
	);`, base)
	schema += customLabelValue

	// expenses := fmt.Sprintf(`
	// CREATE TABLE IF NOT EXISTS expenses (
	// 	%s
	// );`, base)
	// schema += expenses

	// fixExpenses := fmt.Sprintf(`
	// CREATE TABLE IF NOT EXISTS fix_expenses (
	// 	%s
	// );`, base)
	// schema += fixExpenses

	inventory := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS inventory (
		%s,
		store_id int4 not null,
		item_id int4 not null,
		stock int2 not null
	);`, base)
	schema += inventory

	inventoryRestockHistory := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS inventory_restock_history (
		%s,
		amount_restock int2 not null,
		price numeric(15,2) not null,
		inventory_id int4 not null,
		supplier_id int4 not null
	);`, base)
	schema += inventoryRestockHistory

	item := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS item (
		%s,
		name varchar(255) not null,
		price numeric(15,2) not null,
		image_path varchar(255),
		has_variant int2,
		is_variant int2,
		measurement_unit int2 not null,
		is_listed int2 not null,
		company_id int4 not null
	);`, base)
	schema += item

	itemRequirement := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS item_requirement (
		%s,
		item_id int4 not null,
		requirement_item_id int4 not null,
		amount int4 not null
	);`, base)
	schema += itemRequirement

	itemTag := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS item_tag (
		%s,
		item_id int4 not null,
		tag_id int4 not null
	);`, base)
	schema += itemTag

	itemVariantRelation := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS item_variant_relation (
		%s,
		item_id int4 not null,
		parent_id int4 not null
	);`, base)
	schema += itemVariantRelation

	itemVariantValue := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS item_variant_value (
		%s,
		item_id int4 not null,
		variant_value_id int4 not null
	);`, base)
	schema += itemVariantValue

	order := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS "order" (
		%s,
		conn_device_id int4 not null,
		total_pay_amount numeric(15,2) not null,
		total_discount numeric(15,2),
		payment_method int2 not null,
		order_status int2 not null,
		company_id int4 not null
	);`, base)
	schema += order

	orderCustomLabel := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS order_custom_label (
		%s,
		order_id int4 not null,
		custom_label_value_id int4 not null
	);`, base)
	schema += orderCustomLabel

	orderItem := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS order_item (
		%s,
		order_id int4 not null,
		item_id int4 not null,
		quantity int2 not null,
		total_amount numeric(15,2) not null
	);`, base)
	schema += orderItem

	// orderStatus := fmt.Sprintf(`
	// CREATE TABLE IF NOT EXISTS order_status (
	// 	%s
	// );`, base)
	// schema += orderStatus

	// permission := fmt.Sprintf(`
	// CREATE TABLE IF NOT EXISTS permission (
	// 	%s,
	// 	role_name varchar(255) not null
	// );`, base)
	// schema += permission

	rolePermission := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS role_permission (
		%s,
		permission int2 not null,
		user_role_id int4 not null
	);`, base)
	schema += rolePermission

	store := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS store (
		%s,
		company_id int4 not null,
		address varchar(255),
		phone_number varchar(20)
	);`, base)
	schema += store

	subscription := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS subscription (
		%s,
		method int2 not null,
		amount numeric(15,2) not null,
		currency varchar(3) not null,
		token varchar(255),
		company_id int4 not null
	);`, base)
	schema += subscription

	supplier := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS supplier (
		%s,
		name varchar(255),
		address varchar(255),
		phone_number varchar(20) not null,
		email varchar(255)
	);`, base)
	schema += supplier

	tag := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS tag (
		%s,
		value varchar(255) not null,
		company_id int4 not null
	);`, base)
	schema += tag

	user := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS "user" (
		%s,
		name varchar(255),
		email varchar(255),
		password varchar(255),
		phone_number varchar(20),
		gender varchar(20)
	);`, base)
	schema += user

	userRole := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS user_role (
		%s,
		user_id int4 not null,
		company_id int4 not null
	);`, base)
	schema += userRole

	variantType := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS variant_type (
		%s,
		name varchar(255) not null,
		parent_item_id int4 not null
	);`, base)
	schema += variantType

	variantValue := fmt.Sprintf(`
	CREATE TABLE IF NOT EXISTS variant_value (
		%s,
		variant_type_id int4 not null,
		value varchar(255) not null
	);`, base)
	schema += variantValue
}

// GetSchema return all schema table value
func GetSchema() string {
	return schema
}
