package config

import (
	"github.com/joho/godotenv"
	"os"
	"strconv"
)

type Database struct {
	Host     string
	Password string
	Username string
	Port     string
	MaxConn  int
	MaxIdle  int
}

type Redis struct {
	Host string
	Port string
}

type Config struct {
	Database      Database
	LogFileSwitch bool
	Redis         Redis
	MapPath       string
}

var cfg Config

func InitConfig() error {
	env := os.Getenv("DECA_ENV")
	if env == "" {
		env = "development"
	}
	_ = godotenv.Load(".env." + env + ".local")
	if env != "test" {
		_ = godotenv.Load(".env.local")
	}
	_ = godotenv.Load(".env." + env)
	_ = godotenv.Load() // The Original .env

	// Get config value
	host := os.Getenv("DB_HOST")
	password := os.Getenv("DB_PASSWORD")
	username := os.Getenv("DB_USERNAME")
	port := os.Getenv("DB_PORT")
	maxConn, err := strconv.Atoi(os.Getenv("DB_MAX_CONN"))
	if err != nil {
		maxConn = 100
	}
	maxIdle, err := strconv.Atoi(os.Getenv("DB_MAX_IDLE"))
	if err != nil {
		maxIdle = 10
	}

	isLogFile, err := strconv.ParseBool(os.Getenv("LOG_TO_FILE"))
	if err != nil {
		isLogFile = false
	}

	redisHost := os.Getenv("REDIS_HOST")
	redisPort := os.Getenv("REDIS_PORT")

	mapPath := os.Getenv("MAP_PATH")

	// Set config to struct
	cfg = Config{
		Database: Database{
			Host:     host,
			Password: password,
			Username: username,
			MaxConn:  maxConn,
			MaxIdle:  maxIdle,
			Port:     port,
		},
		LogFileSwitch: isLogFile,
		Redis: Redis{
			Host: redisHost,
			Port: redisPort,
		},
		MapPath: mapPath,
	}

	return err
}

func GetConfig() Config {
	return cfg
}
